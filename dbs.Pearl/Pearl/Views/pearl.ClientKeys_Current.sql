﻿CREATE VIEW [pearl].[ClientKeys_Current]
WITH SCHEMABINDING
AS
SELECT
	[k].[ClientKeysID],
	[k].[Refnumber],
	[k].[ClientKey],
	[k].[EffectiveAfter]
FROM
	[pearl].[ClientKeys]	[k]
WHERE	[k].IsValid			= 1
	AND	[k].[ClientKeysID]	= (
		SELECT TOP 1
			[i].[ClientKeysID]
		FROM
			[pearl].[ClientKeys]	[i]
		WHERE
			[i].[Refnumber] = [k].[Refnumber]
		ORDER BY
			[i].[EffectiveAfter]	DESC
		);