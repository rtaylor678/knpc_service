﻿CREATE PROCEDURE [pearl].[Return_RefnumFromClientKey]
(
	@ClientKey	VARCHAR(256)
)
AS
BEGIN

	DECLARE @Refnumber	VARCHAR(32);

	SELECT
		@Refnumber = [k].[Refnumber]
	FROM
		[pearl].[ClientKeys_Current]	[k]
	WHERE
		[k].[ClientKey]	= @ClientKey;

	SELECT @Refnumber;
	RETURN @Refnumber;

END;