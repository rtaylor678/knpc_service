//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Output
{
    using System;
    using System.Collections.Generic;
    
    public partial class DataSubmission
    {
        public int SubmissionID { get; set; }
        public string DataKey { get; set; }
        public int StudyMethodology { get; set; }
        public Nullable<float> NumberValue { get; set; }
        public string TextValue { get; set; }
        public System.DateTime CalcTime { get; set; }
        public Nullable<System.DateTime> DateVal { get; set; }
        public string GroupKey { get; set; }
    
        public virtual DataKey DataKey1 { get; set; }
    }
}
