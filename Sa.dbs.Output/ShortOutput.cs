﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.dbs.Output
{
    [Serializable]
    public class ShortOutput
    {
        public virtual ICollection<CalcTime> CalcTimes { get; set; }
        //public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<DataTable> DataTables { get; set; }
        //public virtual ICollection<Datum> Data { get; set; }
        public virtual ICollection<DataKey> DataKeys { get; set; }
        public virtual ICollection<SubmissionData> DataSubmissions { get; set; }
        //public virtual ICollection<FinProdProp> FinProdProps { get; set; }
        public virtual ICollection<GroupKey> GroupKeys { get; set; }
        public virtual ICollection<ProcessData> ProcessDatas { get; set; }
        //public virtual ICollection<ValidationResult> ValidationResults { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        


        public ShortOutput()
        {
            this.CalcTimes = new HashSet<CalcTime>();
            //this.Comments = new HashSet<Comment>();
            this.DataTables = new HashSet<DataTable>();
            this.DataKeys = new HashSet<DataKey>();
            this.DataSubmissions = new HashSet<SubmissionData>();
            //this.FinProdProps = new HashSet<FinProdProp>();
            this.GroupKeys = new HashSet<GroupKey>();
            this.ProcessDatas = new HashSet<ProcessData>();
            //this.ValidationResults = new HashSet<ValidationResult>();
            this.Messages = new HashSet<Message>();
            
        }
    }

    [Serializable]
    public partial class SubmissionData
    {
        public int SubmissionID { get; set; }
        public string DataKey { get; set; }
        public int StudyMethodology { get; set; }
        public Nullable<float> NumberValue { get; set; }
        public string TextValue { get; set; }
        public System.DateTime CalcTime { get; set; }
        public Nullable<System.DateTime> DateVal { get; set; }
        public string GroupKey { get; set; }
    }
}
