﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa
{
    public static class Logger
    {
        //NOTE: 1.2.14 has a bug that blocks writing to db. SO using 1.2.13 here.
        public static void LogException(Exception exception)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                GetException(sb, exception);
                log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(sb.ToString());
                //log.Logger.Repository.Shutdown();
                //foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
                //{
                //    app.Close();
                //}
            }
            catch (Exception logerr)
            {
                //do nothing
            }
        }
        private static void GetException(StringBuilder message, Exception exp)
        {
            if (exp == null)
            {
                return;
            }
            message.AppendFormat("Message: {0}\r\nStackTrace\r\n{1}\r\n\r\n", exp.Message, exp.StackTrace);
            GetException(message, exp.InnerException);
        }
        public static void LogInfo(string info)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Info(info);
            //log.Logger.Repository.Shutdown();
            //foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
            //{
            //    app.Close();
            //}
        }
    }
}
