﻿INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_DISC', N'Absence_MPS_DISC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_EXC', N'Absence_MPS_EXC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_HOL', N'Absence_MPS_HOL', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_JURY', N'Absence_MPS_JURY', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_MIL', N'Absence_MPS_MIL', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_ONJOB', N'Absence_MPS_ONJOB', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_SICK', N'Absence_MPS_SICK', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_TOTABS', N'Absence_MPS_TOTABS', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_UNEXC', N'Absence_MPS_UNEXC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_UNION', N'Absence_MPS_UNION', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_MPS_VAC', N'Absence_MPS_VAC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_DISC', N'Absence_OCC_DISC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_EXC', N'Absence_OCC_EXC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_HOL', N'Absence_OCC_HOL', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_JURY', N'Absence_OCC_JURY', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_MIL', N'Absence_OCC_MIL', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_ONJOB', N'Absence_OCC_ONJOB', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_SICK', N'Absence_OCC_SICK', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_TOTABS', N'Absence_OCC_TOTABS', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_UNEXC', N'Absence_OCC_UNEXC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_UNION', N'Absence_OCC_UNION', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Absence_OCC_VAC', N'Absence_OCC_VAC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_AvgCDUCap', N'Gensum_AvgCDUCap', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_AvgCptlEmployed', N'Gensum_AvgCptlEmployed', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_AvgCrudeCostUSD', N'Gensum_AvgCrudeCostUSD', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_AvgEDC_k', N'Gensum_AvgEDC_k', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_AvgProcessEDC_k', N'Gensum_AvgProcessEDC_k', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_AvgProcessUEDC_k', N'Gensum_AvgProcessUEDC_k', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_CDUUtilPcnt', N'Gensum_CDUUtilPcnt', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_CrudeAPI', N'Gensum_CrudeAPI', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_CrudeSulfur', N'Gensum_CrudeSulfur', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_EII', N'Gensum_EII', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_MaintEffDiv', N'Gensum_MaintEffDiv', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_MaintEffIndex', N'Gensum_MaintEffIndex', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_MaintIndex', N'Gensum_MaintIndex', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_MaintPersEffDiv', N'Gensum_MaintPersEffDiv', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_MaintPersEffIndex', N'Gensum_MaintPersEffIndex', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_NEOpexEDC', N'Gensum_NEOpexEDC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_NEOpexEffDiv', N'Gensum_NEOpexEffDiv', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_NEOpexEffIndex', N'Gensum_NEOpexEffIndex', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_NEOpexUEDC', N'Gensum_NEOpexUEDC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_NetInputkbbl', N'Gensum_NetInputkbbl', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_NetInputkMT', N'Gensum_NetInputkMT', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_NonMaintPersEffDiv', N'Gensum_NonMaintPersEffDiv', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_NonMaintPersEffIndex', N'Gensum_NonMaintPersEffIndex', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_PEI', N'Gensum_PEI', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_PersCostEDC', N'Gensum_PersCostEDC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_PersEffDiv', N'Gensum_PersEffDiv', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_PersEffIndex', N'Gensum_PersEffIndex', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_ProcessUtilPcnt', N'Gensum_ProcessUtilPcnt', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_RefUtilPcnt', N'Gensum_RefUtilPcnt', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_ROI', N'Gensum_ROI', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_StdEnergyGJ', N'Gensum_StdEnergyGJ', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_TotCashOPEXUEDC', N'Gensum_TotCashOPEXUEDC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_TotCDUCap', N'Gensum_TotCDUCap', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_TotEqPEDC', N'Gensum_TotEqPEDC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_TotWhrEDC', N'Gensum_TotWhrEDC', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_UEDC_k', N'Gensum_UEDC_k', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_UtilOSTA', N'Gensum_UtilOSTA', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Gensum_VEI', N'Gensum_VEI', N'N', NULL, NULL, NULL)
GO
INSERT INTO [output].[DataKeys] ([DataKey], [DataLabel], [DataType], [UOM], [Currency], [PricingBasis]) VALUES (N'Maint_MaintEffIndex', N'Maint_MaintEffIndex', N'N', NULL, NULL, NULL)
GO
