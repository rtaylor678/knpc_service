﻿INSERT INTO [dim].[Absence_LU]
(
	[CategoryID],
	[Description]
)
SELECT
	[t].[CategoryID],
	[t].[Description]
FROM (VALUES
('ONJOB', 'On-the-Job Injuries'),
('SICK', 'Sickness & Off-the-Job Injuries'),
('MIL', 'Military Leave, Reserve Duty, National Service'),
('JURY', 'Jury Duty'),
('UNION', 'Union Representation'),
('DISC', 'Discipline'),
('VAC', 'Personal Vacation/Personal Holiday'),
('HOL', 'Public or National Holidays'),
('EXC', 'Other Excused Personal Absences'),
('UNEXC', 'Unexcused Absences')
) [t] ([CategoryID], [Description])
LEFT OUTER JOIN
	[dim].[Absence_LU]	[l]
		ON	[l].[CategoryID]	= [t].[CategoryID]
WHERE	[l].[CategoryID] IS NULL;