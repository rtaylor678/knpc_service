﻿INSERT INTO [dim].[CptlCode_LU]
(
	[CptlCode],
	[Description]
)
SELECT
	[t].[CptlCode],
	[t].[Description]
FROM (VALUES
('CURR', 'Current Year'),
('P1', 'Current Year minus 1yrs'),
('P2', 'Current Year minus 2yrs'),
('P3', 'Current Year minus 3yrs')
) [t] ([CptlCode], [Description])
LEFT OUTER JOIN
	[dim].[CptlCode_LU]	[l]
		ON	[l].[CptlCode]	= [t].[CptlCode]
WHERE	[l].[CptlCode]	IS NULL;