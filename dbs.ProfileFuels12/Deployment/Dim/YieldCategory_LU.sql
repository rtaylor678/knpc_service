﻿INSERT INTO [dim].[YieldCategory_LU]
(
	[Category],
	[Description]
)
SELECT
	[t].[Category],
	[t].[Description]
FROM (VALUES
('ASP', 'Asphalt'),
('COKE', 'Saleable Petroleum Coke'),
('FCHEM', 'Refinery Feedstocks To Chemical Plant'),
('FFUEL', 'Return Streams to Fuels Refinery'),
('FLUBE', 'Refinery Feedstocks To Lube Refining'),
('MPROD', 'Miscellaneous Products'),
('NLUBE', 'Non-Lube Products not Returned to Fuels'),
('OTHRM', 'Other Raw Material Inputs'),
('PROD', 'Primary Product Yields'),
('RCHEM', 'Returns From Chemical Plant'),
('RLUBE', 'Returns From Lube Refining'),
('RMB', 'Raw Materials Purchased for Blending'),
('RMI', 'Primary Raw Materials'),
('RPF', 'Lube Refinery-Produced Fuel'),
('SOLV', 'Specialty Solvents')
) [t] ([Category], [Description])
LEFT OUTER JOIN
	[dim].[YieldCategory_LU]	[l]
		ON	[l].[Category]	= [t].[Category]
WHERE	[l].[Category]	IS NULL;