﻿INSERT INTO [dim].[FHProcessFluid_LU]
(
	[ProcessFluid],
	[Description]
)
SELECT
	[t].[ProcessFluid],
	[t].[Description]
FROM (VALUES
('H2', 'Hydrogen Only'),
('H2O', 'Stream/Water Mixtures'),
('H2STM', 'Light Hydrocarbon, Hydrogen and Steam Mixtures'),
('HCSTM', 'Hydrocarbon and Steam Mixtures'),
('OIL', 'Oils'),
('OILH2', 'Oil and Hydrogen Mix'),
('OTH', 'Other')
) [t] ([ProcessFluid], [Description])
LEFT OUTER JOIN
	[dim].[FHProcessFluid_LU]	[l]
		ON	[l].[ProcessFluid]	= [t].[ProcessFluid]
WHERE	[l].[ProcessFluid]	IS NULL;