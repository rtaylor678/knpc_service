﻿INSERT INTO [dim].[TankType_LU]
(
	[TankType],
	[Description]
)
SELECT
	[t].[TankType],
	[t].[Description]
FROM (VALUES
('CRD', 'Crude Oil'),
('DST', 'Finished Products - Distillate'),
('GAS', 'Finished Products - Gasoline'),
('INT', 'Intermediate Stocks'),
('NHC', 'Non-Hydrocarbon Tankage'),
('OFP', 'Finished Products - All Other')
) [t] ([TankType], [Description])
LEFT OUTER JOIN
	[dim].[TankType_LU]	[l]
		ON	[l].[TankType]	= [t].[TankType]
WHERE	[l].[TankType]	IS NULL;