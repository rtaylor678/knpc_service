//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class SS_GetInventory_Result
    {
        public string TankType { get; set; }
        public Nullable<short> NumTank { get; set; }
        public Nullable<double> FuelsStorage { get; set; }
        public Nullable<float> AvgLevel { get; set; }
    }
}
