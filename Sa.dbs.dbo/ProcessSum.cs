//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    using System.Collections.Generic;
    
    [Serializable]
    public partial class ProcessSum
    {
        public int SubmissionID { get; set; }
        public string Scenario { get; set; }
        public string DataType { get; set; }
        public Nullable<float> CDU { get; set; }
        public Nullable<float> VAC { get; set; }
        public Nullable<float> VBR { get; set; }
        public Nullable<float> TCR { get; set; }
        public Nullable<float> COK { get; set; }
        public Nullable<float> FCC { get; set; }
        public Nullable<float> HYC { get; set; }
        public Nullable<float> REF { get; set; }
        public Nullable<float> HYG { get; set; }
        public Nullable<float> H2PURE { get; set; }
        public Nullable<float> POLY { get; set; }
        public Nullable<float> DIM { get; set; }
        public Nullable<float> MTBE { get; set; }
        public Nullable<float> ALKY { get; set; }
        public Nullable<float> C4ISOM { get; set; }
        public Nullable<float> C5ISOM { get; set; }
        public Nullable<float> NHYT { get; set; }
        public Nullable<float> KHYT { get; set; }
        public Nullable<float> DHYT { get; set; }
        public Nullable<float> SHYT { get; set; }
        public Nullable<float> VHYT { get; set; }
        public Nullable<float> RHYT { get; set; }
        public Nullable<float> SDA { get; set; }
        public Nullable<float> SRU { get; set; }
        public Nullable<float> TRU { get; set; }
        public Nullable<float> ACID { get; set; }
        public Nullable<float> ASP { get; set; }
        public Nullable<float> CALCIN { get; set; }
        public Nullable<float> DESAL { get; set; }
        public Nullable<float> CO2 { get; set; }
        public Nullable<float> ASE { get; set; }
        public Nullable<float> CUM { get; set; }
        public Nullable<float> CYC6 { get; set; }
        public Nullable<float> HDA { get; set; }
        public Nullable<float> TDP { get; set; }
        public Nullable<float> XYISOM { get; set; }
        public Nullable<float> PXYL { get; set; }
        public Nullable<float> EBZ { get; set; }
        public Nullable<float> C3S { get; set; }
        public Nullable<float> DIB { get; set; }
        public Nullable<float> DIP { get; set; }
        public Nullable<float> DIH { get; set; }
        public Nullable<float> DHEP { get; set; }
        public Nullable<float> REFS { get; set; }
        public Nullable<float> FCCS { get; set; }
        public Nullable<float> BZC { get; set; }
        public Nullable<float> TOLC { get; set; }
        public Nullable<float> XYLC { get; set; }
        public Nullable<float> XYLS { get; set; }
        public Nullable<float> OXYLRC { get; set; }
        public Nullable<float> HVYARO { get; set; }
        public Nullable<float> U1 { get; set; }
        public Nullable<float> U6 { get; set; }
        public Nullable<float> U9 { get; set; }
        public Nullable<float> U10 { get; set; }
        public Nullable<float> U18 { get; set; }
        public Nullable<float> U19 { get; set; }
        public Nullable<float> U20 { get; set; }
        public Nullable<float> U21 { get; set; }
        public Nullable<float> U26 { get; set; }
        public Nullable<float> U29 { get; set; }
        public Nullable<float> U30 { get; set; }
        public Nullable<float> U31 { get; set; }
        public Nullable<float> U32 { get; set; }
        public Nullable<float> U33 { get; set; }
        public Nullable<float> U35 { get; set; }
        public Nullable<float> U37 { get; set; }
        public Nullable<float> U38 { get; set; }
        public Nullable<float> U40 { get; set; }
        public Nullable<float> U45 { get; set; }
        public Nullable<float> U46 { get; set; }
        public Nullable<float> U47 { get; set; }
        public Nullable<float> U48 { get; set; }
        public Nullable<float> U58 { get; set; }
        public Nullable<float> U59 { get; set; }
        public Nullable<float> U60 { get; set; }
        public Nullable<float> U63 { get; set; }
        public Nullable<float> U68 { get; set; }
        public Nullable<float> U69 { get; set; }
        public Nullable<float> U72 { get; set; }
        public Nullable<float> U64 { get; set; }
        public Nullable<float> U65 { get; set; }
        public Nullable<float> U66 { get; set; }
        public Nullable<float> U70 { get; set; }
        public Nullable<float> U71 { get; set; }
        public Nullable<float> U73 { get; set; }
        public Nullable<float> U75 { get; set; }
        public Nullable<float> U76 { get; set; }
        public Nullable<float> U77 { get; set; }
        public Nullable<float> U78 { get; set; }
        public Nullable<float> U79 { get; set; }
    }
}
