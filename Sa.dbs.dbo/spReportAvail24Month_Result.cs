//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class spReportAvail24Month_Result
    {
        public Nullable<float> MechUnavailTA { get; set; }
        public Nullable<float> MechAvail { get; set; }
        public Nullable<float> OpAvail { get; set; }
        public Nullable<float> OnStream { get; set; }
        public Nullable<float> OnStreamSlow { get; set; }
        public System.DateTime PeriodStart { get; set; }
    }
}
