//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class CrudeGravityAdj_Result
    {
        public Nullable<byte> APIScale { get; set; }
        public Nullable<byte> APILevel { get; set; }
        public Nullable<float> MinAPI { get; set; }
        public Nullable<float> MaxAPI { get; set; }
        public Nullable<float> APIAdj { get; set; }
    }
}
