//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class CalcMaintIndex_Result
    {
        public string FactorSet { get; set; }
        public string Currency { get; set; }
        public Nullable<float> RoutIndex { get; set; }
        public Nullable<float> RoutMatlIndex { get; set; }
        public Nullable<float> RoutEffIndex { get; set; }
        public Nullable<float> TAIndex { get; set; }
        public Nullable<float> TAMatlIndex { get; set; }
        public Nullable<float> TAEffIndex { get; set; }
        public float MaintIndex { get; set; }
        public float MaintMatlIndex { get; set; }
        public Nullable<float> MaintEffIndex { get; set; }
    }
}
