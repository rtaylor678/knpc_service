//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class DS_Currency_LU_Result
    {
        public string Description { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }
        public string KCurrency { get; set; }
        public string CurrencyCode { get; set; }
    }
}
