//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.dbs.Dbo
{
    using System;
    
    public partial class CalcAverageFactors_Result
    {
        public string FactorSet { get; set; }
        public Nullable<double> AvgEDC { get; set; }
        public Nullable<double> AvgUEDC { get; set; }
        public Nullable<double> SumUEDC { get; set; }
        public Nullable<double> UtilPcnt { get; set; }
        public Nullable<double> AvgProcessEDC { get; set; }
        public Nullable<double> AvgProcessUEDC { get; set; }
        public Nullable<double> ProcessUtilPcnt { get; set; }
        public Nullable<double> UtilOSTA { get; set; }
        public Nullable<double> EnergyUseDay { get; set; }
        public Nullable<double> AvgStdEnergy { get; set; }
        public Nullable<double> EII { get; set; }
        public Nullable<double> LossGainBpD { get; set; }
        public Nullable<double> AvgStdGainBpD { get; set; }
        public Nullable<double> VEI { get; set; }
        public Nullable<double> Complexity { get; set; }
        public Nullable<double> NonMaintPersEffDiv { get; set; }
        public Nullable<double> MaintPersEffDiv { get; set; }
        public Nullable<double> PersEffDiv { get; set; }
        public Nullable<double> MaintEffDiv { get; set; }
        public Nullable<double> AvgAnnNonMaintPersEffDiv { get; set; }
        public Nullable<double> AvgAnnMaintPersEffDiv { get; set; }
        public Nullable<double> AvgAnnPersEffDiv { get; set; }
        public Nullable<double> AvgAnnMaintEffDiv { get; set; }
    }
}
