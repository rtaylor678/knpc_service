﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.Pearl
{
    public class Knpc
    {
        private Dictionary<string, string> _dictionary = null;

        public string FindDboFieldname(string stagingPropertyValue)
        {
            string result = string.Empty;
            _dictionary.TryGetValue(stagingPropertyValue, out result);
            return result;
        }

        public Knpc()
        {
            _dictionary = new Dictionary<string, string>();
            _dictionary.Add("CatalystsDHYT", "CatalystsDHYT");
            _dictionary.Add("CatalystsFCCAdditives", "CatalystsFCCAdditives");
            _dictionary.Add("CatalystsFCCECat", "CatalystsFCCECat");
            _dictionary.Add("CatalystsFCCECatSales", "CatalystsFCCECatSales");
            _dictionary.Add("CatalystsFCCFresh", "CatalystsFCCFresh");
            _dictionary.Add("CatalystsHYC", "CatalystsHYC");
            _dictionary.Add("CatalystsHYG", "CatalystsHYG");
            _dictionary.Add("CatalystsNKSHYT", "CatalystsNKSHYT");
            _dictionary.Add("CatalystsOth", "CatalystsOth");
            _dictionary.Add("CatalystsPetChem", "CatalystsPetChem");
            _dictionary.Add("CatalystsREF", "CatalystsREF");
            _dictionary.Add("CatalystsRHYT", "CatalystsRHYT");
            _dictionary.Add("CatalystsS2Plant", "CatalystsS2Plant");
            _dictionary.Add("CatalystsVHYT", "CatalystsVHYT");
            _dictionary.Add("ChemicalsAlkyAcid", "ChemicalsAlkyAcid");
            _dictionary.Add("ChemicalsAmines", "ChemicalsAmines");
            _dictionary.Add("ChemicalsASESolv", "ChemicalsASESolv");
            _dictionary.Add("ChemicalsClay", "ChemicalsClay");
            _dictionary.Add("ChemicalsDieselAdd", "ChemicalsDieselAdd");
            _dictionary.Add("ChemicalsGasAdd", "ChemicalsGasAdd");
            _dictionary.Add("ChemicalsH2OTreat", "ChemicalsH2OTreat");
            _dictionary.Add("ChemicalsLube", "ChemicalsLube");
            _dictionary.Add("ChemicalsO2", "ChemicalsO2");
            _dictionary.Add("ChemicalsOth", "ChemicalsOth");
            _dictionary.Add("ChemicalsOthAcid", "ChemicalsOthAcid");
            _dictionary.Add("ChemicalsOthAdd", "ChemicalsOthAdd");
            _dictionary.Add("ChemicalsProcess", "ChemicalsProcess");
            _dictionary.Add("ChemicalsWasteH2O", "ChemicalsWasteH2O");
            _dictionary.Add("Cogen", "Cogen");
            _dictionary.Add("ContMaintInspect", "ContMaintInspect");
            _dictionary.Add("ContMaintLabor", "ContMaintLabor");
            _dictionary.Add("ContMaintMatl", "ContMaintMatl");
            _dictionary.Add("EnvirDisp", "EnvirDisp");
            _dictionary.Add("EnvirEng", "EnvirEng");
            _dictionary.Add("EnvirFines", "EnvirFines");
            _dictionary.Add("EnvirLab", "EnvirLab");
            _dictionary.Add("EnvirMonitor", "EnvirMonitor");
            _dictionary.Add("EnvirOth", "EnvirOth");
            _dictionary.Add("EnvirPermits", "EnvirPermits");
            _dictionary.Add("EnvirSpill", "EnvirSpill");
            _dictionary.Add("EquipMaint", "EquipMaint");
            _dictionary.Add("EquipNonMaint", "EquipNonMaint");
            _dictionary.Add("ExclEnvirFines", "ExclEnvirFines");
            _dictionary.Add("ExclOth", "ExclOth");
            _dictionary.Add("FireSafetyLoss", "ExclFireSafety");
            _dictionary.Add("GAPers", "GAPers");
            _dictionary.Add("InsurBI", "InsurBI");
            _dictionary.Add("InsurOth", "InsurOth");
            _dictionary.Add("InsurPnC", "InsurPC");
            _dictionary.Add("MaintMatl", "MaintMatl");
            _dictionary.Add("MPSBenInsur", "MPSBenInsur");
            _dictionary.Add("MPSBenPension", "MPSBenPension");
            _dictionary.Add("MPSBenStock", "MPSBenStock");
            _dictionary.Add("MPSBenSub", "MPSBenSub");
            _dictionary.Add("MPSBenTaxMed", "MPSBenTaxMed");
            _dictionary.Add("MPSBenTaxOth", "MPSBenTaxOth");
            _dictionary.Add("MPSBenTaxPen", "MPSBenTaxPen");
            _dictionary.Add("MPSSal", "MPSSal");
            _dictionary.Add("OCCBenInsur", "OCCBenInsur");
            _dictionary.Add("OCCBenPension", "OCCBenPension");
            _dictionary.Add("OCCBenStock", "OCCBenStock");
            _dictionary.Add("OCCBenSub", "OCCBenSub");
            _dictionary.Add("OCCBenTaxMed", "OCCBenTaxMed");
            _dictionary.Add("OCCBenTaxOth", "OCCBenTaxOth");
            _dictionary.Add("OCCBenTaxPen", "OCCBenTaxPen");
            _dictionary.Add("OCCSal", "OCCSal");
            _dictionary.Add("OthContAdmin", "OthCont");
            _dictionary.Add("OthContComputing", "OthContAdmin");
            _dictionary.Add("OthContConsult", "OthContComputing");
            _dictionary.Add("OthContFire", "OthContConsult");
            _dictionary.Add("OthContFoodSvc", "OthContFire");
            _dictionary.Add("OthContJan", "OthContFoodSvc");
            _dictionary.Add("OthContLab", "OthContJan");
            _dictionary.Add("OthContLegal", "OthContLab");
            _dictionary.Add("OthContOth", "OthContLegal");
            _dictionary.Add("OthContProcOp", "OthContOth");
            _dictionary.Add("OthContSecurity", "OthContProcOp");
            _dictionary.Add("OthContTransOp", "OthContSecurity");
            _dictionary.Add("OthContVacTrucks", "OthContTransOp");
            _dictionary.Add("OthNonVolComm", "OthNonVolComm");
            _dictionary.Add("OthNonVolComputer", "OthNonVolComputer");
            _dictionary.Add("OthNonVolDonations", "OthNonVolDonations");
            _dictionary.Add("OthNonVolDues", "OthNonVolDues");
            _dictionary.Add("OthNonVolExtraExpat", "OthNonVolExtraExpat");
            _dictionary.Add("OthNonVolNonContribPers", "OthNonVolNonContribPers");
            _dictionary.Add("OthNonVolNonPersSafety", "OthNonVolNonPersSafety");
            _dictionary.Add("OthNonVolOth", "OthNonVolOth");
            _dictionary.Add("OthNonVolSafety", "OthNonVolSafety");
            _dictionary.Add("OthNonVolSupply", "OthNonVolSupply");
            _dictionary.Add("OthNonVolTanks", "OthNonVolTanks");
            _dictionary.Add("OthNonVolTrain", "OthNonVolTrain");
            _dictionary.Add("OthNonVolTravel", "OthNonVolTravel");
            _dictionary.Add("OthRevenue", "OthRevenue");
            _dictionary.Add("OthVol", "OthVol");
            _dictionary.Add("OthVolEmissionsTaxes", "EmissionsTaxes");
            _dictionary.Add("PMAA", "PMAA");
            _dictionary.Add("POXO2", "POXO2");
            _dictionary.Add("PurElec", "PurElec");
            _dictionary.Add("PurFG", "PurFG");
            _dictionary.Add("PurLiquid", "PurLiquid");
            _dictionary.Add("PurOth", "PurOth");
            _dictionary.Add("PurOthH2O", "PurOthH2O");
            _dictionary.Add("PurOthN2", "PurOthN2");
            _dictionary.Add("PurSolid", "PurSolid");
            _dictionary.Add("PurSteam", "PurSteam");
            _dictionary.Add("RefProdFG", "RefProdFG");
            _dictionary.Add("RefProdOth", "RefProdOth");
            _dictionary.Add("Royalties", "Royalties");
            _dictionary.Add("Tax", "Tax");
            _dictionary.Add("ThirdPartyTerminalProd", "ThirdPartyTerminalProd");
            _dictionary.Add("ThirdPartyTerminalRM", "ThirdPartyTerminalRM");

        }
    }
}
