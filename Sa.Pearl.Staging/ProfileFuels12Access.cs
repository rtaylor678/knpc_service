﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;
using Sa.dbs.Dbo;

namespace Sa.Pearl
{
    public class ProfileFuels12Access<TEntity> where TEntity : class
    {
        #region Private members

        private DbContext context;
        private DbSet<TEntity> entityInstance;

        #endregion

        #region Constructors

        public ProfileFuels12Access(DbContext context)
        {
            this.context = context;
        }

        public ProfileFuels12Access()
        {
            this.context = new DboEntities();// ProfileFuels12.ProfileFuels12Dbs7Entities13(); //.ProfileFuels12Entities();
        }

        #endregion

        #region Properties

        public DbContext Context
        {
            get
            {
                return context;
            }
        }

        public DbSet<TEntity> EntityInstance
        {
            get
            {
                if (this.entityInstance == null)
                    this.entityInstance = context.Set<TEntity>();

                return this.entityInstance;
            }
        }

        #endregion

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> where)
        {
            //return Repository.GetAll(x => x.CustomerID == customerId).ToList();
            return EntityInstance.Where(where);
        }

        public IEnumerable<TEntity> Select(TEntity entity, int Id)
        {
            var submissioQuery = from subm in EntityInstance
                                 select subm;
            List<TEntity> submList = submissioQuery.ToList();
            return submList;
        }

        public void Insert(TEntity entity)
        {
            try
            {
                EntityInstance.Add(entity);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                if (ex.InnerException != null)
                    err += " | " + ex.InnerException.ToString();
                Logger.LogInfo(err); //so can see where the error happened; LogException won't do that right now.
                Logger.LogException(ex);
            }
        }

        public void Update(TEntity entity)
        {
            EntityInstance.Attach(entity);
            context.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            EntityInstance.Remove(entity);
            Context.SaveChanges();
        }
    }
}
