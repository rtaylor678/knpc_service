SETLOCAL ENABLEEXTENSIONS
rem https://msdn.microsoft.com/en-us/library/bfsktky3(v=vs.110).aspx

rem install the *.pfx file on the client (the computer connecting to solomon) - this is the private key identifying the client computer
rem install the *.cer file on the server (Trusted People Store) - this is the public key that will validate the identity of the client computer.

rem password: ac5a49ff-19ce-436b-a653-f240db70edf6

rem Create the private key (*pvk) and the public key (*.cer)
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\makecert.exe -r -pe -a sha512 -len 4096 ^
-n "CN=Sa.Pearl.Certificate" ^
-b 01/01/2015 -e 12/31/2015 ^
-cy authority ^
-sv "Sa.Pearl.Certificate.pvk" ^
"Sa.Pearl.Certificate.cer"

rem Create the Key Pair
C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\pvk2pfx.exe -pvk "Sa.Pearl.Certificate.pvk" -spc "Sa.Pearl.Certificate.cer" -pfx "Sa.Pearl.Certificate.pfx" -po ac5a49ff-19ce-436b-a653-f240db70edf6
