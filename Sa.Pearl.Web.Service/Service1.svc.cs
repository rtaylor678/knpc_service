﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using DbsDbo = Sa.dbs.Dbo;
using DbsDim = Sa.dbs.Dim;
using DbsOutput = Sa.dbs.Output;
using DbsStage = Sa.dbs.Stage;
using Sa.Pearl;
using System.Xml.Serialization;

namespace KNPCService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        public Service1()
        {
        }

        #region "CheckService"
        public string CheckService()
        {
            string machineName = Environment.MachineName;
            string domain = Environment.UserDomainName.ToString();
            Sa.Logger.LogInfo("CheckService() called by " + domain + @"\" + machineName);
            return "Wcf Service Successful: Domain = " + domain + Environment.NewLine + "Machine Name = " + machineName + Environment.NewLine;
        }
        #endregion "CheckService"

        #region "ValidateDatabaseConnection"

        public string ValidateDatabaseConnection()
        {
            return null;  //Per Dwane, this was for Developers only, shouldn't be available to Customer
            //string retVal = string.Empty;
            //DbConnectionCheck dbCheck = new DbConnectionCheck();
            //retVal = dbCheck.ValidateConnection(connectionString); // + "Database connection string:" + connectionString;
            //return retVal;
        }

        #endregion "ValidateDatabaseConnection"


        #region Send KNPC Data Test
        public List<string> SendKNPCDataTEST(string table, byte[] xmlstringBytes)
        {
            string abc = string.Empty;
            //GetReference(new byte[0], out abc);

            GetSubmission(new byte[4], new byte[4], out abc);
            byte[] salt = Encoding.UTF8.GetBytes("dog");
            byte[] key = { 248, 215, 80, 175, 235, 183, 19, 200, 195, 97, 212, 123, 110, 0, 192, 83, 148, 33, 49, 121, 147, 240, 138, 188, 113, 177, 209, 112, 145, 219, 53, 78 };
            byte[] vector = { 86, 102, 197, 82, 75, 101, 134, 69, 250, 94, 79, 187, 146, 83, 18, 128 };
            Encoding myEncoding = Encoding.ASCII;

            //TripleDesCryptor ts = new TripleDesCryptor(myEncoding);


            DataSet ds = new DataSet();

            //string buffer = cryptor.Decrypt(xmlstringBytes, salt.Length, key, vector);
            //string xml = DecompressData(Encoding.UTF8.GetBytes(buffer));

            // string buffer = ts.DecryptAsBytes(xmlstringBytes);

            //ds.ReadXml(XmlReader.Create(new StringReader(buffer)));
            //List<string> errors = SubmitData(table, ds);
            //return errors;
            return new List<string>();

        }
        #endregion

        #region Get Reference
        public byte[] GetReference(byte[] clientKeyBytes, out string statusMessage)
        {
            byte[] encryptedReferenceBytes;
            string clientKeyDecrypted;
            string refineryId = string.Empty;
            try
            {
                byte[] decryptedClientKey = Sa.Crypto.Decrypt(clientKeyBytes);
                clientKeyDecrypted = Sa.Serialization.ToObject<string>(decryptedClientKey);

                using (Sa.Pearl.PearlEntities db = new Sa.Pearl.PearlEntities())
                {
                    if (db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted) == null)
                    {
                        statusMessage = "Error in GetReference. Invalid Client Key.";
                        return new byte[0];
                    }
                    else
                    {
                        refineryId = db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted).Refnumber;
                    }
                }
                DbsDim.ShortDim shortDimList = new DbsDim.ShortDim();

                //Explanation: We originally made some new tables in the dim schema.
                //On promotion to Prod, we realized these tables already existed in dbo schema.
                //So we are repointing the dim DTO to the dbo DTO for the following tables:
                using (DbsDbo.DboEntities dbo = new DbsDbo.DboEntities())
                {
                    foreach (DbsDbo.Absence_LU absence_lu in dbo.Absence_LU)
                        shortDimList.Absence_LU.Add(new Sa.dbs.Dbo.Absence_LU { CategoryID = absence_lu.CategoryID, Description = absence_lu.Description });

                    foreach (DbsDbo.Crude_LU crude_lu in dbo.Crude_LU)
                        shortDimList.Crude_LU.Add(new Sa.dbs.Dbo.Crude_LU { CNum = crude_lu.CNum, CrudeName = crude_lu.CrudeName, ProdCountry = crude_lu.ProdCountry, TypicalAPI = crude_lu.TypicalAPI, TypicalSulfur = crude_lu.TypicalSulfur });

                    foreach (DbsDbo.Energy_LU energy_lu in dbo.Energy_LU)
                        shortDimList.Energy_LU.Add(new Sa.dbs.Dbo.Energy_LU { Composition = energy_lu.Composition, EnergySection = energy_lu.EnergySection, EnergyType = energy_lu.EnergyType, EnergyTypeDesc = energy_lu.EnergyTypeDesc, SortKey = decimal.ToInt32(energy_lu.SortKey.HasValue ? energy_lu.SortKey.Value : 0m), TransType = energy_lu.TransType, TransTypeDesc = energy_lu.TransTypeDesc });

                    foreach (DbsDbo.Material_LU material_lu in dbo.Material_LU)
                        shortDimList.Material_LU.Add(new Sa.dbs.Dbo.Material_LU { AllowInASP = material_lu.AllowInASP, AllowInCOKE = material_lu.AllowInCOKE, AllowInFCHEM = material_lu.AllowInFCHEM, AllowInFLUBE = material_lu.AllowInFLUBE, AllowInMPROD = material_lu.AllowInMPROD, AllowInOTHRM = material_lu.AllowInOTHRM, AllowInPROD = material_lu.AllowInPROD, AllowInRCHEM = material_lu.AllowInRCHEM, AllowInRLUBE = material_lu.AllowInRLUBE, AllowInRMI = material_lu.AllowInRMI, AllowInSOLV = material_lu.AllowInSOLV, MaterialID = material_lu.MaterialID, SAIName = material_lu.SAIName, TypicalDensity = material_lu.TypicalDensity, TankType = material_lu.TankType });

                    foreach (DbsDbo.Pers_LU pers_lu in dbo.Pers_LU)
                        shortDimList.Pers_LU.Add(new Sa.dbs.Dbo.Pers_LU { PersCode = short.Parse(pers_lu.PersCode.HasValue ? pers_lu.PersCode.Value.ToString() : "0"), PersID = pers_lu.PersID, SectionID = pers_lu.SectionID,  Description = pers_lu.Description });

                    foreach (DbsDbo.Opex_LU opex_lu in dbo.Opex_LU)
                        shortDimList.OpEx_LU.Add(new DbsDim.OpEx_LU { Description = opex_lu.Description, Property = opex_lu.OpexID, SortOrder = opex_lu.SortKey ?? 0 });

                    foreach (DbsDbo.TankType_LU tanktype_lu in dbo.TankType_LU)
                        shortDimList.TankType_LU.Add(new Sa.dbs.Dbo.TankType_LU { Description = tanktype_lu.Description, TankType = tanktype_lu.TankType });

                    foreach (DbsDbo.UOM uom in dbo.UOMs)
                        shortDimList.UOM.Add(new Sa.dbs.Dbo.UOM { UOMCode = uom.UOMCode, UOMDesc = uom.UOMDesc, UOMGroup = uom.UOMGroup });
                }

                using (DbsDim.DimEntities db = new DbsDim.DimEntities())
                {
                    /* We aren't using this anymore
                    foreach (DbsDim.CptlCode_LU cptlcode_lu in db.CptlCode_LU)
                        shortDimList.CptlCode_LU.Add(cptlcode_lu);
                    */
                    foreach (DbsDim.EmissionType_LU emissiontype_lu in db.EmissionType_LU)
                        shortDimList.EmissionType_LU.Add(emissiontype_lu);

                    foreach (DbsDim.MaterialProp_LU materialProp_lu in db.MaterialProp_LU)
                        shortDimList.MaterialProp_LU.Add(materialProp_lu);

                    foreach (DbsDim.PressureRange_LU pressurerange_lu in db.PressureRange_LU)
                        shortDimList.PressureRange_LU.Add(pressurerange_lu);

                    foreach (DbsDim.Process_LU process_lu in db.Process_LU)
                        shortDimList.Process_LU.Add(process_lu);

                    foreach (DbsDim.ProductGrade_LU productgrade_lu in db.ProductGrade_LU)
                        shortDimList.ProductGrade_LU.Add(productgrade_lu);

                    foreach (DbsDim.ProductProperties_LU productProperties_lu in db.ProductProperties_LU)
                        shortDimList.ProductProperties_LU.Add(productProperties_lu);

                    foreach (DbsDim.ValidValuesList_LU validValuesList_lu in db.ValidValuesList_LU)
                        shortDimList.ValidValuesList_LU.Add(validValuesList_lu);

                    foreach (DbsDim.YieldCategory_LU yieldcategory_lu in db.YieldCategory_LU)
                        shortDimList.YieldCategory_LU.Add(yieldcategory_lu);

                    foreach (DbsDim.FHFuelType_LU fHFuelType_LU in db.FHFuelType_LU)
                        shortDimList.FHFuelType_LU.Add(fHFuelType_LU);

                    foreach (DbsDim.FHProcessFluid_LU fHProcessFluid_LU in db.FHProcessFluid_LU)
                        shortDimList.FHProcessFluid_LU.Add(fHProcessFluid_LU);

                    foreach (DbsDim.FHService_LU fHService_LU in db.FHService_LU)
                        shortDimList.FHService_LU.Add(fHService_LU);


                    byte[] referenceBytes = Sa.Serialization.ToByteArray(shortDimList);
                    encryptedReferenceBytes = Sa.Crypto.Encrypt(referenceBytes);

                    try
                    {
                        //in future, should move this kind of code to another project so changes/additions can be tested there in isolation.
                        DbsOutput.OutputDbAccess<DbsOutput.Message> outputDbAccess = new DbsOutput.OutputDbAccess<DbsOutput.Message>();
                        outputDbAccess.BeginTransaction();
                        DbsOutput.Message message = outputDbAccess.GetAll(x => x.RefineryID == refineryId).FirstOrDefault();
                        if (message != null)
                        {
                            if (message.MessageType.Trim().ToUpper() == "REFERENCEDATACHANGED")
                            {
                                message.MessageValue="false";
                                message.tsInserted = DateTime.Now;
                                message.tsInsertedUser=Assembly.GetExecutingAssembly().FullName;
                            }
                        }
                        outputDbAccess.Update(message);
                        outputDbAccess.Commit();
                    }
                    catch (Exception exOutput)
                    {
                        Sa.Logger.LogException(exOutput);   
                    }

                    statusMessage = string.Empty;
                    return encryptedReferenceBytes;
                }
            }
            catch (Exception exDim)
            {
                Exception errLocation = new Exception("Error in KNPC WCF Service.GetReference");
                Sa.Logger.LogException(errLocation);
                Sa.Logger.LogException(exDim);
                statusMessage = "Error in GetReference. Please try again later or contact Solomon Associates.";
                return new byte[0];
            }

        }
        #endregion

        #region Get KPI
        public byte[] GetKPI(byte[] clientSubmissionIDBytes, byte[] clientKeyBytes, out string statusMessage)
        {
            byte[] encryptedKPIBytes;
            int clientSubmissionIDDecrypted;
            string clientKeyDecrypted;
            try
            {

                byte[] decrypedid = Sa.Crypto.Decrypt(clientSubmissionIDBytes);
                clientSubmissionIDDecrypted = Sa.Serialization.ToObject<int>(decrypedid);

                byte[] decryptedClientKey = Sa.Crypto.Decrypt(clientKeyBytes);
                clientKeyDecrypted = Sa.Serialization.ToObject<string>(decryptedClientKey);


                int dboSubmissionId = -1;
                string refineryId = string.Empty;
                using (Sa.Pearl.PearlEntities db = new Sa.Pearl.PearlEntities())
                {
                    if (db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted) == null)
                    {
                        statusMessage = "Error in GetKPI. Invalid Client Key.";
                        return new byte[0];
                    }
                    else
                    {
                        var record = db.RefnumberSubmissionMaps
                            .OrderByDescending(s => s.ClientKeysMapID)
                            .Where( s => (s.Failed == null || s.Failed != "Y")
                            &&  s.ClientSubmissionId == clientSubmissionIDDecrypted)
                            .FirstOrDefault();

                        if (record == null)
                        {
                            throw new Exception("Unable to find submission for Client Submission Id " + clientSubmissionIDDecrypted);
                        }
                        dboSubmissionId = record.DboSubmissionId;
                        refineryId = record.Refnumber;
                    }
                }
                string dboSubmissionID = dboSubmissionId.ToString();
                //do we need to get dbosubmission where it is not 'don't use' (meaning it = 1) and it matches clientsubmission key in [pearl].[RefnumberSubmissionMap]; order by submission id desc ????

                using (DbsOutput.OutputEntities db = new DbsOutput.OutputEntities())
                {
                    DbsOutput.ShortOutput shortoutput = new DbsOutput.ShortOutput();

                    foreach (DbsOutput.DataSubmission dataSubmission in db.DataSubmissions.Where(s => s.SubmissionID == dboSubmissionId))
                    {
                        shortoutput.DataSubmissions.Add(
                            new DbsOutput.SubmissionData
                            {
                                CalcTime = dataSubmission.CalcTime,
                                DataKey = dataSubmission.DataKey,
                                NumberValue = dataSubmission.NumberValue,
                                StudyMethodology = dataSubmission.StudyMethodology,
                                SubmissionID =clientSubmissionIDDecrypted, 
                                TextValue = dataSubmission.TextValue,
                                DateVal = dataSubmission.DateVal,
                                GroupKey = refineryId
                            });
                    }
                    foreach (DbsOutput.DataTable datum in db.DataTables.Where(s => s.DataKey == dboSubmissionID))
                        shortoutput.DataTables.Add(datum);

                    foreach (DbsOutput.DataKey datakey in db.DataKeys.Where(s => s.DataKey1 == dboSubmissionID))
                        shortoutput.DataKeys.Add(datakey);

                    foreach (DbsOutput.GroupKey groupkey in db.GroupKeys.Where(s => s.GroupKey1 == dboSubmissionID))
                        shortoutput.GroupKeys.Add(groupkey);

                    foreach (DbsOutput.ProcessData processData in db.ProcessDatas.Where(s => s.SubmissionID == dboSubmissionId))
                    {
                        processData.SubmissionID = clientSubmissionIDDecrypted;
                        processData.GroupKey = refineryId;
                        shortoutput.ProcessDatas.Add(processData);
                    }

                    foreach (DbsOutput.Message message in db.Messages.Where(s => s.RefineryID == refineryId))
                        shortoutput.Messages.Add(message);

                    byte[] KPIBytes = Sa.Serialization.ToByteArray(shortoutput);
                    encryptedKPIBytes = Sa.Crypto.Encrypt(KPIBytes);

                    statusMessage = string.Empty;
                    return encryptedKPIBytes;
                }
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                statusMessage = "Error in GetKPI. Please try again later or contact Solomon Associates.";
                return new byte[0];
            }

        }
        #endregion

        #region Get Status
        public byte[] GetStatus()
        {
            return new byte[5];
        }
        #endregion

        #region Get Submission
        public byte[] GetSubmission(byte[] clientSubmissionIDBytes, byte[] clientKeyBytes, out string statusMessage)
        {
            string clientKeyDecrypted;
            int clientSubmissionIDDecrypted;
            try
            {
                byte[] decrypedid = Sa.Crypto.Decrypt(clientSubmissionIDBytes);
                clientSubmissionIDDecrypted = Sa.Serialization.ToObject<int>(decrypedid);

                byte[] decryptedClientKey = Sa.Crypto.Decrypt(clientKeyBytes);
                clientKeyDecrypted = Sa.Serialization.ToObject<string>(decryptedClientKey);

                using (Sa.Pearl.PearlEntities db = new Sa.Pearl.PearlEntities())
                {
                    if (db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted) == null)
                    {
                        statusMessage = "Error in GetSubmission. Invalid Client Key.";
                        return new byte[0];
                    }
                    else
                    {
                        if (db.ClientKeys_CurrentSubmissions.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted && s.ClientSubmissionId== clientSubmissionIDDecrypted) == null)
                        {
                            statusMessage = "Error in GetSubmission. Invalid Client Submission ID.";
                            return new byte[0];
                        }
                    }
                }

                using (DbsStage.StageEntities db = new DbsStage.StageEntities())
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    db.Configuration.LazyLoadingEnabled = false;

                    //wrong, it returns SOlomon submission ID  int clientSubmissionID = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).SubmissionID;
                    int solomonSubmissionID = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).SubmissionID;
                    DbsStage.Submission submission = new DbsStage.Submission();

                    submission.SubmissionID = clientSubmissionIDDecrypted;
                    submission.ClientSubmissionID = clientSubmissionIDDecrypted;
                    submission.RefineryID = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).RefineryID;
                    submission.PeriodBeg = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).PeriodBeg;
                    submission.UOM = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).UOM;
                    submission.RptCurrency = db.Submissions.FirstOrDefault(s => s.ClientSubmissionID == clientSubmissionIDDecrypted).RptCurrency;

                    foreach (DbsStage.Absence absence in db.Absences.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Absences.Add(absence);

                    foreach (DbsStage.Comment comment in db.Comments.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Comments.Add(comment);

                    foreach (DbsStage.Config config in db.Configs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Configs.Add(config);

                    foreach (DbsStage.ConfigBuoy configbuoy in db.ConfigBuoys.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.ConfigBuoys.Add(configbuoy);

                    foreach (DbsStage.ConfigR configr in db.ConfigRS.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.ConfigRS.Add(configr);

                    foreach (DbsStage.Crude crude in db.Crudes.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Crudes.Add(crude);

                    foreach (DbsStage.Diesel diesel in db.Diesels.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Diesels.Add(diesel);

                    foreach (DbsStage.Emission emission in db.Emissions.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Emissions.Add(emission);

                    foreach (DbsStage.Energy energy in db.Energies.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Energies.Add(energy);

                    foreach (DbsStage.FinProdProp finProdProp in db.FinProdProps.Where(s => s.SubmissionId == solomonSubmissionID))
                        submission.FinProdProps.Add(finProdProp);

                    foreach (DbsStage.FiredHeater firedheater in db.FiredHeaters.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.FiredHeaters.Add(firedheater);

                    foreach (DbsStage.Gasoline gasoline in db.Gasolines.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Gasolines.Add(gasoline);

                    foreach (DbsStage.GeneralMisc generalmisc in db.GeneralMiscs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.GeneralMiscs.Add(generalmisc);

                    foreach (DbsStage.Inventory inventory in db.Inventories.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Inventories.Add(inventory);

                    foreach (DbsStage.Kerosene kerosene in db.Kerosenes.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Kerosenes.Add(kerosene);

                    foreach (DbsStage.LPG lpg in db.LPGs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.LPGs.Add(lpg);

                    foreach (DbsStage.MaintRout maintrout in db.MaintRouts.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MaintRouts.Add(maintrout);

                    foreach (DbsStage.MaintTA maintta in db.MaintTAs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MaintTAs.Add(maintta);

                    foreach (DbsStage.MarineBunker marinebunker in db.MarineBunkers.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MarineBunkers.Add(marinebunker);

                    foreach (DbsStage.MExp mexp in db.MExps.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MExps.Add(mexp);

                    foreach (DbsStage.MiscInput miscinput in db.MiscInputs.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.MiscInputs.Add(miscinput);

                    foreach (DbsStage.OpEx opex in db.OpExes.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.OpExes.Add(opex);

                    foreach (DbsStage.Personnel personnel in db.Personnels.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Personnels.Add(personnel);

                    foreach (DbsStage.ProcessData processdata in db.ProcessDatas.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.ProcessDatas.Add(processdata);

                    foreach (DbsStage.Resid resid in db.Resids.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Resids.Add(resid);

                    foreach (DbsStage.RPFResid rpfresid in db.RPFResids.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.RPFResids.Add(rpfresid);

                    foreach (DbsStage.Steam steam in db.Steams.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Steams.Add(steam);

                    foreach (DbsStage.Yield yield in db.Yields.Where(s => s.SubmissionID == solomonSubmissionID))
                        submission.Yields.Add(yield);

                    byte[] submissionBytes = Sa.Serialization.ToByteArray(submission);
                    byte[] encryptedSubmissionBytes = Sa.Crypto.Encrypt(submissionBytes);

                    statusMessage = string.Empty;
                    return encryptedSubmissionBytes;
                }
            }
            catch (Exception)
            {
                statusMessage = "Error in GetSubmission. Please try again later or contact Solomon Associates.";
                return new byte[0];
            }
        }
        #endregion

        #region Submit Data
        public string SubmitData(byte[] SubmissionBytes, byte[] clientKeyBytes)
        {
            Sa.Logger.LogInfo("Entering Service.svc.cs.SubmitData()");
            string clientKeyDecrypted;
            string status = string.Empty;
            string refnum = string.Empty;
            int stagingSubmissionid;
            int dboSubmissionId = -1;
            int clientSubmissionId;
            bool success = false;
            int xRefRecordId = -1;
            Sa.Pearl.StagingToDbo stagingToDbo = new Sa.Pearl.StagingToDbo();
            try
            {
                byte[] unencryptedSubmissionBytes = Sa.Crypto.Decrypt(SubmissionBytes);
                DbsStage.Submission submission = Sa.Serialization.ToObject<DbsStage.Submission>(unencryptedSubmissionBytes);

                //save their last submission for troubleshooting in case of problems
                
                try
                {
                    if (ConfigurationManager.AppSettings["SerializeClientSubmissions"].ToString().ToLower().Trim() == "true")
                    {
                        //I tried various ways to do this better, like converting classes to json and then to XDocument,
                        // but because of the way the classes were set up (circular references to Submission class),
                        // nothing better will work than to leverage code from Sa.Pearl.Client:
                        Sa.Pearl.SerializeSubmission serializer = new SerializeSubmission();
                        string name =  "KnpcSubmission" + DateTime.Now.ToString("MM-dd-yyyy hh-mm-ss") + ".xml";
                        serializer.WriteToXml(ConfigurationManager.AppSettings["SerializeClientSubmissionsPath"].ToString() + name, submission, false);
                        //MakeXMLFile(ConfigurationManager.AppSettings["SerializeClientSubmissionsPath"].ToString(), submission);
                    }
                }
                catch
                { }
                
                byte[] decryptedClientKey = Sa.Crypto.Decrypt(clientKeyBytes);
                clientKeyDecrypted = Sa.Serialization.ToObject<string>(decryptedClientKey);
                Sa.Logger.LogInfo("Decryption done");
                using (Sa.Pearl.PearlEntities db = new Sa.Pearl.PearlEntities())
                {
                    if (db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted) == null)
                    {
                        Sa.Logger.LogException(new Exception("Error in SubmitData. Invalid Client Key: " + clientKeyDecrypted));
                        status = "Error in SubmitData. Invalid Client Key.";
                        return status;
                    }
                    else
                    {
                        refnum = db.ClientKeys_Current.FirstOrDefault(s => s.ClientKey == clientKeyDecrypted).Refnumber;
                    }
                }
                

                using (var db = new DbsStage.StageEntities())
                {
                    Sa.Logger.LogInfo("Preparing to create new StageEntities.submission record.");
                    try
                    {
                        db.Database.Connection.Open();
                        //submission.PeriodEnd = null;
                        submission.tsInserted = DateTimeOffset.Now;
                        Assembly ai = Assembly.GetExecutingAssembly();
                        submission.tsInsertedApp = ai.GetName().Name + " (" + ai.GetName().Version + ")";
                        submission.tsInsertedHost = System.Environment.MachineName;
                        submission.tsInsertedUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                        db.Submissions.Add(submission);
                        try
                        {
                            db.Database.BeginTransaction();
                            db.SaveChanges();
                            db.Database.CurrentTransaction.Commit();
                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException validEx)
                        {
                            //in case of Entity Framework Validation Errors, add this as a watch:  ((System.Data.Entity.Validation.DbEntityValidationException)$exception).EntityValidationErrors
                            Exception exStaging = new Exception("DbEntityValidationException Error moving KNPC data to Staging for refnum " + refnum);
                            Sa.Logger.LogException(exStaging);
                            Sa.Logger.LogException(validEx);
                            status = "There was a problem moving your data in the Solomon database.";
                            return status;
                        }
                        catch (System.Data.Entity.Infrastructure.DbUpdateException insertEx)
                        {
                            Exception exStaging = new Exception("DbUpdateException Error moving KNPC data to Staging for refnum " + refnum);
                            //look in insertEx.base.base.base.InnerException.InnerException - or, check this in watches:  (((System.Exception)(insertEx)).InnerException).InnerException
                            Sa.Logger.LogException(exStaging);
                            Sa.Logger.LogException(insertEx);
                            status = "There was a problem moving your data in the Solomon database.";
                            return status;
                        }
                        catch (Exception saveChangesEx)
                        {
                            Exception exStaging = new Exception("Error moving KNPC data to Staging for refnum " + refnum);
                            Sa.Logger.LogException(exStaging);
                            Sa.Logger.LogException(saveChangesEx);
                            status = "There was a problem moving your data in the Solomon database.";
                            return status;
                        }
                        stagingSubmissionid = submission.SubmissionID;
                        clientSubmissionId = submission.ClientSubmissionID;
                    }
                    catch (Exception exStagingMove)
                    {
                        Exception exStaging = new Exception("Error moving KNPC data to Staging for refnum " + refnum);
                        Sa.Logger.LogException(exStaging);
                        Sa.Logger.LogException(exStagingMove);
                        status = "There was a problem moving your data in the Solomon database.";
                        return status;
                    }
                }

                success = stagingToDbo.MoveStagingToDbo(stagingSubmissionid, refnum, ref dboSubmissionId);

                //insert submission info into pearl x-ref table
                
                Sa.Logger.LogInfo("Going to enter record into RefnumberSubmissionMap");
                try
                {
                    using (var db = new Sa.Pearl.PearlEntities())
                    {
                        db.Database.Connection.Open();
                        Sa.Pearl.RefnumberSubmissionMap rnsm = new Sa.Pearl.RefnumberSubmissionMap();
                        rnsm.Refnumber = refnum;
                        rnsm.StgSubmissionId = stagingSubmissionid;
                        rnsm.ClientSubmissionId = clientSubmissionId;
                        rnsm.DboSubmissionId = dboSubmissionId;
                        rnsm.Failed = null; //success? "N" : "Y";
                        db.RefnumberSubmissionMaps.Add(rnsm);
                        db.SaveChanges();
                        xRefRecordId = rnsm.ClientKeysMapID;
                    }
                }
                catch (Exception submissionMapException)
                {
                    Sa.Logger.LogException(new Exception("Error writing to Sa.Pearl.RefnumberSubmissionMap table for Staging submission " + stagingSubmissionid.ToString()));
                    Sa.Logger.LogException(submissionMapException);
                    status = "There was a problem moving your data in the Solomon database.";
                    return status;
                }
                if (!success)
                {
                    UpdateFailedValueInXrefTable(xRefRecordId, true);
                    Exception exStaging = new Exception("Error moving KNPC data from Staging to Dbo for Staging submission " + stagingSubmissionid.ToString());
                    Sa.Logger.LogException(exStaging);
                    status = "There was a problem moving your data in the Solomon database.";
                    return status;
                }

                //check flag to see if should run calcs after failure or not.           
                bool runCalcsEvenIfThereWasAFailure = false;
                try
                {
                    Boolean.TryParse(ConfigurationManager.AppSettings["RunCalcsEvenIfThereWasAFailure"].ToString(), out runCalcsEvenIfThereWasAFailure);
                }
                catch (Exception exRunCalcsEvenIfThereWasAFailure)
                {
                    runCalcsEvenIfThereWasAFailure = false;
                }
                if (success || runCalcsEvenIfThereWasAFailure)
                {
                    //run calcs. Db will update the xref table.
                    Sa.Logger.LogInfo("Going to call stagingToDbo.callSpCompleteSubmission for submission " + dboSubmissionId.ToString());
                    //StagingToDbo stagingToDbo = new StagingToDbo();
                    if (!stagingToDbo.callSpCompleteSubmission(dboSubmissionId, false))
                    {
                        //error already logged in callSpCompleteSubmission
                        status = "There was a problem while running your calculations in the Solomon database.";
                        success = false; //return status;
                    }
                }
                else
                {
                    //not running calcs, so update the xref table now.
                    UpdateFailedValueInXrefTable(xRefRecordId, !success);
                }

                //DB will run calcs then run the EtlOutput, then update the XRef table
                /*
                if (success)
                {
                    using (var db = new Sa.dbs.Dbo.DboEntities())
                    {
                        db.Database.Connection.Open();
                        try
                        {
                            //call dbs.dbo.[dbo].[EtlOutput] to move from dbo to output.                         
                            string methodology = ConfigurationManager.AppSettings["Methodology"].ToString();
                            object cursor = db.EtlOutput(methodology, dboSubmissionId);
                        }
                        catch (Exception exEtl)
                        {
                            Exception exMethodology = new Exception("Error getting Methodology setting from app.config in SubmitData()");
                            Sa.Logger.LogException(exMethodology);
                            Sa.Logger.LogException(exEtl);
                            status = "There was a problem moving your data in the Solomon database.";
                            UpdateFailedValueInXrefTable(xRefRecordId, true);
                            return status;
                        }
                    }
                }
                */
            }
            catch (Exception exSubmitData)
            {
                Sa.Logger.LogException(exSubmitData);
                status = "Error in SubmitData. Please try again later or contact Solomon Associates.";
            }



            return status;
        }

        private void UpdateFailedValueInXrefTable(int xRefRecordId, bool failed)
        {
            Sa.Logger.LogInfo("Going to update record in RefnumberSubmissionMap");
                try
                {
                    Sa.Pearl.PearlAccess<RefnumberSubmissionMap> pearlAccess = new PearlAccess<RefnumberSubmissionMap>();
                    RefnumberSubmissionMap rnsm = pearlAccess.Get(x => x.ClientKeysMapID == xRefRecordId);
                    rnsm.Failed = failed ? "Y" : "N";
                    pearlAccess.Update(rnsm);
                }
                catch (Exception submissionMapException)
                {
                    Sa.Logger.LogException(new Exception("Error writing to Sa.Pearl.RefnumberSubmissionMap table for RefId " +
                        xRefRecordId));
                    Sa.Logger.LogException(submissionMapException);
                }
        }
/*
        private void MakeXMLFile(String filepath, Sa.dbs.Stage.Submission clientSubmission)
        {
            try
            {
                XmlWriterSettings setting = new XmlWriterSettings();
                setting.ConformanceLevel = ConformanceLevel.Auto;
                using (XmlWriter writer = XmlWriter.Create(filepath, setting))
                {
                    writer.WriteStartElement("Submission");//start of the file
                    //write the elements that belong just to the Submission (have to write these separately)
                    foreach (PropertyInfo propertyInfo in this.GetType().GetProperties().Where(propertyInfo => propertyInfo.GetGetMethod().GetParameters().Count() == 0))
                    {
                        if (!propertyInfo.PropertyType.ToString().Contains("ICollection"))//holy cow this was hard to figure out how to do. there has to be an easier way to know if it is a collection or not
                        {
                            switch (propertyInfo.Name.ToString())
                            {
                                case "SubmissionID":
                                case "RefineryID":
                                case "UOM":
                                case "RptCurrency":
                                    writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(this, null) ?? string.Empty).ToString());
                                    break;
                                case "PeriodBeg"://because it needs to be written as Date
                                    writer.WriteElementString("Date", (propertyInfo.GetValue(this, null) ?? string.Empty).ToString());
                                    break;
                                default://any other field doesn't get written to the XML
                                    break;
                            }
                        }
                    }
                    //then go through each table and add them one at a time (got to be a better way to do this, right?)
                    foreach (Sa.dbs.Stage.Absence a in clientSubmission.Absences) { WriteXML(a, "Absence", writer); }
                    foreach (Sa.dbs.Stage.Comment c in clientSubmission.Comments) { WriteXML(c, "Comment", writer); }
                    foreach (Sa.dbs.Stage.Config c in clientSubmission.Configs) { WriteXML(c, "Config", writer); }
                    foreach (Sa.dbs.Stage.ConfigBuoy a in clientSubmission.ConfigBuoys) { WriteXML(a, "ConfigBuoy", writer); }
                    foreach (Sa.dbs.Stage.ConfigR a in clientSubmission.ConfigRS) { WriteXML(a, "ConfigRS", writer); }
                    foreach (Sa.dbs.Stage.Crude a in clientSubmission.Crudes) { WriteXML(a, "Crude", writer); }
                    foreach (Sa.dbs.Stage.Diesel a in clientSubmission.Diesels) { WriteXML(a, "Diesel", writer); }
                    foreach (Sa.dbs.Stage.Emission a in clientSubmission.Emissions) { WriteXML(a, "Emission", writer); }
                    foreach (Sa.dbs.Stage.Energy a in clientSubmission.Energies) { WriteXML(a, "Energy", writer); }
                    foreach (Sa.dbs.Stage.FiredHeater a in clientSubmission.FiredHeaters) { WriteXML(a, "FiredHeater", writer); }
                    foreach (Sa.dbs.Stage.FinProdProp a in clientSubmission.FinProdProps) { WriteXML(a, "FinProdProp", writer); }
                    foreach (Sa.dbs.Stage.Gasoline g in clientSubmission.Gasolines) { WriteXML(g, "Gasoline", writer); }
                    foreach (Sa.dbs.Stage.GeneralMisc a in clientSubmission.GeneralMiscs) { WriteXML(a, "GeneralMisc", writer); }
                    foreach (Sa.dbs.Stage.Inventory a in clientSubmission.Inventories) { WriteXML(a, "Inventory", writer); }
                    foreach (Sa.dbs.Stage.Kerosene a in clientSubmission.Kerosenes) { WriteXML(a, "Kerosene", writer); }
                    foreach (Sa.dbs.Stage.LPG a in clientSubmission.LPGs) { WriteXML(a, "LPG", writer); }
                    foreach (Sa.dbs.Stage.MaintRout a in clientSubmission.MaintRouts) { WriteXML(a, "MaintRout", writer); }
                    foreach (Sa.dbs.Stage.MaintTA a in clientSubmission.MaintTAs) { WriteXML(a, "MaintTA", writer); }
                    foreach (Sa.dbs.Stage.MarineBunker a in clientSubmission.MarineBunkers) { WriteXML(a, "MarineBunker", writer); }
                    foreach (Sa.dbs.Stage.MExp a in clientSubmission.MExps) { WriteXML(a, "MExp", writer); }
                    foreach (Sa.dbs.Stage.MiscInput a in clientSubmission.MiscInputs) { WriteXML(a, "MiscInput", writer); }
                    foreach (Sa.dbs.Stage.OpEx a in clientSubmission.OpExes) { WriteXML(a, "OpEx", writer); }
                    foreach (Sa.dbs.Stage.Personnel a in clientSubmission.Personnels) { WriteXML(a, "Personnel", writer); }
                    foreach (Sa.dbs.Stage.ProcessData a in clientSubmission.ProcessDatas) { WriteXML(a, "ProcessData", writer); }
                    foreach (Sa.dbs.Stage.Resid r in clientSubmission.Resids) { WriteXML(r, "Resid", writer); }
                    foreach (Sa.dbs.Stage.RPFResid a in clientSubmission.RPFResids) { WriteXML(a, "RPFResid", writer); }
                    foreach (Sa.dbs.Stage.Steam a in clientSubmission.Steams) { WriteXML(a, "Steam", writer); }
                    foreach (Sa.dbs.Stage.Yield a in clientSubmission.Yields) { WriteXML(a, "Yield", writer); }
                    //and write the end of the file
                    writer.WriteEndElement();
                    writer.Flush();
                }
            }
            catch (Exception)
            {
            }

        }

        private static void WriteXML<T>(T a, string name, XmlWriter writer)
        {
            //generic method to write each table to XML
            writer.WriteStartElement(name);
            foreach (PropertyInfo p in a.GetType().GetProperties())
            {
                if (p.Name.ToString() != "Submission")
                {
                    writer.WriteElementString(p.Name.ToString(), (p.GetValue(a, null) ?? string.Empty).ToString());
                }
            }

            writer.WriteEndElement();

        }
*/
        #endregion

        #region Create Data Table
        public static DataTable CreateDataTable<T>(string tableName)
        {
            var dt = new DataTable(tableName);

            var propList = typeof(T).GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

            foreach (MemberInfo info in propList)
            {
                if (info is PropertyInfo)
                    //dt.Columns.Add(new DataColumn(info.Name, (info as PropertyInfo).PropertyType));
                    //dt.Columns.Add(info.Name, Nullable.GetUnderlyingType((info as PropertyInfo).PropertyType) ?? (info as PropertyInfo).PropertyType);
                    dt.Columns.Add(info.Name);
                else if (info is FieldInfo)
                    dt.Columns.Add(new DataColumn(info.Name, (info as FieldInfo).FieldType));
            }

            return dt;
        }
        #endregion

        #region Fill Data Table
        public static void FillDataTable<T>(DataTable dt, List<T> items)
        {
            var propList = typeof(T).GetMembers(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

            foreach (T t in items)
            {
                var row = dt.NewRow();
                foreach (MemberInfo info in propList)
                {
                    if (info is PropertyInfo)
                    {
                        row[info.Name] = (info as PropertyInfo).GetValue(t, null);
                    }
                    else if (info is FieldInfo)
                        row[info.Name] = (info as FieldInfo).GetValue(t);
                }
                dt.Rows.Add(row);
            }
        }
        #endregion

    }

}
