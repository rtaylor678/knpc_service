//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Sa.Pearl.Web.Archive___ReferenceData
{
    public partial class Material_LU
    {
        public string MaterialID { get; set; }
        public string SAIName { get; set; }
        public Nullable<short> SortKey { get; set; }
        public string PricingID { get; set; }
        public string PVPNSA { get; set; }
        public string PVPEUR { get; set; }
        public string PVPPAC { get; set; }
        public string SummaryCategory { get; set; }
        public string MPRODSumCat { get; set; }
        public string OTHRMSumCat { get; set; }
        public Nullable<byte> SensHeatCriteria { get; set; }
        public string SpecProd { get; set; }
        public Nullable<short> BPSortKey { get; set; }
        public string SAINotes { get; set; }
        public Nullable<float> TypicalDensity { get; set; }
        public byte AllowInRMI { get; set; }
        public byte AllowInOTHRM { get; set; }
        public byte AllowInRCHEM { get; set; }
        public byte AllowInRLUBE { get; set; }
        public byte AllowInPROD { get; set; }
        public byte AllowInFLUBE { get; set; }
        public byte AllowInFCHEM { get; set; }
        public byte AllowInASP { get; set; }
        public byte AllowInCOKE { get; set; }
        public byte AllowInSOLV { get; set; }
        public byte AllowInMPROD { get; set; }
        public byte LubesOnly { get; set; }
        public bool AllowInRMB { get; set; }
    }
    
}
