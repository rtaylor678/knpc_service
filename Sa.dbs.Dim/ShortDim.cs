﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sa.dbs.Dbo;

namespace Sa.dbs.Dim
{
    [Serializable]
    public class ShortDim
    {

        public virtual ICollection<Sa.dbs.Dbo.Absence_LU> Absence_LU { get; set; }
        //public virtual ICollection<CptlCode_LU> CptlCode_LU { get; set; }
        public virtual ICollection<Sa.dbs.Dbo.Crude_LU> Crude_LU { get; set; }
        public virtual ICollection<EmissionType_LU> EmissionType_LU { get; set; }
        public virtual ICollection<Sa.dbs.Dbo.Energy_LU> Energy_LU { get; set; }
        public virtual ICollection<Sa.dbs.Dbo.Material_LU> Material_LU { get; set; }
        public virtual ICollection<MaterialProp_LU> MaterialProp_LU { get; set; }
        public virtual ICollection<OpEx_LU> OpEx_LU { get; set; }
        public virtual ICollection<Sa.dbs.Dbo.Pers_LU> Pers_LU { get; set; }
        public virtual ICollection<PressureRange_LU> PressureRange_LU { get; set; }
        public virtual ICollection<Process_LU> Process_LU { get; set; }
        public virtual ICollection<ProductGrade_LU> ProductGrade_LU { get; set; }
        public virtual ICollection<ProductProperties_LU> ProductProperties_LU { get; set; }
        public virtual ICollection<Sa.dbs.Dbo.TankType_LU> TankType_LU { get; set; }
        public virtual ICollection<Sa.dbs.Dbo.UOM> UOM { get; set; }
        public virtual ICollection<ValidValuesList_LU> ValidValuesList_LU { get; set; }
        public virtual ICollection<YieldCategory_LU> YieldCategory_LU { get; set; }        
        public virtual ICollection<FHFuelType_LU> FHFuelType_LU { get; set; }
        public virtual ICollection<FHProcessFluid_LU> FHProcessFluid_LU { get; set; }
        public virtual ICollection<FHService_LU> FHService_LU { get; set; }

        public ShortDim()
        {
            this.Absence_LU = new HashSet<Sa.dbs.Dbo.Absence_LU>();
            //this.CptlCode_LU = new HashSet<CptlCode_LU>();
            this.Crude_LU = new HashSet<Sa.dbs.Dbo.Crude_LU>();
            this.EmissionType_LU = new HashSet<EmissionType_LU>();
            this.Energy_LU = new HashSet<Sa.dbs.Dbo.Energy_LU>();
            this.Material_LU = new HashSet<Sa.dbs.Dbo.Material_LU>();
            this.MaterialProp_LU = new HashSet<MaterialProp_LU>();
            this.OpEx_LU = new HashSet<OpEx_LU>();
            this.Pers_LU = new HashSet<Sa.dbs.Dbo.Pers_LU>();
            this.PressureRange_LU = new HashSet<PressureRange_LU>();
            this.Process_LU = new HashSet<Process_LU>();
            this.ProductGrade_LU = new HashSet<ProductGrade_LU>();
            this.ProductProperties_LU = new HashSet<ProductProperties_LU>();
            this.TankType_LU = new HashSet<Sa.dbs.Dbo.TankType_LU>();
            this.UOM = new HashSet<Sa.dbs.Dbo.UOM>();
            this.ValidValuesList_LU = new HashSet<ValidValuesList_LU>();
            this.YieldCategory_LU = new HashSet<YieldCategory_LU>();
            this.FHFuelType_LU = new HashSet<FHFuelType_LU>();
            this.FHProcessFluid_LU = new HashSet<FHProcessFluid_LU>();
            this.FHService_LU = new HashSet<FHService_LU>();
        }
        
    }
}
