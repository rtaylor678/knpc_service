﻿CREATE PROCEDURE [dim].[DS_Absence_LU]
AS
SELECT
	[CategoryID]	= RTRIM([l].[CategoryID]),
	[Description]	= RTRIM([l].[Description]),
	[l].[SortKey],
	[l].[IncludeForInput],
	[ParentID]		= RTRIM([l].[ParentID]),
	[Indent]		= RTRIM([l].[Indent]),
	[DetailStudy]	= RTRIM([l].[DetailStudy]),
	[DetailProfile]	= RTRIM([l].[DetailProfile])
FROM
	[dbo].[Absence_LU]	[l]
ORDER BY
	[l].[SortKey];
