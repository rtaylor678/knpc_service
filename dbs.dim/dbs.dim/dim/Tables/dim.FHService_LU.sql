﻿CREATE TABLE [dim].[FHService_LU] (
    [Service]     VARCHAR (8)  NOT NULL,
    [Description] VARCHAR (64) NOT NULL,
    CONSTRAINT [PK_FHService_LU] PRIMARY KEY CLUSTERED ([Service] ASC),
    CONSTRAINT [CL_FHService_LU_Description] CHECK ([Description]<>''),
    CONSTRAINT [CL_FHService_LU_Service] CHECK ([Service]<>'')
);

