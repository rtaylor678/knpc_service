﻿CREATE TABLE [dim].[MaterialProp_LU] (
    [MaterialId]  VARCHAR (5)   NOT NULL,
    [PropertyId]  INT           NOT NULL,
    [Required]    BIT           NULL,
    [LookupGroup] VARCHAR (20)  NULL,
    [SortKey]     INT           NULL,
    [IndentLvl]   TINYINT       NULL,
    [BegEffDate]  SMALLDATETIME NULL,
    [EndEffDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK_MaterialProp_LU] PRIMARY KEY CLUSTERED ([MaterialId] ASC, [PropertyId] ASC)
);

