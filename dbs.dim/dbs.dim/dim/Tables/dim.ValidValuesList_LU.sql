﻿CREATE TABLE [dim].[ValidValuesList_LU] (
    [Group]   VARCHAR (20)  NOT NULL,
    [ListValue]  VARCHAR (20)  NOT NULL,
    [Description] NVARCHAR (50) NULL,
    [SortKey]     INT           NULL,
    [BegEffDate]  SMALLDATETIME NULL,
    [EndEffDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK_ValidValuesList_LU] PRIMARY KEY CLUSTERED ([Group] ASC, [ListValue] ASC)
);

