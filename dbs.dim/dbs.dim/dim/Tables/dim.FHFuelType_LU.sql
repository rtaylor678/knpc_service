﻿CREATE TABLE [dim].[FHFuelType_LU] (
    [FuelType]    VARCHAR (8)  NOT NULL,
    [Description] VARCHAR (64) NOT NULL,
    CONSTRAINT [PK_FHFuelType_LU] PRIMARY KEY CLUSTERED ([FuelType] ASC),
    CONSTRAINT [CL_FHFuelType_LU_Description] CHECK ([Description]<>''),
    CONSTRAINT [CL_FHFuelType_LU_Service] CHECK ([FuelType]<>''),
    CONSTRAINT [UX_FHFuelType_LU_Description] UNIQUE NONCLUSTERED ([Description] ASC)
);

