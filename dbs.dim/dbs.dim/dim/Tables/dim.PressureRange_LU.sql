﻿CREATE TABLE [dim].[PressureRange_LU] (
    [PressureRange] VARCHAR (8) NOT NULL,
    CONSTRAINT [PK_PressureRange] PRIMARY KEY CLUSTERED ([PressureRange] ASC),
    CONSTRAINT [CL_PressureRange_LU_PressureRange] CHECK ([PressureRange]<>'')
);

