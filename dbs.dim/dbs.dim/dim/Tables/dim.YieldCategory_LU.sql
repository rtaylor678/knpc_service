﻿CREATE TABLE [dim].[YieldCategory_LU] (
    [Category]    VARCHAR (8)  NOT NULL,
    [Description] VARCHAR (64) NOT NULL,
    CONSTRAINT [PK_YieldCategory_LU] PRIMARY KEY CLUSTERED ([Category] ASC),
    CONSTRAINT [CL_YieldCategory_LU_Description] CHECK ([Description]<>''),
    CONSTRAINT [CL_YieldCategory_LU_YieldCategory] CHECK ([Category]<>''),
    CONSTRAINT [UX_YieldCategory_LU_Description] UNIQUE NONCLUSTERED ([Description] ASC)
);

