﻿CREATE TABLE [dim].[EmissionType_LU] (
    [EmissionType] VARCHAR (8)  NOT NULL,
    [Description]  VARCHAR (64) NOT NULL,
    CONSTRAINT [PK_EmissionType_LU] PRIMARY KEY CLUSTERED ([EmissionType] ASC),
    CONSTRAINT [CL_EmissionType_LU_Description] CHECK ([Description]<>''),
    CONSTRAINT [CL_EmissionType_LU_EmissionType] CHECK ([EmissionType]<>'')
);

