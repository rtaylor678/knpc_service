﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Sa.Pearl;


namespace Sa.Pearl.Client.Ux
{
    //[Serializable]
    public partial class Form1 : Form
    {
        //these are standard objects we create
        ClientSubmission submission = new ClientSubmission();
        ClientKPIs KPIs = new ClientKPIs();
        ClientReferences reference = new ClientReferences();
        String clientKey = "";
        string applicationMode = string.Empty;
        public Form1()
        {
            InitializeComponent();
            //openFileDialog1.Filter = "key files (*.key)|*.key|All files (*.*)|*.*";
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            #if DEBUG
            {
                applicationModeToolStripMenuItem.Visible = true;
                label4.Visible = true;
                lblMode.Visible = true;
            }
            #endif
                                    
        }

        public void CloseButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void btnClearData_Click(object sender, EventArgs e)
        {
            ClearSubmissionData();
        }

        private void ClearSubmissionData()
        {
            //clear all data from the current submission object
            lstStatus.Items.Clear();
            lstStatus.Items.Add("Clearing all data");
            lstStatus.Refresh();

            List<String> results = submission.FlushSubmission();

            UpdateStatus("Clear All Data", results);
        }

        public void LoadRefineryDataButton_Click(object sender, EventArgs e)
        {
            //load some mock data into the submission object (this is not complete, it is just a demo of how it should be done)
            try
            {
                lstStatus.Items.Clear();
                lstStatus.Items.Add("Loading refinery");

                Absence a = new Absence();
                a.AbsenceID = 1;
                a.Category = "ABC";
                a.MPSAbs = 5.5f;
                a.OCCAbs = 3.0f;
                submission.Absences.Add(a);

                a = new Absence();
                a.AbsenceID = 2;
                a.Category = "DEF";
                a.MPSAbs = 2.5f;
                a.OCCAbs = 1.0f;
                submission.Absences.Add(a);

                Config c = new Config();
                c.ConfigID = 1;
                c.EnergyPcnt = 50;
                c.UtilPcnt = 75;

                submission.Configs.Add(c);

                Resid r = new Resid();
                r.BlendID = 10;
                r.Grade = "GRADE";
                r.PourPt = 15.0f;

                submission.Resids.Add(r);

                lstStatus.Items.Add("Data Load Complete");

            }
            catch (Exception ex)
            {
                lstStatus.Items.Add("Error Loading refinery");
                lstStatus.Items.Add(ex.Message);
            }

        }

        public void SubmitRefineryDataFileButton_Click(object sender, EventArgs e)
        {
            //submit the submission object to the database
            lstStatus.Items.Clear();
            lstStatus.Items.Add("Submitting data");
            lstStatus.Refresh();

            List<String> results = submission.SubmitData(clientKey);

            UpdateStatus("Submit Refinery Data", results);

        }

        public void btnChooseClientKey_Click(object sender, EventArgs e)
        {
            //looking for the text file containing the appropriate plant key, and taking the first row as the key
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "key files (*.key)|*.key|All files (*.*)|*.*";

            DialogResult dr = openFileDialog.ShowDialog();
            if (dr == DialogResult.OK)
            {
                lblClientKey.Text = openFileDialog.FileName;
                System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog.FileName);
                clientKey =sr.ReadLine(); // System.IO.File.(openFileDialog.FileName);
                sr.Close();

            }
        }

        public void GetReferenceDataButton_Click(object sender, EventArgs e)
        {
            //retrieve reference data from the database, put it into the refs object
            lstStatus.Items.Clear();
            lstStatus.Items.Add("Requesting Reference data");
            lstStatus.Refresh();

            List<String> results = reference.RetrieveReferences(clientKey);

            UpdateStatus("Get Reference Data", results);

        }

        public void btnRetrieveSubmission_Click(object sender, EventArgs e)
        {
            //retrieve the specified submission from the database, put it in the submission object
            lstStatus.Items.Clear();
            lstStatus.Items.Add("Requesting Submission data");
            lstStatus.Refresh();

            List<String> results = submission.RetrieveSubmission(txtSubmissionID.Text, clientKey);

            UpdateStatus("Retrieve Submission", results);

        }

        public void RequestKPIResultsButton_Click(object sender, EventArgs e)
        {
            //retrieve the specified KPIs from the database, put them in the KPIs object
            lstStatus.Items.Clear();
            lstStatus.Items.Add("Requesting KPI data");
            lstStatus.Refresh();

            List<String> results = KPIs.RetrieveKPIs(txtSubmissionID.Text, clientKey);

            UpdateStatus("Request KPI Results", results);

        }

        public void UpdateStatus(string statusType, List<string> results)
        {
            //populate the status box with appropriate values
            //lstStatus.Items.Clear();
            if (results != null)
            {
                if (results.Count == 0)
                {
                    lstStatus.Items.Add("Success in " + statusType);
                }
                else
                {
                    lstStatus.Items.Add("Errors in " + statusType);
                    foreach (String er in results)
                    {
                        lstStatus.Items.Add(er);
                    }
                }
            }
            lstStatus.Refresh();
        }

        private void btnLoadSavePath_Click(object sender, EventArgs e)
        {
            //looking for the text file containing the appropriate plant key, and taking the first row as the key
            OpenFileDialog openFileDialog = new OpenFileDialog();

            DialogResult dr = openFileDialog.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtLoadSavePath.Text = openFileDialog.FileName;
                lstStatus.Items.Clear();
                lstStatus.Refresh();

            }
        }

        private void btnLoadFromXML_Click(object sender, EventArgs e)
        {
            if (txtLoadSavePath.Text == "")
            {
                lstStatus.Items.Clear();
                lstStatus.Items.Add("You must choose a path before you can load from it.");
                lstStatus.Refresh();
                return;
            }
            ClearSubmissionData();
            lstStatus.Items.Clear();
            lstStatus.Items.Add("Loading XML");
            lstStatus.Refresh();

            List<String> results = submission.LoadSubmissionFromXML(txtLoadSavePath.Text);

            UpdateStatus("Load from XML", results);

        }

        private void btnSaveToXML_Click(object sender, EventArgs e)
        {
            if (txtLoadSavePath.Text == "")
            {
                lstStatus.Items.Clear();
                lstStatus.Items.Add("You must choose a path before you can save to it.");
                lstStatus.Refresh();
                return;
            }

            lstStatus.Items.Clear();
            lstStatus.Items.Add("Writing data to XML");
            lstStatus.Refresh();

            List<String> results = submission.SaveSubmissionToXML(txtLoadSavePath.Text);

            UpdateStatus("Write data to XML", results);
        }

        private void btnSaveReferenceToXML_Click(object sender, EventArgs e)
        {
            if (txtLoadSavePath.Text == "")
            {
                lstStatus.Items.Clear();
                lstStatus.Items.Add("You must choose a path before you can save to it.");
                lstStatus.Refresh();
                return;
            }

            lstStatus.Items.Clear();
            lstStatus.Items.Add("Writing reference to XML");
            lstStatus.Refresh();

            List<String> results = reference.SaveReferenceDataToXML(txtLoadSavePath.Text);

            UpdateStatus("Write reference to XML", results);

        }

        private void btnSaveKPIsToXML_Click(object sender, EventArgs e)
        {
            if (txtLoadSavePath.Text == "")
            {
                lstStatus.Items.Clear();
                lstStatus.Items.Add("You must choose a path before you can save to it.");
                lstStatus.Refresh();
                return;
            }

            lstStatus.Items.Clear();
            lstStatus.Items.Add("Writing KPIs to XML");
            lstStatus.Refresh();

            List<String> results = KPIs.SaveKPIsToXML(txtLoadSavePath.Text);

            UpdateStatus("Write KPIs to XML", results);
        }

        private void checkWcfServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string response =  submission.CheckService();
            MessageBox.Show(response);
            
        }

 
        private void validateDBToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            string response = submission.CheckDatabase();
            MessageBox.Show(response);
        }

        private void localToolStripMenuItem_Click(object sender, EventArgs e)
        {
            submission = new ClientSubmission("Local");
            reference = new ClientReferences("Local");
            KPIs = new ClientKPIs("Local");
            this.lblMode.Text = "Local";
        }

        private void devToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Development environment is not currently coded and available");
            //submission = new ClientSubmission("Dev");
            //reference = new ClientReferences("Dev");
            //KPIs = new ClientKPIs("Dev");
            //this.lblMode.Text = "Dev";
        }

        private void qAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("QA environment is not currently coded and available");
            //submission = new ClientSubmission("QA");
            //reference = new ClientReferences("QA");
            //KPIs = new ClientKPIs("QA");
            //this.lblMode.Text = "QA";

        }

        private void prodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            submission = new ClientSubmission("Prod");
            reference = new ClientReferences("Prod");
            KPIs = new ClientKPIs("Prod");
            this.lblMode.Text = "Prod";
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void checkDbConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            return ;  //Per Dwane, this was for Developers only, shouldn't be available to Customer
            //string response = submission.CheckDatabase();
            //MessageBox.Show(response);
        }

        private void logTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
            try
            {
                int s = 0;
                int t = 1 / s;
            }
            catch (Exception exDim)
            {
                Sa.Logger.LogException(exDim);
                MessageBox.Show("Done. Entry saved to file.");
                
            }
        }

    }

}
