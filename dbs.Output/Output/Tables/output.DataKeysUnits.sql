﻿CREATE TABLE [output].[DataKeysUnits](
	[DataKey] [varchar](40) NOT NULL,
	[DataLabel] [varchar](255) NOT NULL,
	[DataType] [varchar](10) NOT NULL,
	[UOM] [varchar](20) NULL,
	[Currency] [varchar](4) NULL,
	[PricingBasis] [varchar](20) NULL,
 CONSTRAINT [PK_DataKeysUnits] PRIMARY KEY CLUSTERED 
(
	[DataKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [output].[DataKeys]  WITH CHECK ADD  CONSTRAINT [CL_DataKeysUnits_Currency] CHECK  (([Currency]<>''))
GO

ALTER TABLE [output].[DataKeys] CHECK CONSTRAINT [CL_DataKeysUnits_Currency]
GO

ALTER TABLE [output].[DataKeys]  WITH CHECK ADD  CONSTRAINT [CL_DataKeysUnits_DataKey] CHECK  (([DataKey]<>''))
GO

ALTER TABLE [output].[DataKeys] CHECK CONSTRAINT [CL_DataKeysUnits_DataKey]
GO

ALTER TABLE [output].[DataKeys]  WITH CHECK ADD  CONSTRAINT [CL_DataKeysUnits_DataLabel] CHECK  (([DataLabel]<>''))
GO

ALTER TABLE [output].[DataKeys] CHECK CONSTRAINT [CL_DataKeysUnits_DataLabel]
GO

ALTER TABLE [output].[DataKeys]  WITH CHECK ADD  CONSTRAINT [CL_DataKeysUnits_DataType] CHECK  (([DataType]<>''))
GO

ALTER TABLE [output].[DataKeys] CHECK CONSTRAINT [CL_DataKeysUnits_DataType]
GO

ALTER TABLE [output].[DataKeys]  WITH CHECK ADD  CONSTRAINT [CL_DataKeysUnits_PricingBasis] CHECK  (([PricingBasis]<>''))
GO

ALTER TABLE [output].[DataKeys] CHECK CONSTRAINT [CL_DataKeysUnits_PricingBasis]
GO

ALTER TABLE [output].[DataKeys]  WITH CHECK ADD  CONSTRAINT [CL_DataKeysUnits_UOM] CHECK  (([UOM]<>''))
GO

ALTER TABLE [output].[DataKeys] CHECK CONSTRAINT [CL_DataKeysUnits_UOM]
GO