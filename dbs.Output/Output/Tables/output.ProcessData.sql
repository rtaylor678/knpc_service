﻿CREATE TABLE [output].[ProcessData](
	--[GroupKey] [varchar](40) NOT NULL,
	[SubmissionID] [int] NOT NULL, --new
	[DataKey] [varchar](40) NOT NULL,
	[UnitID] [int] NOT NULL,
	[StudyMethodology] [int] NOT NULL,
	[NumberValue] [real] NULL,
	[TextValue] [varchar](256) NULL,
	[CalcTime] [smalldatetime] NOT NULL, 
    CONSTRAINT [PK_ProcessData] PRIMARY KEY ([SubmissionID], [DataKey], [UnitID])
	--OLD: CONSTRAINT [PK_ProcessData] PRIMARY KEY ([GroupKey], [DataKey], [UnitID])
)

