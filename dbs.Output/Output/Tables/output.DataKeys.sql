﻿CREATE TABLE [output].[DataKeys]
(
	[DataKey]				VARCHAR(40)				NOT	NULL,	CONSTRAINT [CL_DataKeys_DataKey]		CHECK([DataKey] <> ''),

	[DataLabel]				VARCHAR(255)			NOT	NULL,	CONSTRAINT [CL_DataKeys_DataLabel]		CHECK([DataLabel] <> ''),
	[DataType]				VARCHAR(10)				NOT	NULL,	CONSTRAINT [CL_DataKeys_DataType]		CHECK([DataType] <> ''),
	[UOM]					VARCHAR(20)					NULL,	CONSTRAINT [CL_DataKeys_UOM]			CHECK([UOM] <> ''),
	[Currency]				VARCHAR(4)					NULL,	CONSTRAINT [CL_DataKeys_Currency]		CHECK([Currency] <> ''),
	[PricingBasis]			VARCHAR(20)					NULL,	CONSTRAINT [CL_DataKeys_PricingBasis]	CHECK([PricingBasis] <> ''),

	CONSTRAINT [PK_DataKeys]	PRIMARY KEY CLUSTERED([DataKey] ASC)
);