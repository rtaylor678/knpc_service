﻿CREATE TABLE [output].[DataSubmission]
(
	[SubmissionID]			INT						NOT	NULL	 CONSTRAINT [FK_DataSubmission_Submission]	REFERENCES [dbo].[SubmissionsAll]([SubmissionID]),
	[DataKey]				VARCHAR(40)				NOT	NULL	CONSTRAINT [FK_DataSubmission_DataKeys]		REFERENCES [output].[DataKeys]([DataKey]),
	[StudyMethodology]		INT						NOT	NULL,

	[NumberValue]			REAL						NULL,
	[TextValue]				VARCHAR(256)				NULL,	--not a good idea here: CONSTRAINT [CL_DataSubmission_TextValue]			CHECK([TextValue] <> ''),
	[DateVal]				DATE						NULL,		--[DateValue] is invalid column name
	[CalcTime]				SMALLDATETIME			NOT	NULL,

	CONSTRAINT [PK_DataSubmission]	PRIMARY KEY CLUSTERED([SubmissionID] ASC, [DataKey] ASC, [StudyMethodology] ASC)
);