﻿CREATE TABLE [output].[CalcTime]
(
	[GroupKey]			VARCHAR(20)		NOT	NULL	CHECK([GroupKey] <> ''),
	[CalcTime]			DATETIME		NOT	NULL,

	PRIMARY KEY CLUSTERED ([GroupKey] ASC)
);