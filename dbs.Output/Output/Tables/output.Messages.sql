﻿CREATE TABLE [output].[Messages]
(
	[Id] INT IDENTITY PRIMARY KEY,
	[RefineryID]		CHAR(6) NOT NULL,
	[MessageType]		VARCHAR(200) NULL,
	[MessageValue]		VARCHAR(200) NULL, 
	[tsInserted]		datetime NOT NULL DEFAULT (GetDate()),
	[tsInsertedUser] [nvarchar](128) NOT NULL DEFAULT (suser_sname()),
    CONSTRAINT [FK_messages_ToTSort] FOREIGN KEY ([RefineryID]) REFERENCES [dbo].[TSort]([RefineryID]) 
)
