﻿CREATE TABLE [output].[Data]
(
	[GroupKey]				VARCHAR(40)				NOT	NULL	CONSTRAINT [FK_Data_GroupKeys]	REFERENCES [output].[GroupKeys]([GroupKey]),
	[DataKey]				VARCHAR(40)				NOT	NULL	CONSTRAINT [FK_Data_DataKeys]	REFERENCES [output].[DataKeys]([DataKey]),
	[StudyMethodology]		INT						NOT	NULL,

	[NumberValue]			REAL						NULL,
	[TextValue]				VARCHAR(256)				NULL,	CONSTRAINT [CL_Data_TextValue]			CHECK([TextValue] <> ''),
	[CalcTime]				SMALLDATETIME			NOT	NULL,

	CONSTRAINT [PK_Data]	PRIMARY KEY CLUSTERED([GroupKey] ASC, [DataKey] ASC, [StudyMethodology] ASC)
);