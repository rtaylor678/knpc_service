﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sa.Pearl;
using stage = Sa.dbs.Stage;// Sa.Pearl.Staging;
using Sa.dbs.Dbo;

namespace Sa.Pearl.Staging.Test
{
    public class DboToStaging
    {
        //decided not to make this class static because Solomon submission ID can/will change
        //MUST call the SubmissionToStg(int profileFuelsSubmissionId) before the others !!!!
        private int _submissionId;
        private int _profileFuels12SubmissionId;
        /*
        public int SubmissionId
        {
            get { return _submissionId; }
            set { _submissionId = value; }
        }
        */

        public DboToStaging(int ProfileFUels12SubmissionId)
        {
            _profileFuels12SubmissionId = ProfileFUels12SubmissionId;

        }

        public ProfileFuels12.SubmissionsAll GetProfileFuels12SubmissionEntity(int submissionId)
        {
            try
            {
                ProfileFuels12TestAccess<ProfileFuels12.SubmissionsAll> pf12 = new ProfileFuels12TestAccess<ProfileFuels12.SubmissionsAll>();
                var query = pf12.GetAll(x => x.SubmissionID == submissionId).ToList();
                return query[0];
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                err += ex.InnerException == null ? "" : ex.InnerException.ToString();
                return null;
            }
        }

        public stage.Submission SubmissionToStg(int profileFuelsSubmissionId, int clientSubmissionID)
        {
            ProfileFuels12.SubmissionsAll dbo = GetProfileFuels12SubmissionEntity(profileFuelsSubmissionId);
            //Sa.dbs.Dbo.Submission dbo = GetProfileFuels12SubmissionEntity(profileFuelsSubmissionId);
            stage.Submission staging = new stage.Submission();
            //Can't do this, this is an Identity:  staging.SubmissionID = profileFuelsSubmissionId;

            if (dbo.PeriodStart != null)
            {
                staging.PeriodBeg = (DateTime)dbo.PeriodStart;
            }
            else
            {
                staging.PeriodBeg = DateTime.Today;
            }
            if (dbo.PeriodEnd != null)
            {
                staging.PeriodEnd = (DateTime)dbo.PeriodEnd;
            }
            else
            {
                staging.PeriodEnd = DateTime.Today;
            }
            //calculated:  staging.PeriodDuration_Days = (int?)dbo.NumDays;
            staging.RptCurrency = dbo.RptCurrency;
            staging.UOM = dbo.UOM;
            staging.RefineryID = dbo.RefineryID;
            staging.ClientSubmissionID = clientSubmissionID;
            //staging.Notes=
            //These don't exist in Staging
            //staging.CalcsNeeded = "Y";
            //staging.DataSet=dbo.DataSet;
            //staging.UseSubmission = true;
            if (dbo.Submitted != null)
            {
                staging.tsInserted = DateTimeOffset.Parse(dbo.Submitted.ToString());// DateTime.Today;
            }
            staging.tsInsertedApp = System.Reflection.Assembly.GetCallingAssembly().FullName.Leftmost(128);
            staging.tsInsertedHost = System.Reflection.Assembly.GetCallingAssembly().CodeBase.Leftmost(128);
            staging.tsInsertedUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Leftmost(128);

            staging.Absences = null;
            staging.ConfigBuoys = null;
            staging.ConfigRS = null;
            staging.Configs = null;
            staging.Crudes = null;
            staging.Diesels = null;
            staging.Emissions = null;
            staging.Energies = null;
            staging.FiredHeaters = null;
            staging.Gasolines = null;
            staging.GeneralMiscs = null;
            staging.Inventories = null;
            staging.Kerosenes = null;
            staging.LPGs = null;
            staging.MaintRouts = null;
            staging.MaintTAs = null;
            staging.MarineBunkers = null;
            staging.MExps = null;
            staging.MiscInputs = null;
            staging.Notes = null;
            staging.OpExes = null;
            staging.Personnels = null;
            staging.ProcessDatas = null;
            staging.Resids = null;
            staging.RPFResids = null;
            staging.Steams = null;
            staging.Yields = null;

            Sa.dbs.Stage.StagingDbAccess<stage.Submission> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Submission>(); //SubmissionstagingDbAccess 
            stagingDb.Insert(staging);

            _submissionId = staging.SubmissionID;

            stagingDb = null;
            return staging;
        }

        public stage.Submission GetStagingSubmissionEntity(int submissionId)
        {
            Sa.dbs.Stage.StagingDbAccess<stage.Submission> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Submission>();
            stage.Submission submission = new stage.Submission();
            var submissioQuery = stagingDb.GetAll(x => x.SubmissionID == submissionId);

            /*
                       var submissioQuery = from subm in _StDb.Submissions
                                 where subm.SubmissionID == submissionId
                                 select subm;
            */
            List<stage.Submission> submList = submissioQuery.ToList();
            return submList[0];
        }

        public void AbsenceToStg(ProfileFuels12.Absence dbo) //, stage.Submission stagingSubmission)
        {
            stage.Absence staging = new stage.Absence();
            staging.SubmissionID = _submissionId;
            staging.Category = dbo.CategoryID;
            staging.MPSAbs = dbo.MPSAbs;
            staging.OCCAbs = dbo.OCCAbs;
            //staging.Submission = stagingSubmission;
            Sa.dbs.Stage.StagingDbAccess<stage.Absence> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Absence>(); //SubmissionstagingDbAccess 
            stagingDb.Insert(staging);
            stagingDb = null;
        }


        public void ConfigToStg(ProfileFuels12.Config dbo)
        {
            stage.Config staging = new stage.Config();
            staging.AllocPcntOfCap = (float?)dbo.AllocPcntOfCap;
            staging.BlockOp = dbo.BlockOp;
            staging.Cap = dbo.Cap;
            staging.ControlType = dbo.ControlType;
            staging.EnergyPcnt = dbo.EnergyPcnt;
            //staging.InServicePcnt = dbo.InServiceCap;
            staging.InServicePcnt = (float?)dbo.InServicePcnt;
            staging.MHPerWeek = dbo.MHPerWeek;
            staging.PostPerShift = (float?)dbo.PostPerShift;
            staging.ProcessID = dbo.ProcessID;
            staging.ProcessType = dbo.ProcessType;
            staging.StmCap = dbo.StmCap;
            staging.StmUtilPcnt = (float?)dbo.StmUtilPcnt;
            staging.SubmissionID = _submissionId;
            staging.UnitID = dbo.UnitID;
            staging.UnitName = dbo.UnitName;
            staging.UtilPcnt = (float?)dbo.UtilPcnt;
            staging.YearsOper = (float?)dbo.YearsOper;
            Sa.dbs.Stage.StagingDbAccess<stage.Config> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Config>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }
        /*
        public void ConfigBuoyToStg(ProfileFuels12.ConfigBuoy dbo)
        {
            stage.ConfigBuoy staging = new stage.ConfigBuoy();
            staging.SubmissionID = _submissionId;
            staging.LineSize = dbo.LineSize;
            staging.PcntOwnership = dbo.PcntOwnership;
            staging.ProcessID = dbo.ProcessID;
            staging.ShipCap = dbo.ShipCap;
            staging.UnitID = dbo.UnitID;
            staging.UnitName = dbo.UnitName;
            Sa.dbs.Stage.StagingDbAccess<stage.ConfigBuoy> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.ConfigBuoy>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }
        */
        public void ConfigRS1ToStg(ProfileFuels12.ConfigR dbo)
        {
            stage.ConfigR staging = new stage.ConfigR();
            staging.AvgSize = dbo.AvgSize;
            staging.ProcessID = dbo.ProcessID;
            staging.ProcessType = dbo.ProcessType;
            staging.SubmissionID = _submissionId;
            staging.Throughput = dbo.Throughput;
            staging.UnitID = dbo.UnitID;
            Sa.dbs.Stage.StagingDbAccess<stage.ConfigR> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.ConfigR>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void CrudeToStg(ProfileFuels12.Crude dbo)
        {
            stage.Crude staging = new stage.Crude();
            staging.SubmissionID = _submissionId;
            staging.CrudeID = dbo.CrudeID;
            staging.BBL = (float?)dbo.BBL;
            staging.CNum = dbo.CNum;
            staging.CrudeName = dbo.CrudeName;
            staging.Gravity = dbo.Gravity;
            //staging.Period = dbo.Period;
            staging.Sulfur = dbo.Sulfur;
            Sa.dbs.Stage.StagingDbAccess<stage.Crude> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Crude>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void DieselToStg(ProfileFuels12.Diesel dbo)
        {
            stage.Diesel staging = new stage.Diesel();
            staging.SubmissionID = _submissionId;
            staging.BlendID = (int?)dbo.BlendID;
            staging.Grade = dbo.Grade;
            staging.Market = dbo.Market;
            staging.Type = dbo.Type;
            staging.Density = dbo.Density;
            staging.Cetane = dbo.Cetane;
            staging.PourPt = dbo.PourPt;
            staging.Sulfur = dbo.Sulfur;
            staging.CloudPt = dbo.CloudPt;
            staging.ASTM90 = dbo.ASTM90;
            staging.E350 = dbo.E350;
            staging.BiodieselPcnt = dbo.BiodieselPcnt;
            staging.KMT = dbo.KMT;
            Sa.dbs.Stage.StagingDbAccess<stage.Diesel> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Diesel>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void EmissionsToStg(ProfileFuels12.Emission dbo)
        {
            stage.Emission staging = new stage.Emission();
            staging.SubmissionID = _submissionId;
            staging.EmissionType = dbo.EmissionType;
            staging.RefEmissions = dbo.RefEmissions;
            Sa.dbs.Stage.StagingDbAccess<stage.Emission> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Emission>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void EnergyToStg(ProfileFuels12.Energy dbo)
        {
            stage.Energy staging = new stage.Energy();
            staging.SubmissionID = _submissionId;
            staging.EnergyType = dbo.EnergyType;
            staging.TransType = dbo.TransType;
            staging.Hydrogen = dbo.Hydrogen;
            staging.Methane = dbo.Methane;
            staging.Ethane = dbo.Ethane;
            staging.Ethylene = dbo.Ethylene;
            staging.Propane = dbo.Propane;
            staging.Propylene = dbo.Propylene;
            staging.Butane = dbo.Butane;
            staging.Isobutane = dbo.Isobutane;
            staging.Butylenes = dbo.Butylenes;
            staging.C5Plus = dbo.C5Plus;
            staging.CO = dbo.CO;
            staging.CO2 = dbo.CO2;
            staging.N2 = dbo.N2;
            //staging.MBTUOut = (float?)dbo.MBTUOut;
            staging.H2S = dbo.H2S;
            staging.NH3 = dbo.NH3;
            staging.SO2 = dbo.SO2;
            //staging.OverrideCalcs = dbo.OverrideCalcs;
            Sa.dbs.Stage.StagingDbAccess<stage.Energy> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Energy>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void FiredHeaterToStg(ProfileFuels12.FiredHeater dbo)
        {
            stage.FiredHeater staging = new stage.FiredHeater();
            staging.SubmissionID = _submissionId;
            staging.HeaterNo = (int?)dbo.HeaterNo;
            staging.UnitID = (int?)dbo.UnitID;
            staging.ProcessID = dbo.ProcessID;
            staging.HeaterName = dbo.HeaterName;
            staging.Service = dbo.Service;
            staging.ProcessFluid = dbo.ProcessFluid;
            staging.ThroughputRpt = dbo.ThroughputRpt;
            staging.ThroughputUOM = dbo.ThroughputUOM;
            staging.FiredDuty = dbo.FiredDuty;
            staging.FuelType = dbo.FuelType;
            staging.OthCombDuty = dbo.OthCombDuty;
            staging.FurnInTemp = dbo.FurnInTemp;
            staging.FurnOutTemp = dbo.FurnOutTemp;
            staging.StackTemp = dbo.StackTemp;
            staging.StackO2 = dbo.StackO2;
            staging.HeatLossPcnt = dbo.HeatLossPcnt;
            staging.CombAirTemp = dbo.CombAirTemp;
            staging.AbsorbedDuty = dbo.AbsorbedDuty;
            staging.ProcessDuty = dbo.ProcessDuty;
            staging.SteamDuty = dbo.SteamDuty;
            staging.SteamSuperHeated = dbo.SteamSuperHeated;
            staging.ShaftDuty = dbo.ShaftDuty;
            staging.OtherDuty = dbo.OtherDuty;
            Sa.dbs.Stage.StagingDbAccess<stage.FiredHeater> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.FiredHeater>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void GasolineToStg(ProfileFuels12.Gasoline dbo)
        {
            stage.Gasoline staging = new stage.Gasoline();
            staging.SubmissionID = _submissionId;
            staging.BlendID = (int?)dbo.BlendID;
            staging.Grade = dbo.Grade;
            staging.Market = dbo.Market;
            staging.Type = dbo.Type;
            staging.Density = dbo.Density;
            staging.RVP = dbo.RVP;
            staging.RON = dbo.RON;
            staging.MON = dbo.MON;
            staging.Oxygen = dbo.Oxygen;
            staging.Ethanol = dbo.Ethanol;
            staging.MTBE = dbo.MTBE;
            staging.ETBE = dbo.ETBE;
            staging.TAME = dbo.TAME;
            staging.OthOxygen = dbo.OthOxygen;
            staging.Sulfur = dbo.Sulfur;
            staging.Lead = dbo.Lead;
            staging.KMT = dbo.KMT;
            Sa.dbs.Stage.StagingDbAccess<stage.Gasoline> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Gasoline>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void GeneralMiscToStg(ProfileFuels12.GeneralMisc dbo)
        {
            stage.GeneralMisc staging = new stage.GeneralMisc();
            staging.SubmissionID = _submissionId;
            staging.FlareLossMT = dbo.FlareLossMT;
            staging.TotLossMT = dbo.TotLossMT;
            Sa.dbs.Stage.StagingDbAccess<stage.GeneralMisc> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.GeneralMisc>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void InventoryToStg(ProfileFuels12.Inventory dbo)
        {
            stage.Inventory staging = new stage.Inventory();
            staging.SubmissionID = _submissionId;
            staging.NumTank = (int?)dbo.NumTank;
            staging.AvgLevel = dbo.AvgLevel;
            staging.LeasedPcnt = dbo.LeasedPcnt;
            staging.MandStorage = (float?)dbo.MandStorage;
            staging.TankType = dbo.TankType;
            staging.TotStorage = (float?)dbo.TotStorage;
            Sa.dbs.Stage.StagingDbAccess<stage.Inventory> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Inventory>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void KeroseneToStg(ProfileFuels12.Kerosene dbo)
        {
            stage.Kerosene staging = new stage.Kerosene();
            staging.SubmissionID = _submissionId;
            staging.BlendID = (int?)dbo.BlendID;
            staging.Grade = dbo.Grade;
            staging.Type = dbo.Type;
            staging.Density = dbo.Density;
            staging.Sulfur = dbo.Sulfur;
            staging.KMT = dbo.KMT;
            Sa.dbs.Stage.StagingDbAccess<stage.Kerosene> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Kerosene>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void LPGToStg(ProfileFuels12.LPG dbo)
        {
            stage.LPG staging = new stage.LPG();
            staging.SubmissionID = _submissionId;
            staging.BlendID = (int?)dbo.BlendID;
            staging.MolOrVol = dbo.MolOrVol;
            staging.VolC2Lt = dbo.VolC2Lt;
            staging.VolC3 = dbo.VolC3;
            staging.VolC3ene = dbo.VolC3ene;
            staging.VoliC4 = dbo.VoliC4;
            staging.VolnC4 = dbo.VolnC4;
            staging.VolC4ene = dbo.VolC4ene;
            staging.VolC5Plus = dbo.VolC5Plus;
            Sa.dbs.Stage.StagingDbAccess<stage.LPG> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.LPG>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void MaintRoutToStg(ProfileFuels12.MaintRout dbo)
        {
            stage.MaintRout staging = new stage.MaintRout();
            staging.SubmissionID = _submissionId;
            staging.UnitID = (int?)dbo.UnitID;
            //staging.InServicePcnt = dbo.InServicePcnt;
            staging.ProcessID = dbo.ProcessID;
            if (dbo.RegNum != null)
                staging.RegNum = (int)dbo.RegNum;
            staging.RegDown = dbo.RegDown;
            if (dbo.MaintNum != null)
                staging.MaintNum = (int)dbo.MaintNum;
            staging.MaintDown = dbo.MaintDown;
            if (dbo.OthNum != null)
                staging.OthNum = (int?)dbo.OthNum;
            staging.OthDown = dbo.OthDown;
            staging.RoutCostLocal = dbo.RoutCostLocal;
            staging.RoutMatlLocal = dbo.RoutMatlLocal;
            staging.RoutCostUS = dbo.RoutCostUS;
            staging.RoutMatlUS = dbo.RoutMatlUS;
            staging.RoutMatlPcnt = dbo.RoutMatlPcnt;
            staging.OthDownEconomic = dbo.OthDownEconomic;
            staging.OthDownExternal = dbo.OthDownExternal;
            staging.OthDownUnitUpsets = dbo.OthDownUnitUpsets;
            staging.OthDownOffsiteUpsets = dbo.OthDownOffsiteUpsets;
            staging.OthDownOther = dbo.OthDownOther;
            staging.RoutExpLocal = dbo.RoutExpLocal;
            staging.RoutCptlLocal = dbo.RoutCptlLocal;
            staging.RoutOvhdLocal = dbo.RoutOvhdLocal;
            staging.OthSlow = dbo.OthSlow;
            if (dbo.UsePcntOfCost != null)
                staging.UsePcntOfCost = (float?)dbo.UsePcntOfCost;
            Sa.dbs.Stage.StagingDbAccess<stage.MaintRout> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.MaintRout>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void MaintTAToStg(ProfileFuels12.MaintTA dbo)
        {
            stage.MaintTA staging = new stage.MaintTA();
            staging.SubmissionID = _submissionId;
            staging.UnitID = (int?)dbo.UnitID;
            staging.TADate = dbo.TADate;

            staging.MaintTAID = dbo.TAID;
            
            staging.TAHrsDown = dbo.TAHrsDown;
            staging.TACostLocal = dbo.TACostLocal;
            staging.TACostUS = dbo.TACostUS;
            staging.TAMatlLocal = dbo.TAMatlLocal;
            staging.TAMatlUS = dbo.TAMatlUS;
            staging.TAOCCSTH = dbo.TAOCCSTH;
            staging.TAOCCOVT = dbo.TAOCCOVT;
            staging.TAMPSSTH = dbo.TAMPSSTH;
            staging.TAMPSOVTPcnt = dbo.TAMPSOVTPcnt;
            staging.TAContOCC = dbo.TAContOCC;
            staging.TAContMPS = dbo.TAContMPS;
            staging.PrevTADate = dbo.PrevTADate;
            staging.TAExceptions = (int?)dbo.TAExceptions;
            staging.ProcessID = dbo.ProcessID;
            staging.TAOvhdLocal = dbo.TAOvhdLocal;
            staging.TALaborCostLocal = dbo.TALaborCostLocal;
            staging.TACptlLocal = dbo.TACptlLocal;
            staging.TAExpLocal = dbo.TAExpLocal;
            Sa.dbs.Stage.StagingDbAccess<stage.MaintTA> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.MaintTA>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void MarineBunkerToStg(ProfileFuels12.MarineBunker dbo)
        {
            stage.MarineBunker staging = new stage.MarineBunker();
            staging.SubmissionID = _submissionId;
            staging.BlendID = (int?)dbo.BlendID;
            staging.CrackedStock = dbo.CrackedStock;
            staging.Density = dbo.Density;
            staging.PourPt = dbo.PourPt;
            staging.Sulfur = dbo.Sulfur;
            staging.KMT = dbo.KMT;
            staging.ViscCSAtTemp = dbo.ViscCSAtTemp;
            staging.ViscTemp = dbo.ViscTemp;
            staging.Grade = dbo.Grade;
            Sa.dbs.Stage.StagingDbAccess<stage.MarineBunker> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.MarineBunker>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void MexpToStg(ProfileFuels12.MExp dbo)
        {
            stage.MExp staging = new stage.MExp();
            staging.SubmissionID = _submissionId;
            staging.ComplexCptl = dbo.ComplexCptl;
            staging.NonRefExcl = dbo.NonRefExcl;
            staging.TAMaintCptl = dbo.TAMaintCptl;
            staging.RoutMaintCptl = dbo.RoutMaintCptl;
            staging.NonMaintInvestExp = dbo.NonMaintInvestExp;
            staging.NonRegUnit = dbo.NonRegUnit;
            staging.ConstraintRemoval = dbo.ConstraintRemoval;
            staging.RegExp = dbo.RegExp;
            staging.RegGaso = dbo.RegGaso;
            staging.RegDiesel = dbo.RegDiesel;
            staging.RegOth = dbo.RegOth;
            staging.SafetyOth = dbo.SafetyOth;
            staging.Energy = dbo.Energy;
            staging.OthInvest = dbo.OthInvest;
            staging.MaintExpTA = dbo.MaintExpTA;
            staging.MaintExpRout = dbo.MaintExpRout;
            staging.MaintExp = dbo.MaintExp;
            staging.MaintOvhdTA = dbo.MaintOvhdTA;
            staging.MaintOvhdRout = dbo.MaintOvhdRout;
            staging.MaintOvhd = dbo.MaintOvhd;
            staging.TotMaint = dbo.TotMaint;
            Sa.dbs.Stage.StagingDbAccess<stage.MExp> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.MExp>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }
        /*
        public void MiscINputToStg(ProfileFuels12.MiscInput dbo)
        {
            stage.MiscInput staging = new stage.MiscInput();
            staging.SubmissionID = _submissionId;
            staging.CDUChargeBbl = (float?)dbo.CDUChargeBbl;
            staging.CDUChargeMT = (float?)dbo.CDUChargeMT;
            Sa.dbs.Stage.StagingDbAccess<stage.MiscInput> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.MiscInput>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }
        */
        public void OpexToStg(ProfileFuels12.OpEx dbo)
        {
            stage.OpEx staging = new stage.OpEx();
            staging.SubmissionID = _submissionId;
            //throw new Exception("PofileFUels12 opex has many many columns that arent getting populated from just the attrivbute/value table in Stage");
            /* 
            staging.OpexID;
             staging.OthDescription;
             staging.Property;
             staging.RptValue;
             staging.Submission;
             
             _StDb.Absences.Add(staging);
             _StDb.SaveChanges();
            */
        }

        public void PersonnelToStg(ProfileFuels12.Per dbo)
        {
            stage.Personnel staging = new stage.Personnel();
            staging.SubmissionID = _submissionId;
            staging.PersID = dbo.PersID;
            staging.NumPers = dbo.NumPers;
            staging.STH = dbo.STH;
            staging.OVTHours = dbo.OVTHours;
            staging.OVTPcnt = dbo.OVTPcnt;
            staging.Contract = dbo.Contract;
            staging.GA = dbo.GA;
            staging.AbsHrs = dbo.AbsHrs;
            Sa.dbs.Stage.StagingDbAccess<stage.Personnel> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Personnel>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void ProcessDataToStg(ProfileFuels12.ProcessData dbo)
        {
            stage.ProcessData staging = new stage.ProcessData();
            staging.SubmissionID = _submissionId;
            staging.UnitID = (int?)dbo.UnitID;
            staging.Property = dbo.Property;
            //staging.RptDVal = dbo.RptDValue;
            staging.RptNVal = dbo.RptValue;
            //staging.RptTVal = dbo.RptTValue;
            staging.UOM = dbo.RptUOM;
            Sa.dbs.Stage.StagingDbAccess<stage.ProcessData> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.ProcessData>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void ResidToStg(ProfileFuels12.Resid dbo)
        {
            stage.Resid staging = new stage.Resid();
            staging.SubmissionID = _submissionId;
            staging.BlendID = (int?)dbo.BlendID;
            staging.Density = dbo.Density;
            staging.PourPt = dbo.PourPT;
            staging.Sulfur = dbo.Sulfur;
            staging.KMT = dbo.KMT;
            staging.ViscCSAtTemp = dbo.ViscCSAtTemp;
            staging.ViscTemp = dbo.ViscTemp;
            staging.Grade = dbo.Grade;
            Sa.dbs.Stage.StagingDbAccess<stage.Resid> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Resid>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void RPFResidToStg(ProfileFuels12.RPFResid dbo)
        {
            stage.RPFResid staging = new stage.RPFResid();
            staging.SubmissionID = _submissionId;
            staging.Density = dbo.Density;
            staging.Sulfur = dbo.Sulfur;
            staging.ViscCSAtTemp = dbo.ViscCSAtTemp;
            staging.ViscTemp = dbo.ViscTemp;
            staging.EnergyType = dbo.EnergyType;
            Sa.dbs.Stage.StagingDbAccess<stage.RPFResid> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.RPFResid>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void SteamToStg(ProfileFuels12.SteamSystem dbo)
        {
            stage.Steam staging = new stage.Steam();
            staging.SubmissionID = _submissionId;
            staging.PressureRange = dbo.PressureRange;
            staging.ActualPress = dbo.ActualPress;
            staging.H2PlantExport = dbo.H2PlantExport;
            staging.FiredProcessHeater = dbo.FiredProcessHeater;
            staging.FiredBoiler = dbo.FiredBoiler;
            staging.FCCCatCoolers = dbo.FCCCatCoolers;
            staging.FCCStackGas = dbo.FCCStackGas;
            staging.FluidCokerCOBoiler = dbo.FluidCokerCOBoiler;
            staging.Calciner = dbo.Calciner;
            staging.FTCogen = dbo.FTCogen;
            staging.WasteHeatFCC = dbo.WasteHeatFCC;
            staging.WasteHeatTCR = dbo.WasteHeatTCR;
            staging.WasteHeatCOK = dbo.WasteHeatCOK;
            staging.WasteHeatOth = dbo.WasteHeatOth;
            staging.OthSource = dbo.OthSource;
            staging.STProd = dbo.STProd;
            staging.Sold = dbo.Sold;
            staging.Pur = dbo.Pur;
            staging.NetPur = dbo.NetPur;
            staging.TotSupply = dbo.TotSupply;
            staging.ConsProcessCDU = dbo.ConsProcessCDU;
            staging.ConsProcessCOK = dbo.ConsProcessCOK;
            staging.ConsProcessFCC = dbo.ConsProcessFCC;
            staging.ConsProcessOth = dbo.ConsProcessOth;
            staging.ConsReboil = dbo.ConsReboil;
            staging.ConsOthHeaters = dbo.ConsOthHeaters;
            staging.ConsCondTurb = dbo.ConsCondTurb;
            staging.ConsTopTurbHigh = dbo.ConsTopTurbHigh;
            staging.ConsTopTurbLow = dbo.ConsTopTurbLow;
            staging.ConsCombAirPreheat = dbo.ConsCombAirPreheat;
            staging.ConsPressControlHigh = dbo.ConsPressControlHigh;
            staging.ConsPressControlLow = dbo.ConsPressControlLow;
            staging.ConsTracingHeat = dbo.ConsTracingHeat;
            staging.ConsDeaerators = dbo.ConsDeaerators;
            staging.ConsFlares = dbo.ConsFlares;
            staging.ConsOth = dbo.ConsOth;
            staging.TotCons = dbo.TotCons;
            Sa.dbs.Stage.StagingDbAccess<stage.Steam> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Steam>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void YieldToStg(ProfileFuels12.Yield dbo, int lastSortKeyUsed)
        {
            stage.Yield staging = new stage.Yield();
            staging.SubmissionID = _submissionId;
            staging.MaterialID = dbo.MaterialID;
            staging.YieldID = lastSortKeyUsed;
            staging.Category = dbo.Category;
            staging.MaterialName = dbo.MaterialName;
            //staging.Period = dbo.Period;
            staging.BBL = (float?)dbo.BBL;
            staging.Density = dbo.Density;
            staging.MT = (float?)dbo.MT;
            //staging.UOM=dbo.
            //dbo.Submission = staging.Submission;   
            Sa.dbs.Stage.StagingDbAccess<stage.Yield> stagingDb = new Sa.dbs.Stage.StagingDbAccess<stage.Yield>();
            stagingDb.Insert(staging);
            stagingDb = null;
        }

        public void GetProfileFuels12SubmissionEntityAndSubEntities(int submissionId)
        {
            ProfileFuels12.SubmissionsAll dbo = new ProfileFuels12.SubmissionsAll();
            /*
             //staging.SubmissionID =_submissionId;
             staging.PeriodBeg=(DateTime)dbo.PeriodStart ;
             staging.PeriodEnd=dbo.PeriodEnd ;
             staging.PeriodDuration_Days=(int?)dbo.NumDays;
             staging.RptCurrency=dbo.RptCurrency ;
             staging.UOM=dbo.UOM ;
             staging.RefineryID=dbo.RefineryID ;
             * */
            /*
         staging.CalcsNeeded = "Y";
         //staging.DataSet=
         staging.UseSubmission = true;
            */

            /* If I need this part, can change the public void *ToDbo to from void to the Entity and call them below
            staging.Absences=dbo.Absences ;
            staging.Configs=dbo.Configs ;
            staging.ConfigBuoys=dbo.ConfigBuoys ;
            staging.ConfigRS=dbo.ConfigRS ;
            staging.Crudes=dbo.Crudes ;
            staging.Diesels=dbo.Diesels ;
            staging.Emissions=dbo.Emissions ;
            staging.Energies=dbo.Energies ;
            staging.FiredHeaters=dbo.FiredHeaters ;
            staging.Gasolines=dbo.Gasolines ;
            staging.GeneralMiscs=dbo.GeneralMiscs ;
            staging.Inventories=dbo.Inventories ;
            staging.Kerosenes=dbo.Kerosenes ;
            staging.LPGs=dbo.LPGs ;
            staging.MaintRouts=dbo.MaintRouts ;
            staging.MaintTAs=dbo.MaintTAs ;
            staging.MarineBunkers=dbo.MarineBunkers ;
            staging.MExps=dbo.MExps ;
            staging.MiscInputs=dbo.MiscInputs ;
            staging.Opexes=dbo.Opexes ;
            staging.Personnels=dbo.Personnels ;
            staging.ProcessDatas=dbo.ProcessDatas ;
            staging.Resids=dbo.Resids ;
            staging.RPFResids=dbo.RPFResids ;
            staging.Steams=dbo.Steams ;
            staging.Yields=dbo.Yields ;
            */
            return;
        }

        public bool InsertIntoXref(string refnum, int solomonStagingSubmissionID)
        {
            try
            {
                using (var db = new Sa.Pearl.PearlEntities())
                {
                    db.Database.Connection.Open();
                    Sa.Pearl.RefnumberSubmissionMap rnsm = new Sa.Pearl.RefnumberSubmissionMap();
                    rnsm.Refnumber = refnum;
                    rnsm.StgSubmissionId= solomonStagingSubmissionID;
                    db.RefnumberSubmissionMaps.Add(rnsm);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return false;
            }
        }

    }
    public static class Extensions
    {
        public static string Leftmost(this string value, int maxLength)
        {
            string returnValue = string.Empty;
            for (int count = 1; count <= maxLength; count++)
            {
                if (count <= value.Length)
                    returnValue += value.Substring(count - 1, 1);
            }
            return returnValue;
        } 
    }
}
