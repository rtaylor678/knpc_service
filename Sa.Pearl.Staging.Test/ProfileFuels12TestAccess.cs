﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;


namespace Sa.Pearl.Staging.Test
{
    public class ProfileFuels12TestAccess<TEntity> where TEntity : class
    {
        #region Private members

        private DbContext context;
        private DbSet<TEntity> entityInstance;
        private DbContextTransaction _transaction;

        #endregion

        #region Constructors

        public ProfileFuels12TestAccess(DbContext context)
        {
            this.context = context;
        }

        public ProfileFuels12TestAccess()
        {
            this.context = new ProfileFuels12.PFProdEntities();
        }

        #endregion

        #region Properties

        public DbContext Context
        {
            get
            {
                return context;
            }
        }

        public DbSet<TEntity> EntityInstance
        {
            get
            {
                if (this.entityInstance == null)
                    this.entityInstance = context.Set<TEntity>();

                return this.entityInstance;
            }
        }

        #endregion

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> where)
        {
            //return Repository.GetAll(x => x.CustomerID == customerId).ToList();
            return EntityInstance.Where(where);
        }

        public IEnumerable<TEntity> Select(TEntity entity, int Id)
        {
            var submissioQuery = from subm in EntityInstance
                                 select subm;
            List<TEntity> submList = submissioQuery.ToList();
            return submList;
        }

        public void Insert(TEntity entity)
        {
            try
            {
                EntityInstance.Add(entity);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                if (ex.InnerException != null)
                    err += " | " + ex.InnerException.ToString();
                //Logger.LogInfo(err); //so can see where the error happened; LogException won't do that right now.
                //Logger.LogException(ex);
            }
        }

        public void Update(TEntity entity)
        {
            EntityInstance.Attach(entity);
            context.Entry<TEntity>(entity).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            EntityInstance.Remove(entity);
            Context.SaveChanges();
        }

        public bool BeginTransaction()
        {      //if this is called, and Commit() is never called, no changes to db are made      
            try
            {
                _transaction = Context.Database.BeginTransaction();
                return true;
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }
        public void Rollback()
        {
            _transaction.Rollback();
        }

        public bool Commit()
        {
            try
            {
                _transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                Sa.Logger.LogException(ex);
                return false;
            }
        }

    }
}
