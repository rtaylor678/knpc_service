//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.Pearl.Staging.Test.ProfileFuels12
{
    using System;
    using System.Collections.Generic;
    
    public partial class APIScale
    {
        public short StudyYear { get; set; }
        public byte APIScale1 { get; set; }
        public byte APILevel { get; set; }
        public float MinAPI { get; set; }
        public float MaxAPI { get; set; }
        public decimal Adj { get; set; }
        public System.DateTime SaveDate { get; set; }
    }
}
