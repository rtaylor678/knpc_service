//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sa.Pearl.Staging.Test.ProfileFuels12
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReadyForCalc
    {
        public string RefineryID { get; set; }
        public string DataSet { get; set; }
        public bool CalcsStarted { get; set; }
        public bool Uploading { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
    }
}
