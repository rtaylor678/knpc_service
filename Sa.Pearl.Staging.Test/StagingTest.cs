﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sa.Pearl.Staging;
using System.Collections.Generic;

using System.Data.Entity;
using System.Data;
//using System.Data.Entity.Validation;
//using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.Collections;

using System.Linq;

using Sa.dbs.Dbo;
using DbsStage = Sa.dbs.Stage;
using staging = Sa.Pearl.Staging;


namespace Sa.Pearl.Staging.Test
{
    [TestClass]
    public class StagingTest
    {
        [TestMethod]
        public void LogTHIS()
        {
            Sa.Logger.LogInfo("TEST " + DateTime.Now.ToString("YYYY-DD-MM HH:MM:SS"));
        }

        //doing this with 1data upload now, not this test.
        /*
        [TestMethod]
        public void DataFromStagingToDbo()
        {
            //need to have the conx string 'DboEntities' checked in both this project's app.config and app.config for Sa.dbs.Dbo
            //need to have the conx string 'StageEntities' checked in both this project's app.config and app.config for Sa.dbs.Stage

            //you will want to point the dboentities conx string to Dev for this.
            int stagingSubmissionIdToUse = 12;
            int dboSubmissionId=-1;
            string refineryIdToUse = "162EUR";

            Sa.Pearl.StagingToDbo stagingToDbo = new Sa.Pearl.StagingToDbo();
            //couldn't reach when was internal. 
            //PrivateObject privateHelperObject = new PrivateObject(typeof(Sa.Pearl.StagingToDbo));
            //PrivateObject stagingToDbo = new PrivateObject(typeof(Sa.Pearl.StagingToDbo));
            //bool testResult = (bool)stagingToDbo.Invoke("MoveStagingToDbo", stagingSubmissionIdToUse);
            bool testResult = stagingToDbo.MoveStagingToDbo(stagingSubmissionIdToUse, refineryIdToUse, ref dboSubmissionId);
            Assert.IsTrue(testResult); //stagingToDbo.MoveStagingToDbo(stagingSubmissionIdToUse));
        }
    */

        [TestMethod]
        public void PopulateStagingFromDbo()
        { //you will want to point the dboentities conx string to Prod for this.

            //not needing this anymore; was just to populate the data for development.
            Assert.IsTrue(1 == 1);
            return;

            #region "reset"
            /*
            use [dbs.stage]

            DELETE FROM stage.Absence;
            DELETE FROM stage.Comment;
            DELETE FROM stage.config;
            DELETE FROM stage.configbuoy;
            DELETE FROM stage.configrs;
            DELETE FROM stage.crude;
            DELETE FROM stage.diesel;
            DELETE FROM stage.emission;
            DELETE FROM stage.energy;
            DELETE FROM stage.finprodprops;
            DELETE FROM stage.firedheaters;
            DELETE FROM stage.gasoline;
            DELETE FROM stage.generalmisc;
            DELETE FROM stage.inventory;
            DELETE FROM stage.kerosene;
            DELETE FROM stage.LPG;
            DELETE FROM stage.maintrout;
            DELETE FROM stage.MaintTA;
            DELETE FROM stage.marinebunker;
            DELETE FROM stage.mexp;
            DELETE FROM stage.miscinput;
            DELETE FROM stage.opex;
            DELETE FROM stage.personnel;
            DELETE FROM stage.processdata;
            DELETE FROM stage.resid;
            DELETE FROM stage.rpfresid;
            DELETE FROM stage.steam;
            DELETE FROM stage.validationresult;
            DELETE FROM stage.yield;
            DELETE FROM pearl.RefnumberSubmissionMap;
            DELETE FROM stage.Submissions;
            */
            #endregion

            //you will want to point the dboentities conx string to Prod for this.
            int submissionID = 4886; //prod submission ID
            string refineryId = "162EUR";
            int clientSubmissionID = 5; //6; //can't reuse this value
            DataFromDboToStaging(submissionID, refineryId, clientSubmissionID);
            // 4886,
            int[] submissionsList = { 4887, 4888, 4802, 4805, 4884, 4803, 4806, 4885 };
            //"162EUR",
            string[] refineriesList = {"162EUR","162EUR","163EUR","163EUR","163EUR","166EUR","166EUR","166EUR"};
            for (int submissionCount = 0; submissionCount < submissionsList.Length; submissionCount++)
            {
                clientSubmissionID++;
                DataFromDboToStaging(submissionsList[submissionCount], refineriesList[submissionCount], clientSubmissionID);
            }            
        }

        public void DataFromDboToStaging(int dboSubmissionID, string refineryId, int clientSubmissionID)
        {
	//need to have the conx string 'DboEntities' checked in both this project's app.config and app.config for Sa.dbs.Dbo
	//need to have the conx string 'StageEntities' checked in both this project's app.config and app.config for Sa.dbs.Stage
            //you will want to point the dboentities conx string to Prod for this.
            //int submissionID = 4886; //prod submission ID
            //string refineryId = "162EUR";
            //int clientSubmissionID = 5; //can't reuse this value

            DboToStaging dboToStaging = new DboToStaging(dboSubmissionID);
            ProfileFuels12.SubmissionsAll pfSubmission = dboToStaging.GetProfileFuels12SubmissionEntity(dboSubmissionID);
            DbsStage.Submission stagingSubmission = dboToStaging.SubmissionToStg(pfSubmission.SubmissionID, clientSubmissionID);

            ProfileFuels12TestAccess<ProfileFuels12.Absence> absenceAccess = new ProfileFuels12TestAccess<ProfileFuels12.Absence>();
            var absences = absenceAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Absence absenceItem in absences)
            {
                dboToStaging.AbsenceToStg(absenceItem);
            }
            /*
            var absenceList = GetEntitiesList(typeof(Absence));
            foreach (ProfileFuels12.Absence absenceItem in absenceList)
            {
                dboToStaging.AbsenceToStg(absenceItem);
            }
            */


            ProfileFuels12TestAccess<ProfileFuels12.Config> configAccess = new ProfileFuels12TestAccess<ProfileFuels12.Config>();
            var configs = configAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Config configItem in configs)
            {
                dboToStaging.ConfigToStg(configItem);
            }
            /*ConfigBuoy Table doesn't exist in ProileFuels12 Prod           
            ProfileFuels12TestAccess<ProfileFuels12.ConfigBuoy> configBuoyAccess = new ProfileFuels12TestAccess<ProfileFuels12.ConfigBuoy>();
            var configBuoys = configBuoyAccess.GetAll(x => x.SubmissionID == submissionID).ToList();
            foreach (ProfileFuels12.ConfigBuoy configBuoyItem in configBuoys)
            {
            dboToStaging.ConfigBuoyToStg(configBuoyItem);
            }
            */
            ProfileFuels12TestAccess<ProfileFuels12.ConfigR> configRAccess = new ProfileFuels12TestAccess<ProfileFuels12.ConfigR>();
            var configRs = configRAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.ConfigR configRItem in configRs)
            {
                dboToStaging.ConfigRS1ToStg(configRItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.Crude> crudeAccess = new ProfileFuels12TestAccess<ProfileFuels12.Crude>();
            var crudes = crudeAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Crude crudeItem in crudes)
            {
                dboToStaging.CrudeToStg(crudeItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.Diesel> dieselAccess = new ProfileFuels12TestAccess<ProfileFuels12.Diesel>();
            //If  SELECT permission is denied then must use different ID than ProfileFuels
            var diesels = dieselAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Diesel dieselItem in diesels)
            {
                dboToStaging.DieselToStg(dieselItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.Emission> emissionAccess = new ProfileFuels12TestAccess<ProfileFuels12.Emission>();
            var emissions = emissionAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Emission emissionItem in emissions)
            {
                dboToStaging.EmissionsToStg(emissionItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.Energy> energyAccess = new ProfileFuels12TestAccess<ProfileFuels12.Energy>();
            var energys = energyAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Energy energyItem in energys)
            {
                dboToStaging.EnergyToStg(energyItem);
            }

            
            ProfileFuels12TestAccess<ProfileFuels12.FiredHeater> firedHeaterAccess = new ProfileFuels12TestAccess<ProfileFuels12.FiredHeater>();
            var firedHeaters = firedHeaterAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.FiredHeater firedHeaterItem in firedHeaters)
            {
                dboToStaging.FiredHeaterToStg(firedHeaterItem);
            }           
            
            ProfileFuels12TestAccess<ProfileFuels12.Gasoline> gasolineAccess = new ProfileFuels12TestAccess<ProfileFuels12.Gasoline>();
            var gasolines = gasolineAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Gasoline gasolineItem in gasolines)
            {
                dboToStaging.GasolineToStg(gasolineItem);
            }
            
            ProfileFuels12TestAccess<ProfileFuels12.GeneralMisc> generalMiscAccess = new ProfileFuels12TestAccess<ProfileFuels12.GeneralMisc>();
            var generalMiscs = generalMiscAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.GeneralMisc generalMiscItem in generalMiscs)
            {
                dboToStaging.GeneralMiscToStg(generalMiscItem);
            }
            

            ProfileFuels12TestAccess<ProfileFuels12.Inventory> inventoryAccess = new ProfileFuels12TestAccess<ProfileFuels12.Inventory>();
            var inventorys = inventoryAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Inventory inventoryItem in inventorys)
            {
                dboToStaging.InventoryToStg(inventoryItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.Kerosene> keroseneAccess = new ProfileFuels12TestAccess<ProfileFuels12.Kerosene>();
            var kerosenes = keroseneAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Kerosene keroseneItem in kerosenes)
            {
                dboToStaging.KeroseneToStg(keroseneItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.LPG> lPGAccess = new ProfileFuels12TestAccess<ProfileFuels12.LPG>();
            var lPGs = lPGAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.LPG lPGItem in lPGs)
            {
                dboToStaging.LPGToStg(lPGItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.MaintRout> maintRoutAccess = new ProfileFuels12TestAccess<ProfileFuels12.MaintRout>();
            var maintRouts = maintRoutAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.MaintRout maintRoutItem in maintRouts)
            {
                dboToStaging.MaintRoutToStg(maintRoutItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.MaintTA> maintTAAccess = new ProfileFuels12TestAccess<ProfileFuels12.MaintTA>();
            var maintTAs = maintTAAccess.GetAll(x => x.RefineryID == refineryId).ToList();
            foreach (ProfileFuels12.MaintTA maintTAItem in maintTAs)
            {
                dboToStaging.MaintTAToStg(maintTAItem);
            }
            

            ProfileFuels12TestAccess<ProfileFuels12.MarineBunker> marineBunkerAccess = new ProfileFuels12TestAccess<ProfileFuels12.MarineBunker>();
            var marineBunkers = marineBunkerAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.MarineBunker marineBunkerItem in marineBunkers)
            {
                dboToStaging.MarineBunkerToStg(marineBunkerItem);
            }
            
            ProfileFuels12TestAccess<ProfileFuels12.MExp> mExpAccess = new ProfileFuels12TestAccess<ProfileFuels12.MExp>();
            var mExps = mExpAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.MExp mExpItem in mExps)
            {
                dboToStaging.MexpToStg(mExpItem);
            }


            ProfileFuels12TestAccess<ProfileFuels12.GeneralMisc> miscInputAccess = new ProfileFuels12TestAccess<ProfileFuels12.GeneralMisc>();
            var miscInputs = miscInputAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.GeneralMisc miscInputItem in miscInputs)
            {
            dboToStaging.GeneralMiscToStg(miscInputItem);
            }
            
            ProfileFuels12TestAccess<ProfileFuels12.OpEx> opexAccess = new ProfileFuels12TestAccess<ProfileFuels12.OpEx>();
            var opexs = opexAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.OpEx opexItem in opexs)
            {
                dboToStaging.OpexToStg(opexItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.Per> personnelAccess = new ProfileFuels12TestAccess<ProfileFuels12.Per>();
            var personnels = personnelAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Per personnelItem in personnels)
            {
                dboToStaging.PersonnelToStg(personnelItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.ProcessData> processDataAccess = new ProfileFuels12TestAccess<ProfileFuels12.ProcessData>();
            var processDatas = processDataAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.ProcessData processDataItem in processDatas)
            {
                dboToStaging.ProcessDataToStg(processDataItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.Resid> residAccess = new ProfileFuels12TestAccess<ProfileFuels12.Resid>();
            var resids = residAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.Resid residItem in resids)
            {
                dboToStaging.ResidToStg(residItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.RPFResid> rPFResidAccess = new ProfileFuels12TestAccess<ProfileFuels12.RPFResid>();
            var rPFResids = rPFResidAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.RPFResid rPFResidItem in rPFResids)
            {
                dboToStaging.RPFResidToStg(rPFResidItem);
            }
            /* StagingEntities table doesn't exist in ProfileFuels12 Prod
            ProfileFuels12TestAccess<ProfileFuels12.StagingEntities> stagingEntitiesAccess = new ProfileFuels12TestAccess<ProfileFuels12.StagingEntities>();
            var stagingEntitiess = stagingEntitiesAccess.GetAll(x => x.SubmissionID == submissionID).ToList();
            foreach (ProfileFuels12.StagingEntities stagingEntitiesItem in stagingEntitiess)
            {
            dboToStaging.StagingEntitiesToStg(stagingEntitiesItem);
            }
            */
            ProfileFuels12TestAccess<ProfileFuels12.SteamSystem> steamAccess = new ProfileFuels12TestAccess<ProfileFuels12.SteamSystem>();
            var steams = steamAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            foreach (ProfileFuels12.SteamSystem steamItem in steams)
            {
                dboToStaging.SteamToStg(steamItem);
            }

            ProfileFuels12TestAccess<ProfileFuels12.Yield> yieldAccess = new ProfileFuels12TestAccess<ProfileFuels12.Yield>();
            var yields = yieldAccess.GetAll(x => x.SubmissionID == dboSubmissionID).ToList();
            int lastSortKeyInUse = 100;
            foreach (ProfileFuels12.Yield yieldItem in yields)
            {
                //this might error due to dupes in profilefuels12 Yield records in Prod
                dboToStaging.YieldToStg(yieldItem, lastSortKeyInUse);
                lastSortKeyInUse++;
            }

            //Doesn't happen in dbo to stage, hasppens in stage to dbo:  if (!dboToStaging.InsertIntoXref(refineryId, stagingSubmission.SubmissionID))
            //{
            //    throw new Exception("Failed on xref");
            //}

        }

        [TestMethod]
        public void InventoryToDboTest()
        {
            //FuelsStorage change per Ralph 2018-02-21
            Assert.Inconclusive("Must prevent db update in PROD");
            DbsStage.Inventory staging = new DbsStage.Inventory();
            bool result = false;
            staging.SubmissionID = -99;
            staging.TotStorage = null;
            staging.MandStorage = null;
            staging.FuelStorage = null;
            PrivateObject po = new PrivateObject(typeof(Sa.Pearl.StagingToDbo));
            object[] param = new object[] { staging };

            result = (bool)po.Invoke("InventoryToDbo", param);
            Assert.IsTrue(result);

            staging.TotStorage = 10;
            result = (bool)po.Invoke("InventoryToDbo", param);
            Assert.IsTrue(result);

            staging.MandStorage = 20;
            result = (bool)po.Invoke("InventoryToDbo", param);
            Assert.IsTrue(result);

            staging.FuelStorage = 5;
            result = (bool)po.Invoke("InventoryToDbo", param);
            Assert.IsTrue(result);

            /*
                        
            float totStorage = staging.TotStorage ?? 0;
            float mandStorage = staging.MandStorage ?? 0;
            //dbo.FuelsStorage = staging.FuelStorage;
            dbo.FuelsStorage = totStorage - mandStorage;
            */
        }

        [TestMethod]
        public void ConfigToDboTest()
        {
            //Changes per Richard Thut 2018-02-22
            //StagingToDbo stagingToDbo = new StagingToDbo();
            Assert.Inconclusive("Must prevent db update in PROD");
            DbsStage.Config staging = new DbsStage.Config();
            bool result = false;
            staging.SubmissionID = -99;
            staging.InServicePcnt = null;
            PrivateObject po = new PrivateObject(typeof(Sa.Pearl.StagingToDbo));
            object[] param = new object[]{staging};
            result=(bool)po.Invoke("ConfigToDbo",param);
            Assert.IsTrue(result);
            staging.InServicePcnt = 10;
            result = (bool)po.Invoke("ConfigToDbo", param);
            Assert.IsTrue(result);
        }



        private List<T> GetEntitiesList<T>(T entity)
        {
	        //DbSet<TEntity> entityInstance;
            //return entityInstance; //this is the Select from the dB context
            List<T> output= new List<T>();
            output.Add(entity);
            return output;
        }

        [TestMethod]
        public void TestXmlWrite()
        {
            Sa.dbs.Stage.Submission stageSubmission = new dbs.Stage.Submission();
            stageSubmission.ClientSubmissionID = 32;
            stageSubmission.PeriodBeg = new DateTime(2016, 05, 01);
            stageSubmission.PeriodEnd = new DateTime(2016, 06, 01);
            stageSubmission.RefineryID = "XXPAC";
            stageSubmission.RptCurrency = "USD";
            stageSubmission.SubmissionID = 33;
            stageSubmission.UOM = "US";
            stageSubmission.PeriodDuration_Days = 30;
            stageSubmission.Notes = "Notes";


            stageSubmission.Absences = new List<Sa.dbs.Stage.Absence>();
            Sa.dbs.Stage.Absence absence = new Sa.dbs.Stage.Absence();
            absence.SubmissionID = 33;
            absence.AbsenceID = 99;
            absence.Category = "Category";
            absence.OCCAbs = 1;
            stageSubmission.Absences.Add(absence);
            SerializeSubmission serializer = new SerializeSubmission();
            //List<String> errors = MakeXMLFile("C:\\test.xml", stageSubmission, true);
            List<String> errors = serializer.WriteToXml("C:\\test1.xml", stageSubmission, true);
            Assert.IsTrue(errors.Count == 0);
            errors = serializer.WriteToXml("C:\\test2.xml", stageSubmission, false);
            Assert.IsTrue(errors.Count == 0);
        }

        [TestMethod]
        public void AddSerializableTagToStaging()
        {
            string folder = @"C:\tfs\CPA\KNPC_Service\Sa.dbs.Stage";
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(folder);
            System.IO.FileInfo[] files =  dir.GetFiles("*.cs");
            foreach (System.IO.FileInfo file in files)
            {
                try
                {
                    if (file.Name != "ShortStage.cs" &&
                       file.Name != "stage.Context.cs" &&
                        file.Name != "stage.cs" &&
                        file.Name != "stage.Designer.cs" &&
                        file.Name != "StagingDbAccess.cs"  )
                    {
                        string contents = string.Empty;
                        bool generated = false;
                        using (System.IO.StreamReader reader = new System.IO.StreamReader(file.FullName))
                        {
                            contents = reader.ReadToEnd();
                            if (contents.Contains("//    This code was generated from a template.") && !contents.Contains("[Serializable]") )
                            {
                                //int startPos = contents.IndexOf("public partial class");
                                contents = contents.Replace("public partial class", "[Serializable]" + Environment.NewLine + "    public partial class");
                                generated = true;
                            }
                        }
                        if (generated)
                        {
                            using (System.IO.StreamWriter writer= new System.IO.StreamWriter(file.FullName))
                            {
                                writer.Write(contents);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Assert.Fail(file.Name + " failed");
                }               
            }
            Assert.AreEqual(1, 1);
        }
    }
}
