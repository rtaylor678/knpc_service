﻿
CREATE FUNCTION [dbo].[SLAverageGrossMargin](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT m.Currency, m.Scenario, GPV = [$(dbsGlobal)].[dbo].WtAvg(m.GPV,m.Divisor)
		, RMC = [$(dbsGlobal)].[dbo].WtAvg(m.RMC, m.Divisor)
		, GrossMargin = [$(dbsGlobal)].[dbo].WtAvg(m.GrossMargin, m.Divisor)
		, OthRev = [$(dbsGlobal)].[dbo].WtAvg(m.OthRev, m.Divisor)
--		, CashOpex = [$(dbsGlobal)].[dbo].WtAvg(m.CashOpex, m.Divisor)
--		, CashMargin = [$(dbsGlobal)].[dbo].WtAvg(m.CashMargin, m.Divisor)
	FROM MarginCalc m INNER JOIN @SubmissionList s ON s.SubmissionID = m.SubmissionID
	WHERE m.DataType = 'BBL'
	GROUP BY m.Currency, m.Scenario
	)


