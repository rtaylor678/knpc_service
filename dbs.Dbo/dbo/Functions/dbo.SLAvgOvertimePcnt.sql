﻿
CREATE FUNCTION [dbo].[SLAvgOvertimePcnt](@SubmissionList dbo.SubmissionIDList READONLY, @SectionID char(2))
RETURNS real
AS
BEGIN
	DECLARE @OVTPcnt real
	SELECT @OVTPcnt = SUM(p.OVTHours)/SUM(p.STH)*100
	FROM @SubmissionList s 
	INNER JOIN PersST p ON p.SubmissionID = s.SubmissionID AND p.SectionID = @SectionID AND p.STH > 0
	
	RETURN @OVTPcnt
END


