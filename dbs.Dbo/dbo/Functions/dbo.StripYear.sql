﻿CREATE FUNCTION [dbo].[StripYear]
(
	@GroupKey	VARCHAR(20),
	@StudyYear	VARCHAR(2)
)
RETURNS VARCHAR(20)

	AS

	BEGIN
		
		DECLARE @IsSinglePlant BIT;
		SET @IsSinglePlant =  [dbo].[IsSinglePlantGroup](@GroupKey);
		DECLARE @Output  VARCHAR(20)

		IF @IsSinglePlant = 0
				
				SET @Output = stuff(@GroupKey, charindex(@StudyYear, @GroupKey), len(@StudyYear), @Output)	
				
			ELSE
				 
				SET @Output = @GroupKey

		RETURN @Output

	END