﻿CREATE FUNCTION [dbo].[CalcAverageMargins](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT m.Currency, m.Scenario, GPV = [$(dbsGlobal)].[dbo].WtAvg(m.GPV,m.Divisor)
		, RMC = [$(dbsGlobal)].[dbo].WtAvg(m.RMC, m.Divisor)
		, GrossMargin = [$(dbsGlobal)].[dbo].WtAvg(m.GrossMargin, m.Divisor)
		, CashOpex = [$(dbsGlobal)].[dbo].WtAvg(m.CashOpex, m.Divisor)
		, CashMargin = [$(dbsGlobal)].[dbo].WtAvg(m.CashMargin, m.Divisor)
	FROM MarginCalc m INNER JOIN dbo.Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND m.DataType = 'BBL'
	AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
	GROUP BY m.Currency, m.Scenario
	)



