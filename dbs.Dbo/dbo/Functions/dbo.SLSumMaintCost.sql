﻿

CREATE FUNCTION [dbo].[SLSumMaintCost](@SubmissionList dbo.SubmissionIDList READONLY, @Currency varchar(4))
RETURNS TABLE
AS
RETURN (
	SELECT a.Currency
		, CurrTACost = SUM(CurrTACost), CurrRoutCost = SUM(CurrRoutCost), CurrMaintCost = SUM(CurrMaintCost)
		, CurrTAMatl = SUM(CurrTAMatl), CurrRoutMatl = SUM(CurrRoutMatl), CurrMaintMatl = SUM(CurrMaintMatl)
		, AllocAnnTACost = SUM(AllocAnnTACost), AllocAnnTAMatl = SUM(AllocAnnTAMatl), AllocCurrRoutCost = SUM(AllocCurrRoutCost), AllocCurrRoutMatl = SUM(AllocCurrRoutMatl)
	FROM MaintTotCost a INNER JOIN @SubmissionList s ON s.SubmissionID = a.SubmissionID
	WHERE a.Currency = ISNULL(@Currency, a.Currency)
	GROUP BY a.Currency
)



