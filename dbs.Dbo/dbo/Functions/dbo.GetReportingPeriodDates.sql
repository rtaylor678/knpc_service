﻿CREATE FUNCTION [dbo].[GetReportingPeriodDates](@RefineryID char(6), @DataSet varchar(15), @PeriodYear smallint, @PeriodMonth smallint)
RETURNS @dates TABLE (SubmissionID int, StartDateSubmission smalldatetime, EndDateSubmission smalldatetime,
	StartDate3Mo smalldatetime NULL, StartDate12Mo smalldatetime NULL, StartDate24Mo smalldatetime NULL, StartDateYTD smalldatetime NULL, StartDateQTR smalldatetime NULL, EndDateQTR smalldatetime NULL)
AS
BEGIN

INSERT @dates(SubmissionID, StartDateSubmission, EndDateSubmission, StartDate3Mo, StartDate12Mo, StartDate24Mo, StartDateYTD)
SELECT SubmissionID, PeriodStart, PeriodEnd, DATEADD(mm, -3, PeriodEnd), Start12Mo = DATEADD(mm, -12, PeriodEnd), 
		Start24Mo = DATEADD(mm, -24, PeriodEnd), StartYTD = dbo.BuildDate(DATEPART(yy, PeriodStart), 1, 1)
FROM dbo.Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet AND UseSubmission = 1

UPDATE @dates SET StartDateQTR = dbo.BuildDate(DATEPART(yy, DATEADD(dd, -1, EndDateSubmission)), DATEPART(QUARTER, DATEADD(dd, -1, EndDateSubmission))*3-2, 1)
UPDATE @dates SET EndDateQTR = DATEADD(mm, 3, StartDateQTR)

RETURN

END


