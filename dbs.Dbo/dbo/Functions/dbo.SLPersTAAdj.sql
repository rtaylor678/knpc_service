﻿CREATE FUNCTION [dbo].[SLPersTAAdj](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet)
RETURNS @TAAdj TABLE (FactorSet varchar(8) NOT NULL, OCCTAWHrEffIndex real NULL, OCCTAmPEI real NULL, OCCTAWHrEDC real NULL, MPSTAWHrEffIndex real NULL, MPSTAmPEI real NULL, MPSTAWHrEDC real NULL)
AS
BEGIN
	INSERT @TAAdj(FactorSet, OCCTAWHrEffIndex, OCCTAmPEI, OCCTAWHrEDC, MPSTAWHrEffIndex, MPSTAmPEI, MPSTAWHrEDC)
	SELECT ftc.FactorSet, OCCTAWHrEffIndex = [$(dbsGlobal)].[dbo].WtAvg(o.TotWHrEffIndex, o.EffDivisor), OCCTAmPEI = [$(dbsGlobal)].[dbo].WtAvg(o.MaintPersEffIndex, o.MaintPersEffDivisor), OCCTAWHrEDC = [$(dbsGlobal)].[dbo].WtAvg(o.TotWHrEDC, o.WHrEDCDivisor)
						, MPSTAWHrEffIndex = [$(dbsGlobal)].[dbo].WtAvg(m.TotWHrEffIndex, m.EffDivisor), MPSTAmPEI = [$(dbsGlobal)].[dbo].WtAvg(m.MaintPersEffIndex, m.MaintPersEffDivisor), MPSTAWHrEDC = [$(dbsGlobal)].[dbo].WtAvg(m.TotWHrEDC, m.WHrEDCDivisor)
	FROM dbo.SLLastPeriods(@SubmissionList) s INNER JOIN FactorTotCalc ftc ON ftc.SubmissionID = s.SubmissionID
	LEFT JOIN PersCalc o ON o.SubmissionID = s.SubmissionID AND o.FactorSet = ftc.FactorSet AND o.PersID = 'OCCTAADJ'
	LEFT JOIN PersCalc m ON m.SubmissionID = s.SubmissionID AND m.FactorSet = ftc.FactorSet AND m.PersID = 'MPSTAADJ'
	WHERE ftc.FactorSet = ISNULL(@FactorSet, ftc.FactorSet)
	GROUP BY ftc.FactorSet
	
	RETURN
END


