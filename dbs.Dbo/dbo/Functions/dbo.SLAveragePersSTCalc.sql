﻿CREATE FUNCTION [dbo].[SLAveragePersSTCalc](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet)
RETURNS @sections TABLE (
	FactorSet varchar(8) NOT NULL,
	SectionID char(2) NOT NULL,
	EffIndex real NULL,
	EffDivisor real NULL,
	WHrEDC real NULL,
	EDCDivisor real NULL)
AS
BEGIN

/* Get Non-TA average KPIs */
INSERT @sections (FactorSet, SectionID, EffIndex, EffDivisor, WHrEDC, EDCDivisor)
SELECT FactorSet, SectionID, TotWHrEffIndex = [$(dbsGlobal)].[dbo].WtAvg(TotWHrEffIndex, EffDivisor), EffDivisor = SUM(EffDivisor)
	, TotWHrEDC = [$(dbsGlobal)].[dbo].WtAvg(TotWHrEDC, WHrEDCDivisor), WHrEDCDivisor = SUM(WHrEDCDivisor)
FROM PersSTCalcNonTA
WHERE FactorSet = ISNULL(@FactorSet, FactorSet) AND SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)
GROUP BY FactorSet, SectionID

/* Get Current T/A personnel adjustments */
DECLARE @TAAdj TABLE (FactorSet varchar(8) NOT NULL, OCCTAWHrEffIndex real NULL, OCCTAWHrEDC real NULL, MPSTAWHrEffIndex real NULL, MPSTAWHrEDC real NULL)
INSERT @TAAdj (FactorSet, OCCTAWHrEffIndex, OCCTAWHrEDC, MPSTAWHrEffIndex, MPSTAWHrEDC)
SELECT FactorSet, OCCTAWHrEffIndex, OCCTAWHrEDC, MPSTAWHrEffIndex, MPSTAWHrEDC
FROM dbo.SLPersTAAdj(@SubmissionList, @FactorSet)

/* Multiply current T/A indicators by average divisor and add to non-T/A */
UPDATE s SET EffIndex = ISNULL(s.EffIndex,0) + ISNULL(ta.OCCTAWHrEffIndex,0)
FROM @sections s INNER JOIN @TAAdj ta ON ta.FactorSet = s.FactorSet
WHERE s.SectionID IN ('OM','TO','TP')
UPDATE s SET EffIndex = ISNULL(s.EffIndex,0) + ISNULL(ta.MPSTAWHrEffIndex,0)
FROM @sections s INNER JOIN @TAAdj ta ON ta.FactorSet = s.FactorSet
WHERE s.SectionID IN ('MM','TM','TP')
UPDATE @sections SET WHrEDC = EffIndex*EffDivisor/EDCDivisor WHERE EDCDivisor > 0 AND SectionID IN ('OM','MM','TO','TM','TP')

RETURN

END


