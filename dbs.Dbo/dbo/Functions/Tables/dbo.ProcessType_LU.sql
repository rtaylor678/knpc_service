﻿CREATE TABLE [dbo].[ProcessType_LU] (
    [ProcessID]   [dbo].[ProcessID]   NOT NULL,
    [ProcessType] [dbo].[ProcessType] NOT NULL,
    [Description] VARCHAR (255)       NULL,
    CONSTRAINT [ProcType_pKey] PRIMARY KEY CLUSTERED ([ProcessID] ASC, [ProcessType] ASC) WITH (FILLFACTOR = 90)
);

