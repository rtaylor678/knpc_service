﻿CREATE TABLE [dbo].[ROICalc] (
    [SubmissionID] INT                  NOT NULL,
    [FactorSet]    [dbo].[FactorSet]    NOT NULL,
    [Scenario]     [dbo].[Scenario]     NOT NULL,
    [Currency]     [dbo].[CurrencyCode] NOT NULL,
    [ROI]          REAL                 NULL,
    [RORV]         REAL                 NULL,
    [CMI]          REAL                 NULL,
    [VAI]          REAL                 NULL,
    [RV]           REAL                 NULL,
    [WorkingCptl]  REAL                 NULL,
    [TotCptl]      REAL                 NULL,
    [RVCptlRatio]  REAL                 NULL,
    [NetOperVAI]   REAL                 NULL,
    [GrossOperVAI] REAL                 NULL,
    CONSTRAINT [PK_ROICalc] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [FactorSet] ASC, [Scenario] ASC, [Currency] ASC) WITH (FILLFACTOR = 90)
);

