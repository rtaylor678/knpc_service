﻿CREATE TABLE [dbo].[RefProcessGroupings] (
    [SubmissionID]    INT               NOT NULL,
    [FactorSet]       [dbo].[FactorSet] NOT NULL,
    [ProcessGrouping] [dbo].[ProcessID] NOT NULL,
    [UnitID]          [dbo].[UnitID]    NOT NULL,
    CONSTRAINT [PK_RefProcessGroupings] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [FactorSet] ASC, [ProcessGrouping] ASC, [UnitID] ASC)
);

