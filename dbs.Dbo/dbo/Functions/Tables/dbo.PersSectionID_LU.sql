﻿CREATE TABLE [dbo].[PersSectionID_LU] (
    [SectionID]   [dbo].[PersSectionID] NOT NULL,
    [SectionDesc] VARCHAR (50)          NOT NULL,
    CONSTRAINT [PK_PersSectionID_LU] PRIMARY KEY CLUSTERED ([SectionID] ASC) WITH (FILLFACTOR = 90)
);

