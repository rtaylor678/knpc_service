﻿CREATE TABLE [dbo].[CrudeCalc] (
    [SubmissionID] INT              NOT NULL,
    [Scenario]     [dbo].[Scenario] NOT NULL,
    [CrudeID]      [dbo].[CrudeID]  NOT NULL,
    [CNum]         [dbo].[CrudeNum] NULL,
    [BasePrice]    REAL             CONSTRAINT [DF_CrudeCalc_BasePrice] DEFAULT ((0)) NULL,
    [TransCost]    REAL             CONSTRAINT [DF_CrudeCalc_TransCost] DEFAULT ((0)) NULL,
    [GravityAdj]   REAL             CONSTRAINT [DF_CrudeCalc_GravityAdj] DEFAULT ((0)) NULL,
    [SulfurAdj]    REAL             NULL,
    [PricePerBbl]  REAL             NULL,
    CONSTRAINT [PK_CrudeCalc] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [CrudeID] ASC) WITH (FILLFACTOR = 90)
);

