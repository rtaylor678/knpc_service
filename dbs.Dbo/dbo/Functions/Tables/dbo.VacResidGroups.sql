﻿CREATE TABLE [dbo].[VacResidGroups] (
    [ResidGroup] CHAR (2) NOT NULL,
    [Category]   CHAR (5) NOT NULL,
    [MaterialID] CHAR (5) NOT NULL,
    [SortKey]    INT      NULL
);

