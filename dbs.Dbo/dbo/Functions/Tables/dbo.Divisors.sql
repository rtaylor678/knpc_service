﻿CREATE TABLE [dbo].[Divisors] (
    [SubmissionID] INT               NOT NULL,
    [FactorSet]    [dbo].[FactorSet] NOT NULL,
    [Divisor]      VARCHAR (8)       NOT NULL,
    [DivValue]     REAL              NOT NULL,
    CONSTRAINT [PK_Divisors] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [FactorSet] ASC, [Divisor] ASC) WITH (FILLFACTOR = 90)
);

