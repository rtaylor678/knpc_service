﻿CREATE TABLE [dbo].[Etl]
(
	[EtlId] INT NOT NULL PRIMARY KEY IDENTITY,
	[DataKey]			VARCHAR(60)		NOT	NULL	CHECK([DataKey] <> ''),
	[RptDatabase]		VARCHAR(20)		NOT	NULL	CHECK([RptDatabase] <> '')		DEFAULT('Refining14'),
	[RptSchema]			VARCHAR(8)		NOT	NULL	CHECK([RptSchema] <> '')		DEFAULT('dbo'),
	[RptTable]			VARCHAR(40)		NOT	NULL	CHECK([RptTable] <> ''),
	[RptColumn]			VARCHAR(30)		NOT	NULL	CHECK([RptColumn] <> ''),
	[WhereClause]		VARCHAR(98)			NULL	CHECK([WhereClause] <> '')

)
