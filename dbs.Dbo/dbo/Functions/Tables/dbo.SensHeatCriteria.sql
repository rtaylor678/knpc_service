﻿CREATE TABLE [dbo].[SensHeatCriteria] (
    [CriteriaNum] TINYINT             NOT NULL,
    [ProcessID]   [dbo].[ProcessID]   NOT NULL,
    [ProcessType] [dbo].[ProcessType] NOT NULL,
    CONSTRAINT [PK_SensHeatCriteria_2__13] PRIMARY KEY CLUSTERED ([CriteriaNum] ASC, [ProcessID] ASC, [ProcessType] ASC) WITH (FILLFACTOR = 90)
);

