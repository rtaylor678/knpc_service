﻿CREATE TABLE [dbo].[IdleUnits] (
    [RefineryID]  CHAR (6)      NOT NULL,
    [DataSet]     VARCHAR (15)  NOT NULL,
    [UnitID]      INT           NOT NULL,
    [IdledDate]   SMALLDATETIME NOT NULL,
    [RestartDate] SMALLDATETIME NULL,
    CONSTRAINT [PK_IdleUnits] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [DataSet] ASC, [UnitID] ASC, [IdledDate] ASC)
);

