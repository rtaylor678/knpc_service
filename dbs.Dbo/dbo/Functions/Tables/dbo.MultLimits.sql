﻿CREATE TABLE [dbo].[MultLimits] (
    [FactorSet]       [dbo].[FactorSet] NOT NULL,
    [MultGroup]       [dbo].[ProcessID] NOT NULL,
    [MinMultiplicity] NUMERIC (4, 3)    CONSTRAINT [DF_MultLimits_MinMultiplicity] DEFAULT ((1)) NOT NULL,
    [MaxMultiplicity] NUMERIC (4, 3)    CONSTRAINT [DF_MultLimits_MaxMultiplicity] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MultLimits] PRIMARY KEY CLUSTERED ([FactorSet] ASC, [MultGroup] ASC) WITH (FILLFACTOR = 90)
);

