﻿CREATE TABLE [dbo].[CrudePrice] (
    [StudyYear]      [dbo].[StudyYear] NOT NULL,
    [CNum]           CHAR (5)          NOT NULL,
    [BasePrice]      REAL              NOT NULL,
    [APIScale]       TINYINT           NOT NULL,
    [PostedAPI]      REAL              NOT NULL,
    [SaveDate]       SMALLDATETIME     CONSTRAINT [DF_CrudePrice_SaveDate] DEFAULT (getdate()) NOT NULL,
    [CrudeOrigin]    SMALLINT          NULL,
    [PostedSulfur]   REAL              NULL,
    [SulfurAdjSlope] REAL              NULL,
    CONSTRAINT [PK_SAICrude_1__13] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [CNum] ASC) WITH (FILLFACTOR = 90)
);

