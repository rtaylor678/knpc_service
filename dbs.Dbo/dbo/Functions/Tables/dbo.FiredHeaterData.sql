﻿CREATE TABLE [dbo].[FiredHeaterData] (
    [SubmissionID]     INT          NOT NULL,
    [HeaterNo]         INT          NOT NULL,
    [ThroughputRpt]    REAL         NULL,
    [ThroughputUOM]    VARCHAR (12) NULL,
    [FiredDuty]        REAL         NULL,
    [FuelType]         VARCHAR (4)  NULL,
    [OthCombDuty]      REAL         NULL,
    [FurnInTemp]       REAL         NULL,
    [FurnOutTemp]      REAL         NULL,
    [StackTemp]        REAL         NULL,
    [StackO2]          REAL         NULL,
    [HeatLossPcnt]     REAL         NULL,
    [CombAirTemp]      REAL         NULL,
    [AbsorbedDuty]     REAL         NULL,
    [ProcessDuty]      REAL         NULL,
    [SteamDuty]        REAL         NULL,
    [SteamSuperHeated] [dbo].[YorN] NULL,
    [ShaftDuty]        REAL         NULL,
    [OtherDuty]        REAL         NULL,
    CONSTRAINT [PK_FiredHeaterData] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [HeaterNo] ASC)
);

