﻿CREATE TABLE [dbo].[ProcessData] (
    [SubmissionID] INT            NOT NULL,
    [UnitID]       [dbo].[UnitID] NOT NULL,
    [Property]     VARCHAR (30)   NOT NULL,
    [RptValue]     REAL           NULL,
    [RptUOM]       [dbo].[UOM]    NULL,
    [SAValue]      REAL           NULL,
	[RptDValue]	   datetime		  NULL,
	[RptTValue]    VARCHAR(256)   NULL,
    CONSTRAINT [PK_ProcessData] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [UnitID] ASC, [Property] ASC) WITH (FILLFACTOR = 90)
);

