﻿CREATE TABLE [dbo].[ActivityLog] (
    [ActivityTime] DATETIME      NOT NULL,
    [Application]  VARCHAR (20)  NULL,
    [Methodology]  VARCHAR (20)  NULL,
    [RefineryID]   VARCHAR (6)   NOT NULL,
    [CallerIP]     VARCHAR (20)  NOT NULL,
    [UserID]       VARCHAR (20)  NULL,
    [ComputerName] VARCHAR (50)  NULL,
    [Service]      VARCHAR (50)  NULL,
    [Method]       VARCHAR (50)  NULL,
    [EntityName]   VARCHAR (50)  NULL,
    [PeriodStart]  VARCHAR (20)  NULL,
    [PeriodEnd]    VARCHAR (20)  NULL,
    [Notes]        VARCHAR (MAX) NULL,
    [Status]       VARCHAR (50)  NULL
);

