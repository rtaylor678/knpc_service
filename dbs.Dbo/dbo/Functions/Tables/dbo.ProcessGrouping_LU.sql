﻿CREATE TABLE [dbo].[ProcessGrouping_LU] (
    [ProcessGrouping] VARCHAR (8)   NULL,
    [ProcessGroup]    CHAR (1)      NULL,
    [ConvGroup]       CHAR (1)      NULL,
    [IsProcessUnit]   BIT           NULL,
    [Operated]        BIT           NULL,
    [InProcessUtil]   BIT           NULL,
    [AncillaryUnit]   BIT           NULL,
    [Comment]         VARCHAR (MAX) NULL
);

