﻿CREATE FUNCTION [dbo].[SLSumMaterialTotCost](@SubmissionList dbo.SubmissionIDList READONLY, @Currency varchar(4), @Scenario dbo.Scenario)
RETURNS TABLE
AS
RETURN (
	SELECT mtc.Scenario, mtc.Currency, SUM(mtc.RawMatCost) AS RawMatCost, SUM(mtc.ProdValue) AS ProdValue
	, SUM(mtc.ThirdPartyTerminalRM) AS ThirdPartyTerminalRM, SUM(mtc.ThirdPartyTerminalProd) AS ThirdPartyTerminalProd, POXO2 = SUM(mtc.POXO2)
	FROM MaterialTotCost mtc INNER JOIN @SubmissionList sl ON sl.SubmissionID = mtc.SubmissionID
	WHERE mtc.Scenario = ISNULL(@Scenario, mtc.Scenario) AND mtc.Currency = ISNULL(@Currency, mtc.Currency)
	GROUP BY mtc.Scenario, mtc.Currency
)



