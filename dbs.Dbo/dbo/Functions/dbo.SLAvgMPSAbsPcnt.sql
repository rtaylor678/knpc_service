﻿
CREATE FUNCTION [dbo].[SLAvgMPSAbsPcnt](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS real
AS
BEGIN
	DECLARE @AbsPcnt real, @AbsHrs float, @STH float

	SELECT @AbsHrs = MPSAbs FROM dbo.SLSumAbsenceTot(@SubmissionList)
	SELECT @STH = STH FROM dbo.SLSumPersSection(@SubmissionList, 'TM')
	
	IF @STH > 0
		SELECT @AbsPcnt = @AbsHrs/@STH*100
	ELSE
		SET @AbsPcnt = NULL
		
	RETURN @AbsPcnt
	END


