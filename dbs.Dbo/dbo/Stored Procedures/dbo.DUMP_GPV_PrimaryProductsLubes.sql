﻿CREATE PROCEDURE [dbo].[DUMP_GPV_PrimaryProductsLubes]
	@CurrencyCode nvarchar(10),
	@UOM nvarchar(10),
	@PriceCol real,
	@DataSetID nvarchar(10),
	@RefNum nvarchar(10)
	
	AS
	
	SELECT s.Location,s.PeriodStart, s.PeriodEnd,  
        s.NumDays as DaysInPeriod, @CurrencyCode  as Currency,@UOM  as UOM,y.MaterialID,  
        SAIName as MaterialName, ISNULL(SUM(BBL),0) AS BBL,   
         CASE WHEN  SUM(y.BBL)=0 THEN 0   
         ELSE   
         ISNULL(SUM(BBL * PriceLocal) / SUM(BBL), 0) 
         END AS PriceLocal,  
         CASE WHEN  SUM(y.BBL)=0 THEN 0   
         ELSE   
         ISNULL(SUM(BBL * PriceUS) / SUM(BBL), 0) 
         END AS PriceUS,   
         ISNULL(SUM(BBL),0)*ISNULL(SUM( @priceCol ),0)/1000 As GPV,  
         MIN(m.SortKey) AS SortKey   
         FROM Yield y  
         INNER JOIN Submissions s ON  y.SubmissionID=s.SubmissionID AND s.DataSet=@DataSetID  AND  s.RefineryID=@RefNum
         INNER JOIN Material_LU m ON m.MaterialID = y.MaterialID   
         INNER JOIN TSort t ON t.RefineryID =@RefNum 
        WHERE(AllowInPROD = 1 And LubesOnly = 1 And FuelsLubesCombo = 1)  
         GROUP BY s.PeriodStart,s.PeriodEnd,s.Location,s.NumDays,y.MaterialID, SAIName ORDER BY s.PeriodStart DESC,SortKey ASC


