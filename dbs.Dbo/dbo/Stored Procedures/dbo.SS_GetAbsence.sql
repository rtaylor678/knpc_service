﻿CREATE PROC [dbo].[SS_GetAbsence]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime
	
AS

SELECT OCCAbs,MPSAbs,RTRIM(CategoryID) As CategoryID 
             FROM dbo.Absence WHERE  (SubmissionID IN 
             (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID AND usesubmission=1
           AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd )))) 


