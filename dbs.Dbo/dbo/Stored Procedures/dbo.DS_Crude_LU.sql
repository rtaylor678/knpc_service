﻿CREATE PROC [dbo].[DS_Crude_LU]
	
AS

SELECT RTRIM(CNum) as CNum, RTRIM(CrudeName) as CrudeName, TypicalAPI, TypicalSulfur,
                    RTRIM(ProdCountry) as ProdCountry, SortKey 
                    FROM Crude_LU 
                    ORDER BY ProdCountry, CrudeName

