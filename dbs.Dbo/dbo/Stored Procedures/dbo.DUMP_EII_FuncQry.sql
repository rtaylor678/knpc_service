﻿CREATE PROCEDURE [dbo].[DUMP_EII_FuncQry]
	@CurrencyCode nvarchar(10),
	@UOM nvarchar(10),
	@StudyYear nvarchar(20),
	@DataSetID nvarchar(10),
	@RefNum nvarchar(10)
	
	AS
	
	SELECT s.Location,s.PeriodStart,s.PeriodEnd, 
                  s.NumDays as DaysInPeriod,@CurrencyCode AS Currency,@UOM as UOM ,  
                   p.Description,c.SortKey,c.UnitName, c.unitid, c.ProcessID, 
                   c.processType, c.utilcap, fc.stdenergy, fc.stdgain,  
                  VEIFormulaForReport, EIIFormulaForReport, displaytextUS 
                   FROM Config c, factorcalc fc, factors f, processid_lu p, displayunits_lu d , Submissions s  
                   WHERE c.unitid = fc.unitid AND c.submissionId = fc.submissionid    
                   AND f.factorset=fc.factorset AND f.processid = c.processid AND f.processtype = c.processtype AND f.factorset=@StudyYear
                  AND c.processid = p.processid AND c.PROCESSID NOT IN ('STEAMGEN','ELECGEN','FCCPOWER','FTCOGEN','BLENDING','TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') and p.displayunits = d.displayunits 
                   AND s.SubmissionID=c.SubmissionID AND c.submissionid IN  
                  (SELECT Distinct SubmissionID FROM dbo.Submissions WHERE  DataSet=@DataSetID
                  AND  RefineryID=@RefNum
                   ) ORDER BY PeriodStart DESC, c.SortKey


