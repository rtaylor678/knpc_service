﻿CREATE   PROCEDURE [dbo].[BblMTGravity] (
	@Bbl float OUTPUT, 
	@MT float OUTPUT, 
	@API real OUTPUT, 
	@Density real OUTPUT)
AS
IF (@API IS NOT NULL AND @Density IS NULL)
	SELECT @Density = dbo.ConvertAPItoKGM3(@API)
IF @Bbl IS NULL
	SELECT @Bbl = @MT/(@Density/6289.0)
IF @MT IS NULL
	SELECT @MT = @Bbl*@Density/6289.0
IF @Density IS NULL
	SELECT @Density = @MT/(@Bbl/6289.0)
IF @API IS NULL
	SELECT @API = dbo.ConvertKGM3toAPI(@Density)

