﻿CREATE  PROC [dbo].[RVComponents](@FactorSet FactorSet, @RefineryType varchar(5), @Complexity real, @ProcessUtilityRV real, 
	@ProcOS real OUTPUT, @NonProcOS real OUTPUT, @CatChem real OUTPUT, @SpareParts real OUTPUT, @TotRV real OUTPUT, @ComplexityLimited bit OUTPUT)
AS
DECLARE @cmplx real, @OffsitesRVCmplxMax real, @OffsitesRVCmplxDef real, 
	@ProcessOffsitesRVConstant real, @ProcessOffsitesRVCmplxFactor real,
	@NonProcessOffsitesRVConstant real, @NonProcessOffsitesRVCmplxFactor real,
	@CatChemRVCmplxMax real, @CatChemRVCmplxDef real, 
	@CatChemRVConstant real, @CatChemRVCmplxFactor real, @SparePartsRVConstant real
SELECT @ComplexityLimited = 0, @cmplx = @Complexity
SELECT @OffsitesRVCmplxMax = OffsitesRVCmplxMax, @OffsitesRVCmplxDef = OffsitesRVCmplxDef, 
	@ProcessOffsitesRVConstant = ProcessOffsitesRVConstant, @ProcessOffsitesRVCmplxFactor = ProcessOffsitesRVCmplxFactor,
	@NonProcessOffsitesRVConstant = NonProcessOffsitesRVConstant, @NonProcessOffsitesRVCmplxFactor = NonProcessOffsitesRVCmplxFactor,
	@CatChemRVCmplxMax = CatChemRVCmplxMax, @CatChemRVCmplxDef = CatChemRVCmplxDef, 
	@CatChemRVConstant = CatChemRVConstant, @CatChemRVCmplxFactor = CatChemRVCmplxFactor, @SparePartsRVConstant = SparePartsRVConstant
FROM FactorSets WHERE RefineryType = @refineryType AND FactorSet = @FactorSet
IF @OffsitesRVCmplxMax IS NOT NULL
BEGIN
     IF @cmplx >= @OffsitesRVCmplxMax
	SELECT @ComplexityLimited = 1, @cmplx = @OffsitesRVCmplxDef
END
SELECT 	@ProcOS = @ProcessUtilityRV * (@ProcessOffsitesRVConstant + (@cmplx * @ProcessOffsitesRVCmplxFactor)),
	@NonProcOS = @ProcessUtilityRV * (@NonProcessOffsitesRVConstant + (@cmplx * @NonProcessOffsitesRVCmplxFactor))

SELECT @cmplx = @Complexity
IF @CatChemRVCmplxMax IS NOT NULL
BEGIN
	IF @cmplx >= @CatChemRVCmplxMax
        	SELECT @ComplexityLimited = 1, @cmplx = @CatChemRVCmplxDef
END
SELECT 	@CatChem = @ProcessUtilityRV * (@CatChemRVConstant + (@cmplx * @CatChemRVCmplxFactor)),
	@SpareParts = @ProcessUtilityRV * @SparePartsRVConstant
SELECT 	@TotRV = @ProcessUtilityRV + @ProcOS + @NonProcOS + @CatChem + @SpareParts


