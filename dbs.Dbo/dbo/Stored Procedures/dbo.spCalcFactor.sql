﻿CREATE  PROC [dbo].[spCalcFactor](@ActualCap real, @FactorConstant real, @FactorExponent real, @MinCap real, @MaxCap real, @Factor real OUTPUT, @FactorValue real OUTPUT)
AS
	IF @ActualCap > 0
	BEGIN
		SELECT @Factor = @FactorConstant*POWER(CASE WHEN @ActualCap < @MinCap THEN @MinCap WHEN @ActualCap > @MaxCap THEN @MaxCap ELSE @ActualCap END, @FactorExponent)
		SELECT @FactorValue = @Factor*@ActualCap
	END
	ELSE
		SELECT @Factor = NULL, @FactorValue = 0

