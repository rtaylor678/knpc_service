﻿CREATE PROCEDURE [dbo].[DS_ConfigRSSchema]
	
AS
BEGIN
	SELECT RTRIM(ProcessID) as ProcessID, Throughput as RailcarBBL, Throughput as TankTruckBBL, Throughput as TankerBerthBBL, Throughput as OffshoreBuoyBBL, Throughput as BargeBerthBBL, Throughput as PipelineBBL FROM ConfigRS WHERE SubmissionID = 0
END

