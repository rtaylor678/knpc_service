﻿


CREATE   PROC [dbo].[spReportGazpromKPICalc2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15), 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5),
	@EII real = NULL OUTPUT, @EII_QTR real = NULL OUTPUT, @EII_AVG real = NULL OUTPUT, @EII_YTD real = NULL OUTPUT, 
	@EnergyUseDay real = NULL OUTPUT, @EnergyUseDay_QTR real = NULL OUTPUT, @EnergyUseDay_AVG real = NULL OUTPUT, @EnergyUseDay_YTD real = NULL OUTPUT, 
	@TotStdEnergy real = NULL OUTPUT, @TotStdEnergy_QTR real = NULL OUTPUT, @TotStdEnergy_AVG real = NULL OUTPUT, @TotStdEnergy_YTD real = NULL OUTPUT, 
	@RefUtilPcnt real = NULL OUTPUT, @RefUtilPcnt_QTR real = NULL OUTPUT, @RefUtilPcnt_AVG real = NULL OUTPUT, @RefUtilPcnt_YTD real = NULL OUTPUT, 
	@EDC real = NULL OUTPUT, @EDC_QTR real = NULL OUTPUT, @EDC_AVG real = NULL OUTPUT, @EDC_YTD real = NULL OUTPUT, 
	@UEDC real = NULL OUTPUT, @UEDC_QTR real = NULL OUTPUT, @UEDC_AVG real = NULL OUTPUT, @UEDC_YTD real = NULL OUTPUT, 
	@VEI real = NULL OUTPUT, @VEI_QTR real = NULL OUTPUT, @VEI_AVG real = NULL OUTPUT, @VEI_YTD real = NULL OUTPUT, 
	@ReportLossGain real = NULL OUTPUT, @ReportLossGain_QTR real = NULL OUTPUT, @ReportLossGain_AVG real = NULL OUTPUT, @ReportLossGain_YTD real = NULL OUTPUT, 
	@EstGain real = NULL OUTPUT, @EstGain_QTR real = NULL OUTPUT, @EstGain_AVG real = NULL OUTPUT, @EstGain_YTD real = NULL OUTPUT, 
	@OpAvail real = NULL OUTPUT, @OpAvail_QTR real = NULL OUTPUT, @OpAvail_AVG real = NULL OUTPUT, @OpAvail_YTD real = NULL OUTPUT, 
	@MechUnavailTA real = NULL OUTPUT, @MechUnavailTA_QTR real = NULL OUTPUT, @MechUnavailTA_AVG real = NULL OUTPUT, @MechUnavailTA_YTD real = NULL OUTPUT, 
	@NonTAUnavail real = NULL OUTPUT, @NonTAUnavail_QTR real = NULL OUTPUT, @NonTAUnavail_AVG real = NULL OUTPUT, @NonTAUnavail_YTD real = NULL OUTPUT, 
	@RoutIndex real = NULL OUTPUT, @RoutIndex_QTR real = NULL OUTPUT, @RoutIndex_AVG real = NULL OUTPUT, @RoutIndex_YTD real = NULL OUTPUT,
	@RoutCost real = NULL OUTPUT, @RoutCost_QTR real = NULL OUTPUT, @RoutCost_AVG real = NULL OUTPUT, @RoutCost_YTD real = NULL OUTPUT, 
	@PersIndex real = NULL OUTPUT, @PersIndex_QTR real = NULL OUTPUT, @PersIndex_AVG real = NULL OUTPUT, @PersIndex_YTD real = NULL OUTPUT, 
	@AnnTAWhr real = NULL OUTPUT, @AnnTAWhr_QTR real = NULL OUTPUT, @AnnTAWhr_AVG real = NULL OUTPUT, @AnnTAWhr_YTD real = NULL OUTPUT,
	@NonTAWHr real = NULL OUTPUT, @NonTAWHr_QTR real = NULL OUTPUT, @NonTAWHr_AVG real = NULL OUTPUT, @NonTAWHr_YTD real = NULL OUTPUT,
	@NEOpexEDC real = NULL OUTPUT, @NEOpexEDC_QTR real = NULL OUTPUT, @NEOpexEDC_AVG real = NULL OUTPUT, @NEOpexEDC_YTD real = NULL OUTPUT, 
	@NEOpex real = NULL OUTPUT, @NEOpex_QTR real = NULL OUTPUT, @NEOpex_AVG real = NULL OUTPUT, @NEOpex_YTD real = NULL OUTPUT, 
	@OpexUEDC real = NULL OUTPUT, @OpexUEDC_QTR real = NULL OUTPUT, @OpexUEDC_AVG real = NULL OUTPUT, @OpexUEDC_YTD real = NULL OUTPUT, 
	@TAAdj real = NULL OUTPUT, @TAAdj_QTR real = NULL OUTPUT, @TAAdj_AVG real = NULL OUTPUT, @TAAdj_YTD real = NULL OUTPUT,
	@EnergyCost real = NULL OUTPUT, @EnergyCost_QTR real = NULL OUTPUT, @EnergyCost_AVG real = NULL OUTPUT, @EnergyCost_YTD real = NULL OUTPUT, 
	@TotCashOpex real = NULL OUTPUT, @TotCashOpex_QTR real = NULL OUTPUT, @TotCashOpex_AVG real = NULL OUTPUT, @TotCashOpex_YTD real = NULL OUTPUT
	)
AS

SELECT @EII = NULL, @EII_QTR = NULL, @EII_AVG = NULL, @EII_YTD = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_QTR = NULL, @EnergyUseDay_AVG = NULL, @EnergyUseDay_YTD = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_QTR = NULL, @TotStdEnergy_AVG = NULL, @TotStdEnergy_YTD = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_QTR = NULL, @RefUtilPcnt_AVG = NULL, @RefUtilPcnt_YTD = NULL, 
	@EDC = NULL, @EDC_QTR = NULL, @EDC_AVG = NULL, @EDC_YTD = NULL, 
	@UEDC = NULL, @UEDC_QTR = NULL, @UEDC_AVG = NULL, @UEDC_YTD = NULL, 
	@VEI = NULL, @VEI_QTR = NULL, @VEI_AVG = NULL, @VEI_YTD = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_QTR = NULL, @ReportLossGain_AVG = NULL, @ReportLossGain_YTD = NULL,
	@EstGain = NULL, @EstGain_QTR = NULL, @EstGain_AVG = NULL, @EstGain_YTD = NULL, 
	@OpAvail = NULL, @OpAvail_QTR = NULL, @OpAvail_AVG = NULL, @OpAvail_YTD = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_QTR = NULL, @MechUnavailTA_AVG = NULL, @MechUnavailTA_YTD = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_QTR = NULL, @NonTAUnavail_AVG = NULL, @NonTAUnavail_YTD = NULL, 
	@RoutIndex = NULL, @RoutIndex_QTR = NULL, @RoutIndex_AVG = NULL, @RoutIndex_YTD = NULL,
	@RoutCost = NULL, @RoutCost_QTR = NULL, @RoutCost_AVG = NULL, @RoutCost_YTD = NULL, 
	@PersIndex = NULL, @PersIndex_QTR = NULL, @PersIndex_AVG = NULL, @PersIndex_YTD = NULL, 
	@AnnTAWhr = NULL, @AnnTAWhr_QTR = NULL, @AnnTAWhr_AVG = NULL, @AnnTAWhr_YTD = NULL,
	@NonTAWHr = NULL, @NonTAWHr_QTR = NULL, @NonTAWHr_AVG = NULL, @NonTAWHr_YTD = NULL,
	@NEOpexEDC = NULL, @NEOpexEDC_QTR = NULL, @NEOpexEDC_AVG = NULL, @NEOpexEDC_YTD = NULL, 
	@NEOpex = NULL, @NEOpex_QTR = NULL, @NEOpex_AVG = NULL, @NEOpex_YTD = NULL, 
	@OpexUEDC = NULL, @OpexUEDC_QTR = NULL, @OpexUEDC_AVG = NULL, @OpexUEDC_YTD = NULL, 
	@TAAdj = NULL, @TAAdj_QTR = NULL, @TAAdj_AVG = NULL, @TAAdj_YTD = NULL,
	@EnergyCost = NULL, @EnergyCost_QTR = NULL, @EnergyCost_AVG = NULL, @EnergyCost_YTD = NULL, 
	@TotCashOpex = NULL, @TotCashOpex_QTR = NULL, @TotCashOpex_AVG = NULL, @TotCashOpex_YTD = NULL

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = Start3Mo, @Start12Mo = Start12Mo, @StartYTD = StartYTD, @Start24Mo = Start24Mo
FROM dbo.GetPeriods(@SubmissionID)

IF DATEPART(yy, @Start3Mo) < 2010
	SET @Start3Mo = '12/31/2009'
IF DATEPART(yy, @Start12Mo) < 2010
	SET @Start12Mo = '12/31/2009'
	
DECLARE @SubListMonth dbo.SubmissionIDList, @SubList3Mo dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @PeriodStart, @PeriodEnd)
INSERT @SubList3Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start3Mo, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @Dataset, @Start24Mo, @PeriodEnd)

SELECT @EII = m.EII, @EII_QTR = avg3mo.EII, @EII_AVG = avg12mo.EII, @EII_YTD = avgYTD.EII, 
	@EnergyUseDay = m.EnergyUseDay/1000, @EnergyUseDay_QTR = avg3mo.EnergyUseDay/1000, @EnergyUseDay_AVG = avg12mo.EnergyUseDay/1000, @EnergyUseDay_YTD = avgYTD.EnergyUseDay/1000, 
	@TotStdEnergy = m.TotStdEnergy/1000, @TotStdEnergy_QTR = avg3mo.TotStdEnergy/1000, @TotStdEnergy_AVG = avg12mo.TotStdEnergy/1000, @TotStdEnergy_YTD = avgYTD.TotStdEnergy/1000, 
	@RefUtilPcnt = m.RefUtilPcnt, @RefUtilPcnt_QTR = avg3mo.RefUtilPcnt, @RefUtilPcnt_AVG = avg12mo.RefUtilPcnt, @RefUtilPcnt_YTD = avgYTD.RefUtilPcnt, 
	@EDC = m.EDC/1000, @EDC_QTR = avg3mo.EDC/1000, @EDC_AVG = avg12mo.EDC/1000, @EDC_YTD = avgYTD.EDC/1000, 
	@UEDC = m.UEDC/1000, @UEDC_QTR = avg3mo.UEDC/1000, @UEDC_AVG = avg12mo.UEDC/1000, @UEDC_YTD = avgYTD.UEDC/1000, 
	@VEI = m.VEI, @VEI_QTR = avg3mo.VEI, @VEI_AVG = avg12mo.VEI, @VEI_YTD = avgYTD.VEI, 
	@ReportLossGain = m.ReportLossGain, @ReportLossGain_QTR = avg3mo.ReportLossGain, @ReportLossGain_AVG = avg12mo.ReportLossGain, @ReportLossGain_YTD = avgYTD.ReportLossGain, 
	@EstGain = m.EstGain, @EstGain_QTR = avg3mo.EstGain, @EstGain_AVG = avg12mo.EstGain, @EstGain_YTD = avgYTD.EstGain, 
	@OpAvail = m.OpAvail, @OpAvail_QTR = avg3mo.OpAvail, @OpAvail_AVG = avg24mo.OpAvail, @OpAvail_YTD = avgYTD.OpAvail, 
	@MechUnavailTA = m.MechUnavailTA, @MechUnavailTA_QTR = avg3mo.MechUnavailTA, @MechUnavailTA_AVG = avg24mo.MechUnavailTA, @MechUnavailTA_YTD = avgYTD.MechUnavailTA, 
	@NonTAUnavail = m.MechUnavailRout + m.RegUnavail, @NonTAUnavail_QTR = avg3mo.MechUnavailRout + avg3mo.RegUnavail, @NonTAUnavail_AVG = avg24mo.MechUnavailRout + avg24mo.RegUnavail, @NonTAUnavail_YTD = avgYTD.MechUnavailRout + avgYTD.RegUnavail, 
	@RoutIndex = m.RoutIndex, @RoutIndex_QTR = avg3mo.RoutIndex, @RoutIndex_AVG = mi24.RoutIndex, @RoutIndex_YTD = avgYTD.RoutIndex,
	@RoutCost = m.RoutCost/1000, @RoutCost_QTR = avg3mo.RoutCost/1000, @RoutCost_AVG = mi24.RoutEffIndex*avg24mo.MaintEffDiv/100/1e6, @RoutCost_YTD = avgYTD.RoutCost/1000, 
	@PersIndex = m.PersIndex, @PersIndex_QTR = avg3mo.PersIndex, @PersIndex_AVG = avg12mo.PersIndex, @PersIndex_YTD = avgYTD.PersIndex, 
	@AnnTAWhr = m.MaintTAWHr_k, @AnnTAWhr_QTR = avg3mo.MaintTAWHr_k, @AnnTAWhr_AVG = avg12mo.MaintTAWHr_k, @AnnTAWhr_YTD = avgYTD.MaintTAWHr_k,
	@NonTAWHr = m.TotNonTAWHr_k, @NonTAWHr_QTR = avg3mo.TotNonTAWHr_k, @NonTAWHr_AVG = avg12mo.TotNonTAWHr_k, @NonTAWHr_YTD = avgYTD.TotNonTAWHr_k,
	@NEOpexEDC = m.NEOpexEDC, @NEOpexEDC_QTR = avg3mo.NEOpexEDC, @NEOpexEDC_AVG = avg12mo.NEOpexEDC, @NEOpexEDC_YTD = avgYTD.NEOpexEDC, 
	@NEOpex = m.NEOpex/1000, @NEOpex_QTR = avg3mo.NEOpex/1000, @NEOpex_AVG = avg12mo.NEOpex/1000, @NEOpex_YTD = avgYTD.NEOpex/1000, 
	@OpexUEDC = m.OpexUEDC, @OpexUEDC_QTR = avg3mo.OpexUEDC, @OpexUEDC_AVG = avg12mo.OpexUEDC, @OpexUEDC_YTD = avgYTD.OpexUEDC, 
	@TAAdj = m.AnnTACost/1000, @TAAdj_QTR = avg3mo.AnnTACost/1000, @TAAdj_AVG = avg12mo.AnnTACost/1000, @TAAdj_YTD = avgYTD.AnnTACost/1000,
	@EnergyCost = m.EnergyCost/1000, @EnergyCost_QTR = avg3mo.EnergyCost/1000, @EnergyCost_AVG = avg12mo.EnergyCost/1000, @EnergyCost_YTD = avgYTD.EnergyCost/1000, 
	@TotCashOpex = m.TotCashOpex/1000, @TotCashOpex_QTR = avg3mo.TotCashOpex/1000, @TotCashOpex_AVG = avg12mo.TotCashOpex/1000, @TotCashOpex_YTD = avgYTD.TotCashOpex/1000
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList3Mo, @FactorSet, @Scenario, @Currency, @UOM) avg3mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM) avgYTD ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1
LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1


