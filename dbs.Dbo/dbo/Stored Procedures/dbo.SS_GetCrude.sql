﻿
CREATE PROC [dbo].[SS_GetCrude]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime
	
AS

SELECT RTRIM(CNum) as CNum,RTRIM(CrudeName) AS CrudeName,Gravity,Sulfur,BBL,CostPerBBL
            FROM dbo.Crude WHERE  (SubmissionID IN 
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions 
            WHERE RefineryID=@RefineryID and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd))))


