﻿CREATE PROCEDURE [dbo].[EtlOutput]
	@Methodology VARCHAR(20),
	@SubmissionId INT --,	@Test VARCHAR(600) OUT
AS
	--cant use, someone put a unique constraing on the Logging table !!!!!
	--INSERT INTO  [GlobalDB].[dbo].[Logging]([CrudAction]) VALUES('KNPC EtlOutput DECLARE SqlCursor');

	DECLARE @SqlInsert		NVARCHAR(MAX);
	DECLARE @SqlCommand		NVARCHAR(MAX);
	DECLARE @DataKey		NVARCHAR(60);
	DECLARE @DataType		CHAR;
	DECLARE @Db				VARCHAR(20);
	DECLARE @DbSchema		VARCHAR(8);
	DECLARE @DbTable		VARCHAR(40);
	DECLARE @DbColumn		VARCHAR(30);
	DECLARE @WhereClause	NVARCHAR(98);
	DECLARE @InsertAnyTypeValue	NVARCHAR(MAX);
	DECLARE @InsertNValue	NVARCHAR(MAX);
	DECLARE @InsertTValue	NVARCHAR(MAX);
	DECLARE @InsertDValue	DATE;
	DECLARE @DeleteCommand		NVARCHAR(70);

	--limit this to only submissions from KNPC or others in the ClientKeys table
	IF EXISTS (select c.ClientKeysID from pearl.ClientKeys c inner join dbo.SubmissionsAll s on s.RefineryID = c.Refnumber where s.SubmissionID=@SubmissionId) 
	BEGIN

		SET @DeleteCommand = N'DELETE FROM output.DataSubmission where SubmissionID =' 
			+  CAST(@SubmissionId AS varchar(20));	
		exec sp_executesql @DeleteCommand;

		DECLARE SqlCursor CURSOR FAST_FORWARD
		FOR
		SELECT k.DataType, e.DataKey,	
		CONVERT(NVARCHAR(MAX)	,
		 'SELECT @InsertAnyTypeValue =  [' +  e.RptColumn + '] FROM [' +  e.RptDatabase + '].[' +  e.RptSchema + '].[' +  e.RptTable 
		 + '] WHERE ' +  e.WhereClause + ' AND SUBMISSIONID = ' + CAST(@SubmissionId AS varchar(20)) + ';'
		 )  
		from dbo.Etl e 
		inner join output.DataKeys k on e.DataKey= k.DataKey	
		ORDER BY [e].[DataKey] ASC

		OPEN SqlCursor;
		FETCH NEXT FROM SqlCursor INTO @DataType, @DataKey, @SqlCommand;
		--INSERT INTO [GlobalDB].[dbo].[Logging]([CrudAction]) VALUES('KNPC EtlOutput Run SqlCursor');
		WHILE @@FETCH_STATUS = 0
		BEGIN
			exec sp_executesql @SqlCommand, N'@InsertAnyTypeValue NVARCHAR(MAX) OUTPUT', @InsertAnyTypeValue OUTPUT;
			IF @DataType = 'N'
				BEGIN 
					SET @InsertAnyTypeValue =  ISNULL(@InsertAnyTypeValue,'NULL')
					SET @InsertNValue = CAST(@InsertAnyTypeValue As nvarchar(MAX));
					SET @SqlInsert = N'insert into output.DataSubmission([SubmissionID],[DataKey], [StudyMethodology] ,[NumberValue],[TextValue],[DateVal], [CalcTime] ) ' 
								+ 'Values (' + CAST(@SubmissionId AS nvarchar(20))  + ', ''' + @DataKey + ''', ' 
								+ @Methodology + ','  + @InsertNValue + ', NULL, NULL, GETDATE() ) ; ';				
				END
			IF @DataType = 'T' --NOT TESTED
				BEGIN
					SET @InsertAnyTypeValue =  ISNULL(@InsertAnyTypeValue,'NULL')
					SET @InsertTValue = CAST(@InsertAnyTypeValue As nvarchar(MAX));
					SET @SqlInsert = N'insert into output.DataSubmission([SubmissionID],[DataKey], [StudyMethodology] ,[NumberValue],[TextValue],[DateVal], [CalcTime] ) ' 
								+ 'Values (' + CAST(@SubmissionId AS nvarchar(20))  + ', ''' + @DataKey + ''', ' 
								+ @Methodology + ','''  + @InsertTValue + ''', NULL, NULL, GETDATE() ) ; ';				
				END
			IF @DataType = 'D'	 --NOT TESTED
				BEGIN	
					SET @InsertAnyTypeValue =  ISNULL(@InsertAnyTypeValue,'NULL')
					SET @InsertDValue = CAST(@InsertAnyTypeValue As date);
					SET @SqlInsert = N'insert into output.DataSubmission([SubmissionID],[DataKey], [StudyMethodology] ,[NumberValue],[TextValue],[DateVal], [CalcTime] ) ' 
								+ 'Values (' + CAST(@SubmissionId AS nvarchar(20))  + ', ''' + @DataKey + ''', ' 
								+ @Methodology + ','''  +  CAST(@InsertDValue AS nvarchar(30)) + ''', NULL, NULL, GETDATE() ) ; ';				
				END	
			
			BEGIN TRY
				EXECUTE sp_executesql @SqlInsert;
			END TRY
			BEGIN CATCH
				--INSERT INTO [GlobalDB].[dbo].[Logging]([CrudAction]) VALUES('ERROR: KNPC Insert ' + @DataKey);
				/*
				PRINT @DataKey;
				PRINT '';
				PRINT @SqlCommand;
				*/
				BREAK;
			END CATCH;
			FETCH NEXT FROM SqlCursor INTO @DataType, @DataKey, @SqlCommand;
		END; --END WHILE
		CLOSE SqlCursor;
		DEALLOCATE SqlCursor;
		--INSERT INTO [GlobalDB].[dbo].[Logging]([CrudAction]) VALUES('KNPC EtlOutput SELECT * FROM DataTable');
	END
RETURN

