﻿CREATE PROCEDURE [dbo].[DS_ElectricSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT RTRIM(TransType) as TransType, RTRIM(TransferTo) as TransferTo, RTRIM(EnergyType) as EnergyType, TransCode As SortKey,0.0 AS RptGenEff,0.0 AS RptMWH,0.0 AS PriceLocal 
		FROM Electric 
		WHERE SubmissionID=(SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID ORDER BY PeriodStart DESC) 
END



