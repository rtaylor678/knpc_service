﻿CREATE PROC [dbo].[DS_Opex_LU]
	
AS

SELECT OpexID,RTRIM(Description) AS Description,Indent, RTRIM(ParentID) AS ParentID, RTRIM(DetailStudy) AS DetailStudy, RTRIM(DetailProfile) AS DetailProfile,SortKey FROM Opex_LU ORDER BY SortKey

