﻿CREATE PROC [dbo].[SS_GetInputCrude]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            RTRIM(CNum) as CNum,RTRIM(CrudeName) AS CrudeName,Gravity,Sulfur,BBL,CostPerBBL 
            FROM  
            dbo.Crude c 
            ,dbo.Submissions s  
            WHERE   
            c.SubmissionID = s.SubmissionID AND 
            (c.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions WHERE RefineryID = @RefineryID and DataSet = @Dataset and UseSubmission=1 ))


