﻿CREATE PROCEDURE [dbo].[DS_MaintRoutSchema]
	@RefineryID nvarchar(10),
	@Dataset nvarchar(20) = 'ACTUAL'
AS
BEGIN
SELECT UnitID, RTRIM(ProcessID) as ProcessID, CAST(0.0 AS REAL) as RoutCostLocal,CAST(0.0 AS REAL) as RoutExpLocal, CAST(0.0 AS REAL) as RoutCptlLocal, CAST(0.0 AS REAL) as RoutOvhdLocal,  
                  CAST(0 AS SMALLINT) as RegNum,CAST(0 AS SMALLINT) as MaintNum,CAST(0 AS SMALLINT) as OthNum,CAST(0.0 AS REAL) as OthDownEconomic,  
                  CAST(0.0 AS REAL) as OthDownExternal, CAST(0.0 AS REAL) as OthDownUnitUpsets,CAST(0.0 AS REAL) as OthDownOffsiteUpsets, 
                  CAST(0.0 AS REAL) as OthDownOther,
                  CAST(0.0 AS REAL)as RegDown,CAST(0.0 AS REAL) as MaintDown, 
                  CAST(0.0 AS REAL) as OthDown,CAST(0 AS SMALLINT) AS SortKey,RTRIM(ProcessID) as UnitName 
FROM MAINTROUT WHERE SubmissionID = (SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID AND DataSet = @Dataset ORDER BY PeriodStart DESC)
END


