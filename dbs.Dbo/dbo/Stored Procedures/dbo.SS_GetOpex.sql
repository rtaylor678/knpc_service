﻿CREATE PROC [dbo].[SS_GetOpex]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@Dataset nvarchar(20)='ACTUAL'
	
AS

	SELECT ThirdPartyTerminalProd,ThirdPartyTerminalRM,OthRevenue,
            OthNonVol,OthVol,PurOth,Catalysts,Chemicals,GAPers,Envir,OthCont,
            ContMaintLabor,MaintMatl,MPSBen,OCCBen,MPSSal,OCCSal,POXO2
             FROM dbo.OpEx WHERE DataType='RPT' AND (SubmissionID IN 
             (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @Dataset and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @periodEnd))))


