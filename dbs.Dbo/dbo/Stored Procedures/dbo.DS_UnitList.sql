﻿CREATE PROC [dbo].[DS_UnitList]
	@RefineryID nvarchar(20),
	@Dataset nvarchar(20)='ACTUAL'
AS

SELECT c.UnitID, TAID = ISNULL(max(m.TAID) , 0) 
               FROM Config c INNER JOIN dbo.Submissions s ON s.SubmissionID = c.SubmissionID
               LEFT JOIN MaintTA m ON m.RefineryId = s.RefineryID AND m.DataSet = s.DataSet AND m.Unitid = c.Unitid 
               WHERE s.RefineryID =@RefineryID and s.DataSet = @Dataset and UseSubmission=1 GROUP BY c.UnitID


