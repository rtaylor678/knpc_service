﻿CREATE   PROC [dbo].[spReportCrude] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays AS DaysInPeriod, Currency = @Currency,
c.CNum, c.CrudeName AS MaterialName, Bbl = CAST(c.Bbl AS int), 
PricePerBbl = CAST(cc.PricePerBbl*dbo.ExchangeRate('USD',@Currency,s.PeriodStart) AS decimal(6, 2)),
Value = CAST(Bbl*cc.PricePerBbl*dbo.ExchangeRate('USD',@Currency,s.PeriodStart)/1000 AS decimal(15, 1))
FROM Submissions s INNER JOIN Crude c ON c.SubmissionID = s.SubmissionID
INNER JOIN CrudeCalc cc ON cc.SubmissionID = s.SubmissionID AND cc.CrudeID = c.CrudeID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
AND cc.Scenario = @Scenario
ORDER BY s.PeriodStart, c.CrudeID


