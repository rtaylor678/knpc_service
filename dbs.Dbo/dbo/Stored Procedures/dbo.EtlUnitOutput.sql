﻿CREATE PROCEDURE [dbo].[EtlUnitOutput]
	@Methodology VARCHAR(20),
	@SubmissionId INT ,	
	@UnitId INT
AS
	--cant use, someone put a unique constraint on the Logging table !!!!!
	--INSERT INTO  [GlobalDB].[dbo].[Logging]([CrudAction]) VALUES('KNPC EtlOutput DECLARE SqlCursor');

	DECLARE @SqlInsert		NVARCHAR(MAX);
	DECLARE @SqlCommand		NVARCHAR(MAX);
	DECLARE @DataKey		NVARCHAR(60);
	DECLARE @DataType		CHAR;
	DECLARE @Db				VARCHAR(20);
	DECLARE @DbSchema		VARCHAR(8);
	DECLARE @DbTable		VARCHAR(40);
	DECLARE @DbColumn		VARCHAR(30);
	DECLARE @WhereClause	NVARCHAR(98);
	DECLARE @InsertAnyTypeValue	NVARCHAR(MAX);
	DECLARE @InsertNValue	NVARCHAR(MAX);
	DECLARE @InsertTValue	NVARCHAR(MAX);
	DECLARE @InsertDValue	DATE;
	DECLARE @RptColumn		NVARCHAR(MAX);
	DECLARE @UnitIdString	NVARCHAR(10);
	
	--limit this to only submissions from KNPC or others in the ClientKeys table
	IF EXISTS (select c.ClientKeysID from pearl.ClientKeys c inner join dbo.SubmissionsAll s on s.RefineryID = c.Refnumber where s.SubmissionID=@SubmissionId) 
	BEGIN

		SET @UnitIdString = CAST(@UnitId As nvarchar(10));

		DECLARE SqlCursor CURSOR FAST_FORWARD
		FOR
		SELECT k.DataType, e.DataKey, e.RptColumn,
		CONVERT(NVARCHAR(MAX)	,
		 'SELECT @InsertAnyTypeValue =  [' +  e.RptColumn + '] FROM [' +  e.RptDatabase + '].[' +  e.RptSchema + '].[' +  e.RptTable 
			+ '] WHERE ' +  e.WhereClause + ' AND SUBMISSIONID = ' + CAST(@SubmissionId AS varchar(20)) + 
			' AND UNITID = ' + @UnitIdString + ';'
			)  
		from dbo.EtlUnit e 
		inner join output.DataKeysUnits k on e.DataKey= k.DataKey	
		ORDER BY [e].[DataKey] ASC
		
		OPEN SqlCursor;
		FETCH NEXT FROM SqlCursor INTO @DataType, @DataKey, @RptColumn, @SqlCommand;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			exec sp_executesql @SqlCommand, N'@InsertAnyTypeValue NVARCHAR(MAX) OUTPUT', @InsertAnyTypeValue OUTPUT;
		
			IF @DataType = 'N'
				BEGIN 
					SET @InsertAnyTypeValue =  ISNULL(@InsertAnyTypeValue,'NULL')
					SET @InsertNValue = CAST(@InsertAnyTypeValue As nvarchar(MAX));
					SET @SqlInsert = N'insert into output.ProcessData([SubmissionID],[DataKey], [UnitID], [StudyMethodology] ,[NumberValue],[TextValue],[DateVal], [CalcTime] ) ' 
								+ 'Values (' + CAST(@SubmissionId AS nvarchar(20))  + ', ''' + @DataKey + ''', ' + @UnitIdString + ','
								+ @Methodology + ','  + @InsertNValue + ', NULL, NULL, GETDATE() ) ; ';				

				END
			IF @DataType = 'T' --NOT TESTED
				BEGIN
					SET @InsertAnyTypeValue =  ISNULL(@InsertAnyTypeValue,'NULL')
					SET @InsertTValue = CAST(@InsertAnyTypeValue As nvarchar(MAX));
					SET @SqlInsert = N'insert into output.ProcessData([SubmissionID],[DataKey], [UnitID], [StudyMethodology] ,[NumberValue],[TextValue],[DateVal], [CalcTime] ) ' 
								+ 'Values (' + CAST(@SubmissionId AS nvarchar(20))  + ', ''' + @DataKey + ''', ' + @UnitID + ','
								+ @Methodology + ','''  + @InsertTValue + ''', NULL, NULL, GETDATE() ) ; ';				
				END
			IF @DataType = 'D'	 --NOT TESTED
				BEGIN	
					SET @InsertAnyTypeValue =  ISNULL(@InsertAnyTypeValue,'NULL')
					SET @InsertDValue = CAST(@InsertAnyTypeValue As date);
					SET @SqlInsert = N'insert into output.ProcessData([SubmissionID],[DataKey], [UnitID], [StudyMethodology] ,[NumberValue],[TextValue],[DateVal], [CalcTime] ) ' 
								+ 'Values (' + CAST(@SubmissionId AS nvarchar(20))  + ', ''' + @DataKey + ''', ' + @UnitID + ','
								+ @Methodology + ','''  +  CAST(@InsertDValue AS nvarchar(30)) + ''', NULL, NULL, GETDATE() ) ; ';				
				END	
			
			--BEGIN TRY  --With only 48 char to write to dbo.Logging, and each must be unique, GUID to make text unique would take up too
						--many chars to be worthwhile. So just throw error back to calling proc.
				EXECUTE sp_executesql @SqlInsert;
			--END TRY
			--BEGIN CATCH
			--	INSERT INTO  [GlobalDB].[dbo].[Logging]([CrudAction]) VALUES(@SqlInsert);
			--	PRINT @SqlCommand;
			--	BREAK;
			--END CATCH;
			FETCH NEXT FROM SqlCursor INTO @DataType, @DataKey, @SqlCommand;
		END; --END WHILE
		CLOSE SqlCursor;
		DEALLOCATE SqlCursor;

	END
RETURN
GO


