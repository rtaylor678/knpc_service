﻿



CREATE    PROC [dbo].[spReportChevronKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
IF @RefineryID = '179EUR'
	RETURN 1

DECLARE 
	@BU_UtilPcnt real, @BU_KEDC real, @BU_KUEDC real, 
	@BU_MechAvail real, @BU_KMAEDC real, @BU_KProcEDC real,
	@BU_EII real, @BU_KEnergyUseDay real, @BU_KTotStdEnergy real,
	@BU_AdjMaintIndex real, @BU_TAIndex_Avg real, @BU_RoutIndex real,   
	@BU_TotEqPEDC real, 
	@BU_NEOpexUEDC real, @BU_TotCashOpexUEDC real,
	@BU_CashMargin real, @BU_NetInputBPD real
DECLARE 
	@CT_UtilPcnt real, @CT_KEDC real, @CT_KUEDC real, 
	@CT_MechAvail real, @CT_KMAEDC real, @CT_KProcEDC real,
	@CT_EII real, @CT_KEnergyUseDay real, @CT_KTotStdEnergy real,
	@CT_AdjMaintIndex real, @CT_TAIndex_Avg real, @CT_RoutIndex real,   
	@CT_TotEqPEDC real, 
	@CT_NEOpexUEDC real, @CT_TotCashOpexUEDC real,
	@CT_CashMargin real, @CT_NetInputBPD real
DECLARE 
	@ES_UtilPcnt real, @ES_KEDC real, @ES_KUEDC real, 
	@ES_MechAvail real, @ES_KMAEDC real, @ES_KProcEDC real,
	@ES_EII real, @ES_KEnergyUseDay real, @ES_KTotStdEnergy real,
	@ES_AdjMaintIndex real, @ES_TAIndex_Avg real, @ES_RoutIndex real,   
	@ES_TotEqPEDC real, 
	@ES_NEOpexUEDC real, @ES_TotCashOpexUEDC real,
	@ES_CashMargin real, @ES_NetInputBPD real
DECLARE 
	@HI_UtilPcnt real, @HI_KEDC real, @HI_KUEDC real, 
	@HI_MechAvail real, @HI_KMAEDC real, @HI_KProcEDC real,
	@HI_EII real, @HI_KEnergyUseDay real, @HI_KTotStdEnergy real,
	@HI_AdjMaintIndex real, @HI_TAIndex_Avg real, @HI_RoutIndex real,   
	@HI_TotEqPEDC real, 
	@HI_NEOpexUEDC real, @HI_TotCashOpexUEDC real,
	@HI_CashMargin real, @HI_NetInputBPD real
DECLARE 
	@PA_UtilPcnt real, @PA_KEDC real, @PA_KUEDC real, 
	@PA_MechAvail real, @PA_KMAEDC real, @PA_KProcEDC real,
	@PA_EII real, @PA_KEnergyUseDay real, @PA_KTotStdEnergy real,
	@PA_AdjMaintIndex real, @PA_TAIndex_Avg real, @PA_RoutIndex real,   
	@PA_TotEqPEDC real, 
	@PA_NEOpexUEDC real, @PA_TotCashOpexUEDC real,
	@PA_CashMargin real, @PA_NetInputBPD real
DECLARE 
	@RI_UtilPcnt real, @RI_KEDC real, @RI_KUEDC real, 
	@RI_MechAvail real, @RI_KMAEDC real, @RI_KProcEDC real,
	@RI_EII real, @RI_KEnergyUseDay real, @RI_KTotStdEnergy real,
	@RI_AdjMaintIndex real, @RI_TAIndex_Avg real, @RI_RoutIndex real,   
	@RI_TotEqPEDC real, 
	@RI_NEOpexUEDC real, @RI_TotCashOpexUEDC real,
	@RI_CashMargin real, @RI_NetInputBPD real
DECLARE 
	@SL_UtilPcnt real, @SL_KEDC real, @SL_KUEDC real, 
	@SL_MechAvail real, @SL_KMAEDC real, @SL_KProcEDC real,
	@SL_EII real, @SL_KEnergyUseDay real, @SL_KTotStdEnergy real,
	@SL_AdjMaintIndex real, @SL_TAIndex_Avg real, @SL_RoutIndex real,   
	@SL_TotEqPEDC real, 
	@SL_NEOpexUEDC real, @SL_TotCashOpexUEDC real,
	@SL_CashMargin real, @SL_NetInputBPD real

DECLARE 
	@CHEVRON_UtilPcnt real, @CHEVRON_KEDC real, @CHEVRON_KUEDC real, 
	@CHEVRON_MechAvail real, @CHEVRON_KMAEDC real, @CHEVRON_KProcEDC real,
	@CHEVRON_EII real, @CHEVRON_KEnergyUseDay real, @CHEVRON_KTotStdEnergy real,
	@CHEVRON_AdjMaintIndex real, @CHEVRON_TAIndex_Avg real, @CHEVRON_RoutIndex real,   
	@CHEVRON_TotEqPEDC real, 
	@CHEVRON_NEOpexUEDC real, @CHEVRON_TotCashOpexUEDC real,
	@CHEVRON_CashMargin real, @CHEVRON_NetInputBPD real

EXEC [dbo].[spReportChevronKPI_REF] '162NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@BU_UtilPcnt OUTPUT, @KEDC=@BU_KEDC OUTPUT, @KUEDC=@BU_KUEDC OUTPUT, 
	@MechAvail=@BU_MechAvail OUTPUT, @KMAEDC=@BU_KMAEDC OUTPUT, @KProcEDC=@BU_KProcEDC OUTPUT,
	@EII=@BU_EII OUTPUT, @KEnergyUseDay=@BU_KEnergyUseDay OUTPUT, @KTotStdEnergy=@BU_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@BU_AdjMaintIndex OUTPUT, @TAIndex_Avg=@BU_TAIndex_Avg OUTPUT, @RoutIndex=@BU_RoutIndex OUTPUT,   
	@TotEqPEDC=@BU_TotEqPEDC OUTPUT, 
	@NEOpexUEDC=@BU_NEOpexUEDC OUTPUT, @TotCashOpexUEDC=@BU_TotCashOpexUEDC OUTPUT,
	@CashMargin=@BU_CashMargin OUTPUT, @NetInputBPD=@BU_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '53PAC', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@CT_UtilPcnt OUTPUT, @KEDC=@CT_KEDC OUTPUT, @KUEDC=@CT_KUEDC OUTPUT, 
	@MechAvail=@CT_MechAvail OUTPUT, @KMAEDC=@CT_KMAEDC OUTPUT, @KProcEDC=@CT_KProcEDC OUTPUT,
	@EII=@CT_EII OUTPUT, @KEnergyUseDay=@CT_KEnergyUseDay OUTPUT, @KTotStdEnergy=@CT_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@CT_AdjMaintIndex OUTPUT, @TAIndex_Avg=@CT_TAIndex_Avg OUTPUT, @RoutIndex=@CT_RoutIndex OUTPUT,   
	@TotEqPEDC=@CT_TotEqPEDC OUTPUT, 
	@NEOpexUEDC=@CT_NEOpexUEDC OUTPUT, @TotCashOpexUEDC=@CT_TotCashOpexUEDC OUTPUT,
	@CashMargin=@CT_CashMargin OUTPUT, @NetInputBPD=@CT_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '15NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@ES_UtilPcnt OUTPUT, @KEDC=@ES_KEDC OUTPUT, @KUEDC=@ES_KUEDC OUTPUT, 
	@MechAvail=@ES_MechAvail OUTPUT, @KMAEDC=@ES_KMAEDC OUTPUT, @KProcEDC=@ES_KProcEDC OUTPUT,
	@EII=@ES_EII OUTPUT, @KEnergyUseDay=@ES_KEnergyUseDay OUTPUT, @KTotStdEnergy=@ES_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@ES_AdjMaintIndex OUTPUT, @TAIndex_Avg=@ES_TAIndex_Avg OUTPUT, @RoutIndex=@ES_RoutIndex OUTPUT,   
	@TotEqPEDC=@ES_TotEqPEDC OUTPUT, 
	@NEOpexUEDC=@ES_NEOpexUEDC OUTPUT, @TotCashOpexUEDC=@ES_TotCashOpexUEDC OUTPUT,
	@CashMargin=@ES_CashMargin OUTPUT, @NetInputBPD=@ES_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '16NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@HI_UtilPcnt OUTPUT, @KEDC=@HI_KEDC OUTPUT, @KUEDC=@HI_KUEDC OUTPUT, 
	@MechAvail=@HI_MechAvail OUTPUT, @KMAEDC=@HI_KMAEDC OUTPUT, @KProcEDC=@HI_KProcEDC OUTPUT,
	@EII=@HI_EII OUTPUT, @KEnergyUseDay=@HI_KEnergyUseDay OUTPUT, @KTotStdEnergy=@HI_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@HI_AdjMaintIndex OUTPUT, @TAIndex_Avg=@HI_TAIndex_Avg OUTPUT, @RoutIndex=@HI_RoutIndex OUTPUT,   
	@TotEqPEDC=@HI_TotEqPEDC OUTPUT, 
	@NEOpexUEDC=@HI_NEOpexUEDC OUTPUT, @TotCashOpexUEDC=@HI_TotCashOpexUEDC OUTPUT,
	@CashMargin=@HI_CashMargin OUTPUT, @NetInputBPD=@HI_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '17NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@PA_UtilPcnt OUTPUT, @KEDC=@PA_KEDC OUTPUT, @KUEDC=@PA_KUEDC OUTPUT, 
	@MechAvail=@PA_MechAvail OUTPUT, @KMAEDC=@PA_KMAEDC OUTPUT, @KProcEDC=@PA_KProcEDC OUTPUT,
	@EII=@PA_EII OUTPUT, @KEnergyUseDay=@PA_KEnergyUseDay OUTPUT, @KTotStdEnergy=@PA_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@PA_AdjMaintIndex OUTPUT, @TAIndex_Avg=@PA_TAIndex_Avg OUTPUT, @RoutIndex=@PA_RoutIndex OUTPUT,   
	@TotEqPEDC=@PA_TotEqPEDC OUTPUT, 
	@NEOpexUEDC=@PA_NEOpexUEDC OUTPUT, @TotCashOpexUEDC=@PA_TotCashOpexUEDC OUTPUT,
	@CashMargin=@PA_CashMargin OUTPUT, @NetInputBPD=@PA_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '79FL', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@RI_UtilPcnt OUTPUT, @KEDC=@RI_KEDC OUTPUT, @KUEDC=@RI_KUEDC OUTPUT, 
	@MechAvail=@RI_MechAvail OUTPUT, @KMAEDC=@RI_KMAEDC OUTPUT, @KProcEDC=@RI_KProcEDC OUTPUT,
	@EII=@RI_EII OUTPUT, @KEnergyUseDay=@RI_KEnergyUseDay OUTPUT, @KTotStdEnergy=@RI_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@RI_AdjMaintIndex OUTPUT, @TAIndex_Avg=@RI_TAIndex_Avg OUTPUT, @RoutIndex=@RI_RoutIndex OUTPUT,   
	@TotEqPEDC=@RI_TotEqPEDC OUTPUT, 
	@NEOpexUEDC=@RI_NEOpexUEDC OUTPUT, @TotCashOpexUEDC=@RI_TotCashOpexUEDC OUTPUT,
	@CashMargin=@RI_CashMargin OUTPUT, @NetInputBPD=@RI_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '21NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@SL_UtilPcnt OUTPUT, @KEDC=@SL_KEDC OUTPUT, @KUEDC=@SL_KUEDC OUTPUT, 
	@MechAvail=@SL_MechAvail OUTPUT, @KMAEDC=@SL_KMAEDC OUTPUT, @KProcEDC=@SL_KProcEDC OUTPUT,
	@EII=@SL_EII OUTPUT, @KEnergyUseDay=@SL_KEnergyUseDay OUTPUT, @KTotStdEnergy=@SL_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@SL_AdjMaintIndex OUTPUT, @TAIndex_Avg=@SL_TAIndex_Avg OUTPUT, @RoutIndex=@SL_RoutIndex OUTPUT,   
	@TotEqPEDC=@SL_TotEqPEDC OUTPUT, 
	@NEOpexUEDC=@SL_NEOpexUEDC OUTPUT, @TotCashOpexUEDC=@SL_TotCashOpexUEDC OUTPUT,
	@CashMargin=@SL_CashMargin OUTPUT, @NetInputBPD=@SL_NetInputBPD OUTPUT


SELECT	@CHEVRON_KEDC = ISNULL(@BU_KEDC,0) + ISNULL(@CT_KEDC,0) + ISNULL(@ES_KEDC,0) + ISNULL(@HI_KEDC,0) 
					  + ISNULL(@PA_KEDC,0) + ISNULL(@RI_KEDC,0) + ISNULL(@SL_KEDC,0),
		@CHEVRON_KUEDC = ISNULL(@BU_KUEDC,0) + ISNULL(@CT_KUEDC,0) + ISNULL(@ES_KUEDC,0) + ISNULL(@HI_KUEDC,0) 
					   + ISNULL(@PA_KUEDC,0) + ISNULL(@RI_KUEDC,0) + ISNULL(@SL_KUEDC,0),
		@CHEVRON_KMAEDC = ISNULL(@BU_KMAEDC,0) + ISNULL(@CT_KMAEDC,0) + ISNULL(@ES_KMAEDC,0) + ISNULL(@HI_KMAEDC,0) 
					    + ISNULL(@PA_KMAEDC,0) + ISNULL(@RI_KMAEDC,0) + ISNULL(@SL_KMAEDC,0),
		@CHEVRON_KProcEDC = ISNULL(@BU_KProcEDC,0) + ISNULL(@CT_KProcEDC,0) + ISNULL(@ES_KProcEDC,0) + ISNULL(@HI_KProcEDC,0) 
							 + ISNULL(@PA_KProcEDC,0) + ISNULL(@RI_KProcEDC,0) + ISNULL(@SL_KProcEDC,0),
		@CHEVRON_NetInputBPD = ISNULL(@BU_NetInputBPD,0) + ISNULL(@CT_NetInputBPD,0) + ISNULL(@ES_NetInputBPD,0) + ISNULL(@HI_NetInputBPD,0) 
							 + ISNULL(@PA_NetInputBPD,0) + ISNULL(@RI_NetInputBPD,0) + ISNULL(@SL_NetInputBPD,0),
		@CHEVRON_KEnergyUseDay = ISNULL(@BU_KEnergyUseDay,0) + ISNULL(@CT_KEnergyUseDay,0) + ISNULL(@ES_KEnergyUseDay,0) + ISNULL(@HI_KEnergyUseDay,0) 
							   + ISNULL(@PA_KEnergyUseDay,0) + ISNULL(@RI_KEnergyUseDay,0) + ISNULL(@SL_KEnergyUseDay,0),
		@CHEVRON_KTotStdEnergy = ISNULL(@BU_KTotStdEnergy,0) + ISNULL(@CT_KTotStdEnergy,0) + ISNULL(@ES_KTotStdEnergy,0) + ISNULL(@HI_KTotStdEnergy,0) 
							   + ISNULL(@PA_KTotStdEnergy,0) + ISNULL(@RI_KTotStdEnergy,0) + ISNULL(@SL_KTotStdEnergy,0)

IF @CHEVRON_KEDC > 0
	SELECT	@CHEVRON_UtilPcnt = 100.0 * @CHEVRON_KUEDC / @CHEVRON_KEDC,
			@CHEVRON_AdjMaintIndex = (ISNULL(@BU_KEDC,0)*ISNULL(@BU_AdjMaintIndex,0) + ISNULL(@CT_KEDC,0)*ISNULL(@CT_AdjMaintIndex,0) + ISNULL(@ES_KEDC,0)*ISNULL(@ES_AdjMaintIndex,0) + ISNULL(@HI_KEDC,0)*ISNULL(@HI_AdjMaintIndex,0) 
			   						+ ISNULL(@PA_KEDC,0)*ISNULL(@PA_AdjMaintIndex,0) + ISNULL(@RI_KEDC,0)*ISNULL(@RI_AdjMaintIndex,0) + ISNULL(@SL_KEDC,0)*ISNULL(@SL_AdjMaintIndex,0))/@CHEVRON_KEDC,
			@CHEVRON_TAIndex_Avg = (ISNULL(@BU_KEDC,0)*ISNULL(@BU_TAIndex_Avg,0) + ISNULL(@CT_KEDC,0)*ISNULL(@CT_TAIndex_Avg,0) + ISNULL(@ES_KEDC,0)*ISNULL(@ES_TAIndex_Avg,0) + ISNULL(@HI_KEDC,0)*ISNULL(@HI_TAIndex_Avg,0) 
								  + ISNULL(@PA_KEDC,0)*ISNULL(@PA_TAIndex_Avg,0) + ISNULL(@RI_KEDC,0)*ISNULL(@RI_TAIndex_Avg,0) + ISNULL(@SL_KEDC,0)*ISNULL(@SL_TAIndex_Avg,0))/@CHEVRON_KEDC,
			@CHEVRON_RoutIndex = (ISNULL(@BU_KEDC,0)*ISNULL(@BU_RoutIndex,0) + ISNULL(@CT_KEDC,0)*ISNULL(@CT_RoutIndex,0) + ISNULL(@ES_KEDC,0)*ISNULL(@ES_RoutIndex,0) + ISNULL(@HI_KEDC,0)*ISNULL(@HI_RoutIndex,0) 
								+ ISNULL(@PA_KEDC,0)*ISNULL(@PA_RoutIndex,0) + ISNULL(@RI_KEDC,0)*ISNULL(@RI_RoutIndex,0) + ISNULL(@SL_KEDC,0)*ISNULL(@SL_RoutIndex,0))/@CHEVRON_KEDC,
			@CHEVRON_TotEqPEDC = (ISNULL(@BU_KEDC,0)*ISNULL(@BU_TotEqPEDC,0) + ISNULL(@CT_KEDC,0)*ISNULL(@CT_TotEqPEDC,0) + ISNULL(@ES_KEDC,0)*ISNULL(@ES_TotEqPEDC,0) + ISNULL(@HI_KEDC,0)*ISNULL(@HI_TotEqPEDC,0) 
								+ ISNULL(@PA_KEDC,0)*ISNULL(@PA_TotEqPEDC,0) + ISNULL(@RI_KEDC,0)*ISNULL(@RI_TotEqPEDC,0) + ISNULL(@SL_KEDC,0)*ISNULL(@SL_TotEqPEDC,0))/@CHEVRON_KEDC

IF @CHEVRON_KUEDC > 0
	SELECT	@CHEVRON_NEOpexUEDC = (ISNULL(@BU_KUEDC,0)*ISNULL(@BU_NEOpexUEDC,0) + ISNULL(@CT_KUEDC,0)*ISNULL(@CT_NEOpexUEDC,0) + ISNULL(@ES_KUEDC,0)*ISNULL(@ES_NEOpexUEDC,0) + ISNULL(@HI_KUEDC,0)*ISNULL(@HI_NEOpexUEDC,0) 
								 + ISNULL(@PA_KUEDC,0)*ISNULL(@PA_NEOpexUEDC,0) + ISNULL(@RI_KUEDC,0)*ISNULL(@RI_NEOpexUEDC,0) + ISNULL(@SL_KUEDC,0)*ISNULL(@SL_NEOpexUEDC,0))/@CHEVRON_KUEDC,
			@CHEVRON_TotCashOpexUEDC = (ISNULL(@BU_KUEDC,0)*ISNULL(@BU_TotCashOpexUEDC,0) + ISNULL(@CT_KUEDC,0)*ISNULL(@CT_TotCashOpexUEDC,0) + ISNULL(@ES_KUEDC,0)*ISNULL(@ES_TotCashOpexUEDC,0) + ISNULL(@HI_KUEDC,0)*ISNULL(@HI_TotCashOpexUEDC,0) 
									  + ISNULL(@PA_KUEDC,0)*ISNULL(@PA_TotCashOpexUEDC,0) + ISNULL(@RI_KUEDC,0)*ISNULL(@RI_TotCashOpexUEDC,0) + ISNULL(@SL_KUEDC,0)*ISNULL(@SL_TotCashOpexUEDC,0))/@CHEVRON_KUEDC

IF @CHEVRON_KProcEDC > 0
	SELECT	@CHEVRON_MechAvail = 100.0 * @CHEVRON_KMAEDC / @CHEVRON_KProcEDC

IF @CHEVRON_KTotStdEnergy > 0
	SELECT	@CHEVRON_EII = 100.0 * @CHEVRON_KEnergyUseDay / @CHEVRON_KTotStdEnergy

IF @CHEVRON_NetInputBPD > 0
	SELECT	@CHEVRON_CashMargin = (ISNULL(@BU_NetInputBPD,0)*ISNULL(@BU_CashMargin,0) + ISNULL(@CT_NetInputBPD,0)*ISNULL(@CT_CashMargin,0) + ISNULL(@ES_NetInputBPD,0)*ISNULL(@ES_CashMargin,0) + ISNULL(@HI_NetInputBPD,0)*ISNULL(@HI_CashMargin,0) 
									  + ISNULL(@PA_NetInputBPD,0)*ISNULL(@PA_CashMargin,0) + ISNULL(@RI_NetInputBPD,0)*ISNULL(@RI_CashMargin,0) + ISNULL(@SL_NetInputBPD,0)*ISNULL(@SL_CashMargin,0))/@CHEVRON_NetInputBPD

SET NOCOUNT OFF

SELECT  ReportPeriod = CAST(CAST(@PeriodMonth as varchar(2)) + '/1/' + CAST(@PeriodYear as varchar(4)) as smalldatetime),
	BU_UtilPcnt=@BU_UtilPcnt, BU_KEDC=@BU_KEDC, BU_KUEDC=@BU_KUEDC, 
	BU_MechAvail=@BU_MechAvail, BU_KMAEDC=@BU_KMAEDC, BU_KProcEDC=@BU_KProcEDC,
	BU_EII=@BU_EII, BU_KEnergyUseDay=@BU_KEnergyUseDay, BU_KTotStdEnergy=@BU_KTotStdEnergy,
	BU_AdjMaintIndex=@BU_AdjMaintIndex, BU_TAIndex_Avg=@BU_TAIndex_Avg, BU_RoutIndex=@BU_RoutIndex,   
	BU_TotEqPEDC=@BU_TotEqPEDC, 
	BU_NEOpexUEDC=@BU_NEOpexUEDC, BU_TotCashOpexUEDC=@BU_TotCashOpexUEDC,
	BU_CashMargin=@BU_CashMargin, BU_NetInputBPD=@BU_NetInputBPD,

	CT_UtilPcnt=@CT_UtilPcnt, CT_KEDC=@CT_KEDC, CT_KUEDC=@CT_KUEDC, 
	CT_MechAvail=@CT_MechAvail, CT_KMAEDC=@CT_KMAEDC, CT_KProcEDC=@CT_KProcEDC,
	CT_EII=@CT_EII, CT_KEnergyUseDay=@CT_KEnergyUseDay, CT_KTotStdEnergy=@CT_KTotStdEnergy,
	CT_AdjMaintIndex=@CT_AdjMaintIndex, CT_TAIndex_Avg=@CT_TAIndex_Avg, CT_RoutIndex=@CT_RoutIndex,   
	CT_TotEqPEDC=@CT_TotEqPEDC, 
	CT_NEOpexUEDC=@CT_NEOpexUEDC, CT_TotCashOpexUEDC=@CT_TotCashOpexUEDC,
	CT_CashMargin=@CT_CashMargin, CT_NetInputBPD=@CT_NetInputBPD,

	ES_UtilPcnt=@ES_UtilPcnt, ES_KEDC=@ES_KEDC, ES_KUEDC=@ES_KUEDC, 
	ES_MechAvail=@ES_MechAvail, ES_KMAEDC=@ES_KMAEDC, ES_KProcEDC=@ES_KProcEDC,
	ES_EII=@ES_EII, ES_KEnergyUseDay=@ES_KEnergyUseDay, ES_KTotStdEnergy=@ES_KTotStdEnergy,
	ES_AdjMaintIndex=@ES_AdjMaintIndex, ES_TAIndex_Avg=@ES_TAIndex_Avg, ES_RoutIndex=@ES_RoutIndex,   
	ES_TotEqPEDC=@ES_TotEqPEDC, 
	ES_NEOpexUEDC=@ES_NEOpexUEDC, ES_TotCashOpexUEDC=@ES_TotCashOpexUEDC,
	ES_CashMargin=@ES_CashMargin, ES_NetInputBPD=@ES_NetInputBPD,

	HI_UtilPcnt=@HI_UtilPcnt, HI_KEDC=@HI_KEDC, HI_KUEDC=@HI_KUEDC, 
	HI_MechAvail=@HI_MechAvail, HI_KMAEDC=@HI_KMAEDC, HI_KProcEDC=@HI_KProcEDC,
	HI_EII=@HI_EII, HI_KEnergyUseDay=@HI_KEnergyUseDay, HI_KTotStdEnergy=@HI_KTotStdEnergy,
	HI_AdjMaintIndex=@HI_AdjMaintIndex, HI_TAIndex_Avg=@HI_TAIndex_Avg, HI_RoutIndex=@HI_RoutIndex,   
	HI_TotEqPEDC=@HI_TotEqPEDC, 
	HI_NEOpexUEDC=@HI_NEOpexUEDC, HI_TotCashOpexUEDC=@HI_TotCashOpexUEDC,
	HI_CashMargin=@HI_CashMargin, HI_NetInputBPD=@HI_NetInputBPD,

	PA_UtilPcnt=@PA_UtilPcnt, PA_KEDC=@PA_KEDC, PA_KUEDC=@PA_KUEDC, 
	PA_MechAvail=@PA_MechAvail, PA_KMAEDC=@PA_KMAEDC, PA_KProcEDC=@PA_KProcEDC,
	PA_EII=@PA_EII, PA_KEnergyUseDay=@PA_KEnergyUseDay, PA_KTotStdEnergy=@PA_KTotStdEnergy,
	PA_AdjMaintIndex=@PA_AdjMaintIndex, PA_TAIndex_Avg=@PA_TAIndex_Avg, PA_RoutIndex=@PA_RoutIndex,   
	PA_TotEqPEDC=@PA_TotEqPEDC, 
	PA_NEOpexUEDC=@PA_NEOpexUEDC, PA_TotCashOpexUEDC=@PA_TotCashOpexUEDC,
	PA_CashMargin=@PA_CashMargin, PA_NetInputBPD=@PA_NetInputBPD,

	RI_UtilPcnt=@RI_UtilPcnt, RI_KEDC=@RI_KEDC, RI_KUEDC=@RI_KUEDC, 
	RI_MechAvail=@RI_MechAvail, RI_KMAEDC=@RI_KMAEDC, RI_KProcEDC=@RI_KProcEDC,
	RI_EII=@RI_EII, RI_KEnergyUseDay=@RI_KEnergyUseDay, RI_KTotStdEnergy=@RI_KTotStdEnergy,
	RI_AdjMaintIndex=@RI_AdjMaintIndex, RI_TAIndex_Avg=@RI_TAIndex_Avg, RI_RoutIndex=@RI_RoutIndex,   
	RI_TotEqPEDC=@RI_TotEqPEDC, 
	RI_NEOpexUEDC=@RI_NEOpexUEDC, RI_TotCashOpexUEDC=@RI_TotCashOpexUEDC,
	RI_CashMargin=@RI_CashMargin, RI_NetInputBPD=@RI_NetInputBPD,

	SL_UtilPcnt=@SL_UtilPcnt, SL_KEDC=@SL_KEDC, SL_KUEDC=@SL_KUEDC, 
	SL_MechAvail=@SL_MechAvail, SL_KMAEDC=@SL_KMAEDC, SL_KProcEDC=@SL_KProcEDC,
	SL_EII=@SL_EII, SL_KEnergyUseDay=@SL_KEnergyUseDay, SL_KTotStdEnergy=@SL_KTotStdEnergy,
	SL_AdjMaintIndex=@SL_AdjMaintIndex, SL_TAIndex_Avg=@SL_TAIndex_Avg, SL_RoutIndex=@SL_RoutIndex,   
	SL_TotEqPEDC=@SL_TotEqPEDC, 
	SL_NEOpexUEDC=@SL_NEOpexUEDC, SL_TotCashOpexUEDC=@SL_TotCashOpexUEDC,
	SL_CashMargin=@SL_CashMargin, SL_NetInputBPD=@SL_NetInputBPD,

	CHEVRON_UtilPcnt=@CHEVRON_UtilPcnt, CHEVRON_KEDC=@CHEVRON_KEDC, CHEVRON_KUEDC=@CHEVRON_KUEDC, 
	CHEVRON_MechAvail=@CHEVRON_MechAvail, CHEVRON_KMAEDC=@CHEVRON_KMAEDC, CHEVRON_KProcEDC=@CHEVRON_KProcEDC,
	CHEVRON_EII=@CHEVRON_EII, CHEVRON_KEnergyUseDay=@CHEVRON_KEnergyUseDay, CHEVRON_KTotStdEnergy=@CHEVRON_KTotStdEnergy,
	CHEVRON_AdjMaintIndex=@CHEVRON_AdjMaintIndex, CHEVRON_TAIndex_Avg=@CHEVRON_TAIndex_Avg, CHEVRON_RoutIndex=@CHEVRON_RoutIndex,   
	CHEVRON_TotEqPEDC=@CHEVRON_TotEqPEDC, 
	CHEVRON_NEOpexUEDC=@CHEVRON_NEOpexUEDC, CHEVRON_TotCashOpexUEDC=@CHEVRON_TotCashOpexUEDC,
	CHEVRON_CashMargin=@CHEVRON_CashMargin, CHEVRON_NetInputBPD=@CHEVRON_NetInputBPD



