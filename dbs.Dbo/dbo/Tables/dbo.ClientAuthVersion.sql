﻿CREATE TABLE [dbo].[ClientAuthVersion]
(
	[ClientAuthVersionId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ApplicationId] INT NOT NULL, 
    [ClientRefNum] NVARCHAR(50) NOT NULL, 
    [Active] BIT NOT NULL DEFAULT 1, 
    [CreateDate] DATETIME NOT NULL
)
