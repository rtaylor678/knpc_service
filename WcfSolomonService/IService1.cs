﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace KNPCService
{

    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string CheckService();

        [OperationContract]
        string ValidateDatabaseConnection();

        [OperationContract]
        List<string> SendKNPCDataTEST(string table, byte[] xmlstringBytes);

        [OperationContract]
        byte[] GetSubmission(byte[] clientSubmissionIDBytes, byte[] clientKeyBytes, out string statusMessage);

        [OperationContract]
        string SubmitData(byte[] SubmissionBytes, byte[] clientKeyBytes);

        [OperationContract]
        byte[] GetReference(byte[] clientKeyBytes, out string statusMessage);

        [OperationContract]
        byte[] GetKPI(byte[] clientSubmissionIDBytes, byte[] clientKeyBytes, out string statusMessage);

        [OperationContract]
        byte[] GetStatus();
    }

}
