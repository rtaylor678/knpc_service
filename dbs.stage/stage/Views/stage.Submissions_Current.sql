﻿CREATE VIEW [stage].[Submissions_Current]
WITH SCHEMABINDING
AS
SELECT
	[s].[SubmissionID],
	[s].[ClientSubmissionID],
	[s].[PeriodBeg],
	[s].[PeriodEnd],
	[s].[PeriodDuration_Days],
	[s].[RptCurrency],
	[s].[UOM],
	[s].[RefineryID],
	[s].[Notes]
FROM
	[stage].[Submissions]	[s]
WHERE
	[s].[SubmissionID] = (
		SELECT TOP 1
			[i].[SubmissionID]
		FROM
			[stage].[Submissions]	[i]
		WHERE
			[i].[ClientSubmissionID] = [s].[ClientSubmissionID]
		ORDER BY
			[i].[tsInserted]	DESC
		);