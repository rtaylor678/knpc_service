﻿CREATE proc [stage].[ClearSubmission]
	@SubmissionID int,
	@Table nvarchar(40) = null
AS
	DECLARE @SQL nvarchar(max)
	IF @Table = 'ALL'
	BEGIN

		DELETE FROM [stage].[Submissions] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[Absence] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[Config] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[ConfigRS] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[ConfigBuoy] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[Crude] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[Diesel] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[Emission] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[Energy] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[FiredHeaters] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[Gasoline] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[GeneralMisc] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[Inventory] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[Kerosene] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[LPG] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[MaintRout] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[MaintTA] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[MarineBunker] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[MExp] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[MiscInput] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[OpEx] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[Personnel] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[ProcessData] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[Resid] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[RPFResid] WHERE [SubmissionID] = @SubmissionID;
		DELETE FROM [stage].[Steam] WHERE [SubmissionID] = @SubmissionID;

		DELETE FROM [stage].[Yield] WHERE [SubmissionID] = @SubmissionID;
		
	END
	else
	BEGIN
	IF @Table is null
		DELETE FROM [stage].[Submissions] where [SubmissionID] = @SubmissionID;
	ELSE
	BEGIN
		SET @SQL = 'DELETE FROM [stage].[' + @Table + '] WHERE [[SubmissionID]] = ' + CONVERT(varchar, @SubmissionID) + ';'
		EXECUTE sp_executesql @SQL
	END
	END
