﻿CREATE TABLE [stage].[Kerosene] (
    [KeroseneID]   INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [BlendID]      INT           NULL,
    [Grade]        NVARCHAR (20) NULL,
    [Type]         NVARCHAR (50) NULL,
    [Density]      REAL          NULL,
    [Sulfur]       REAL          NULL,
    [KMT]          REAL          NULL,
    CONSTRAINT [PK_Kerosene] PRIMARY KEY CLUSTERED ([KeroseneID] ASC),
    CONSTRAINT [FK_Kerosene_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Kerosene] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [BlendID] ASC)
);

