﻿CREATE TABLE [stage].[RPFResid] (
    [RPFResidID]   INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [Density]      REAL          NULL,
    [Sulfur]       REAL          NULL,
    [ViscCSAtTemp] REAL          NULL,
    [ViscTemp]     REAL          NULL,
    [EnergyType]   NVARCHAR (3) NULL,
    CONSTRAINT [PK_RPFResid] PRIMARY KEY CLUSTERED ([RPFResidID] ASC),
    CONSTRAINT [FK_RPFResid_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
 CONSTRAINT [UK_RPFResid] UNIQUE NONCLUSTERED 
(
	[SubmissionID] ASC,
	[EnergyType] ASC)
);

