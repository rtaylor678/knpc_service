﻿CREATE TABLE [stage].[Crude] (
    [CrudeTableID] INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [CrudeID]      INT           NULL,
    [CrudeName]    NVARCHAR (50) NULL,
    [BBL]          REAL          NULL,
    [Gravity]      REAL          NULL,
    [Sulfur]       REAL          NULL,
    [CNum]         NVARCHAR (20) NULL,
    [Period]       NVARCHAR (20) NULL,
    CONSTRAINT [PK_Crude] PRIMARY KEY CLUSTERED ([CrudeTableID] ASC),
    CONSTRAINT [FK_Crude_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Crude] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [CrudeID] ASC)
);

