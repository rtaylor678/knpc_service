﻿CREATE TABLE [stage].[Emission] (
    [EmissionID]   INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [EmissionType] NVARCHAR (5) NULL,
    [RefEmissions] REAL          NULL,
    CONSTRAINT [PK_Emission] PRIMARY KEY CLUSTERED ([EmissionID] ASC),
    CONSTRAINT [FK_Emission_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Emission] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [EmissionType] ASC)
);

