﻿CREATE TABLE [stage].[Config] (
    [ConfigID]       INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID]   INT           NOT NULL,
    [UnitID]         INT           NULL,
    [ProcessID]      NVARCHAR (50) NULL,
    [UnitName]       NVARCHAR (40) NULL,
    [ProcessType]    NVARCHAR (50) NULL,
    [Cap]            REAL          NULL,
    [UtilPcnt]       REAL          NULL,
    [StmCap]         REAL          NULL,
    [StmUtilPcnt]    REAL          NULL,
    [InServicePcnt]  REAL          NULL,
    [YearsOper]      REAL          NULL,
    [MHPerWeek]      REAL          NULL,
    [PostPerShift]   REAL          NULL,
    [BlockOp]        VARCHAR (6)   NULL,
    [ControlType]    CHAR (4)      NULL,
    [EnergyPcnt]     REAL          NULL,
    [AllocPcntOfCap] REAL          NULL,
    CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED ([ConfigID] ASC),
    CONSTRAINT [FK_Config_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Config] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [UnitID] ASC)
);

