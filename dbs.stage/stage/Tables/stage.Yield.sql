﻿CREATE TABLE [stage].[Yield] (
    [YieldID]      INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID] INT           NOT NULL,
    [MaterialID]   NVARCHAR (5) NULL,
    [Category]     NVARCHAR (5) NULL,
    [MaterialName] NVARCHAR (50) NULL,
    [Period]       NVARCHAR (20) NULL,
    [BBL]          REAL          NULL,
    [Density]      REAL          NULL,
    [MT]           REAL          NULL,
    [UOM]          NVARCHAR (20) NULL,
    CONSTRAINT [PK_Yield] PRIMARY KEY CLUSTERED ([YieldID] ASC),
    CONSTRAINT [FK_Yield_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID])
	--, CONSTRAINT [UK_Yield] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [MaterialID] ASC, [MaterialName] ASC, [Category] ASC, [Period] ASC)
);

