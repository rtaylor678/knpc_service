﻿CREATE TABLE [stage].[Submissions] (
    [SubmissionID]        INT                IDENTITY (1, 1) NOT NULL,
    [ClientSubmissionID]  INT                NOT NULL,
    [PeriodBeg]           DATETIME2 (7)      NOT NULL,
    [PeriodEnd]           DATETIME2 (7)      NULL,
    [PeriodDuration_Days] AS                 (datediff(day,[PeriodBeg],[PeriodEnd])) PERSISTED,
    [RptCurrency]         NVARCHAR (20)      NULL,
    [UOM]                 NVARCHAR (5)      NULL,
    [RefineryID]          NVARCHAR (20)      NULL,
    [Notes]               NVARCHAR (MAX)     NULL,
    [tsInserted]          DATETIMEOFFSET (7) CONSTRAINT [DF_Submissions_tsInserted] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsInsertedHost]      NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsInsertedHost] DEFAULT (host_name()) NOT NULL,
    [tsInsertedUser]      NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsInsertedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsInsertedApp]       NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsInsertedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Submissions] PRIMARY KEY NONCLUSTERED ([SubmissionID] ASC),
    CONSTRAINT [UK_Submissions] UNIQUE CLUSTERED ([ClientSubmissionID] ASC, [tsInserted] DESC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Submissions]
    ON [stage].[Submissions]([ClientSubmissionID] ASC, [tsInserted] DESC)
    INCLUDE([SubmissionID]);

