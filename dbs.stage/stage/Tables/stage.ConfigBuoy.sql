﻿CREATE TABLE [stage].[ConfigBuoy] (
    [ConfigBuoyID]  INT           IDENTITY (1, 1) NOT NULL,
    [SubmissionID]  INT           NOT NULL,
    [ProcessID]     NVARCHAR (50) NULL,
    [LineSize]      REAL          NULL,
    [PcntOwnership] REAL          NULL,
    [ShipCap]       REAL          NULL,
    [UnitID]        INT           NULL,
    [UnitName]      NVARCHAR (50) NULL,
    [ProcessType]   NVARCHAR (50) NULL,
    CONSTRAINT [PK_ConfigBuoy] PRIMARY KEY CLUSTERED ([ConfigBuoyID] ASC),
    CONSTRAINT [FK_ConfigBuoy_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_ConfigBuoy] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [ProcessID] ASC)
);

