﻿CREATE TABLE [stage].[Diesel] (
    [DieselID]      INT      IDENTITY (1, 1) NOT NULL,
    [SubmissionID]  INT      NOT NULL,
    [BlendID]       INT      NULL,
    [Grade]         CHAR (4) NULL,
    [Market]        CHAR (4) NULL,
    [Type]          CHAR (5) NULL,
    [Density]       REAL     NULL,
    [Cetane]        REAL     NULL,
    [PourPt]        REAL     NULL,
    [Sulfur]        REAL     NULL,
    [CloudPt]       REAL     NULL,
    [ASTM90]        REAL     NULL,
    [E350]          REAL     NULL,
    [BiodieselPcnt] REAL     NULL,
    [KMT]           REAL     NULL,
    CONSTRAINT [PK_Diesel] PRIMARY KEY CLUSTERED ([DieselID] ASC),
    CONSTRAINT [FK_Diesel_Submissions] FOREIGN KEY ([SubmissionID]) REFERENCES [stage].[Submissions] ([SubmissionID]),
    CONSTRAINT [UK_Diesel] UNIQUE NONCLUSTERED ([SubmissionID] ASC, [BlendID] ASC)
);

