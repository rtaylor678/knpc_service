﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Reflection;
using staging = Sa.dbs.Stage;
using output = Sa.dbs.Output;
using dim = Sa.dbs.Dim;
using System.Linq;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using WcfServiceLocal = Sa.Pearl.Client.KnpcLocal;
using WcfServiceProd = Sa.Pearl.Client.ServiceReference1;
using System.Reflection;

namespace Sa.Pearl
{
    [Serializable]
    public class ClientSubmission : staging.Submission
    {
        #region private members

        string appMode = string.Empty;

        #endregion

        #region constructors

        public ClientSubmission()
        {
        }

        public ClientSubmission(string applicationMode = "")
        {
            if(applicationMode.Length>0)
            appMode = applicationMode;
        }

        #endregion


        #region public
        //we change Submissions by adding the partial class here which contains the code for the functions we want to add


        /// <summary>Given a path to an XML file it will load the XML file into the Submission object.</summary>
        public string CheckService()
        {

            string response = string.Empty;

            switch (appMode)
            {
                case "Local":
                    WcfServiceLocal.Service1Client scLocal = new WcfServiceLocal.Service1Client();
                    response = scLocal.CheckService();
                    break;
                case "Dev":
                    break;
                case "QA":
                    break;
                case "Prod":
                    WcfServiceProd.Service1Client scProd = new WcfServiceProd.Service1Client();
                    response = scProd.CheckService();
                    break;
                default:
                    WcfServiceProd.Service1Client scProdDefault = new WcfServiceProd.Service1Client();
                    response = scProdDefault.CheckService();
                    break;
            }


            return response;
        }

        /// <summary>Given a path to an XML file it will load the XML file into the Submission object.</summary>
        public string CheckDatabase()
        {
            string response = string.Empty;

            switch (appMode)
            {
                case "Local":
                    WcfServiceLocal.Service1Client scLocal = new WcfServiceLocal.Service1Client();
                    response = Convert.ToString(scLocal.ValidateDatabaseConnection());
                    break;
                case "Dev":
                    break;
                case "QA":
                    break;
                case "Prod":
                    WcfServiceProd.Service1Client scProd = new WcfServiceProd.Service1Client();
                    response = Convert.ToString(scProd.ValidateDatabaseConnection());
                    break;
                default:
                    WcfServiceProd.Service1Client scProdDefault = new WcfServiceProd.Service1Client();
                    response = Convert.ToString(scProdDefault.ValidateDatabaseConnection());
                    break;
            } 

            return response;
        }

        /// <summary>Given a path to an XML file it will load the XML file into the Submission object.</summary>
        public List<String> LoadSubmissionFromXML(String filepath)
        {
            List<String> errors = new List<String>();
            //test();
            //return errors;

            errors = GetXMLFile(filepath);

            return errors;
        }

        /// <summary>Given a path to an XML file it will save the Submission object to that location (and will automatically overwrite any file there).</summary>
        public List<String> SaveSubmissionToXML(String filepath)
        {
            List<String> errors = new List<String>();
            Sa.Pearl.SerializeSubmission serializer = new SerializeSubmission();
            errors = serializer.WriteToXml(filepath, this, true);
            //errors = MakeXMLFile(filepath);

            return errors;
        }

        /// <summary>Loads the data in this Submission object to the web service. Returns a list of errors, or an empty list if no errors occurred.</summary>
        public List<String> SubmitData(String ClientKey)
        {
            //send the data to the web service
            List<String> errors = new List<String>();

            //TODO: do i need to verify that there is data loaded first? or are we sending a blank submission?
            //two fields must be populated or we do not send data
            //client submission id
            //period beginning

            //first error check: ClientKey must not be blank
            if (ClientKey.Trim() == "")
            {
                errors.Add("ClientKey must not be blank");
                return errors;
            }

            errors = SendDataToService(ClientKey);

            return errors;
        }

        /// <summary>Gets the data with the chosen Submission ID and Client Key and puts it into this Submission object. Returns a list of errors, or an empty list if no errors occurred.</summary>
        public List<String> RetrieveSubmission(String SubmissionID, String ClientKey)
        {
            //return a specific submission from the database
            //calls to the web service, gets a submission back, loads it into the class object for the client to read
            List<String> errors = new List<String>();

            //first error check: ClientKey and SubmissionID must not be blank
            if (ClientKey.Trim() == "")
            {
                errors.Add("ClientKey must not be blank");
            }

            if (SubmissionID.Trim() == "")
            {
                errors.Add("SubmissionID must not be blank");
            }

            if (errors.Count != 0)
            {
                return errors;
            }

            errors = GetSubmissionFromService(SubmissionID, ClientKey);
            return errors;

        }

        /// <summary>Gets the status of the latest submission for this ClientKey.</summary>
        public List<String> GetStatus(String SubmissionId, String ClientKey)
        {
            //TODO: returns a status from the database, loads it into a class object for the client to read
            //what do we do with it?
            List<String> errors = new List<String>();


            //first error check: ClientKey must not be blank
            if (ClientKey.Trim() == "")
            {
                errors.Add("ClientKey must not be blank");
            }

            if (errors.Count != 0)
            {
                return errors;
            }



            errors.Add("This function has not yet been implemented.");

            return errors;
        }

        /// <summary>Clear out everything in this Submission object. Returns a list of errors, or an empty list if no errors occurred.</summary>
        public List<String> FlushSubmission()
        {
            List<String> result = ClearCurrentSubmission();

            return result;

        }

        #endregion


        #region private

        private List<String> GetXMLFile(String filepath)
        {
            List<String> errors = new List<String>();
            //TODO: should we wipe the submission before loading? here we are adding to it
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(filepath);

                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    #if DEBUG
                        Console.WriteLine(node.InnerText);
                    #endif

                    //PeriodDuration_Days is a computed column, can't add it to db.
                    //we don't need PeriodEnd; the end date is calculated.) 
                    //if (node.FirstChild != null && node.Name != "PeriodDuration_Days" && node.Name != "PeriodEnd") 

                    if (node.FirstChild != null && node.Name == "PeriodEnd")
                    {
                        if (node.InnerText != null && node.InnerText.Length > 0)
                        {
                            PropertyInfo pi = this.GetType().GetProperty("PeriodEnd");
                            //convert from string to nullable
                            DateTime temp2 =Convert.ToDateTime(node.InnerText);
                            DateTime? temp3 = new DateTime?(temp2);
                            pi.SetValue(this, temp3, null);
                        }
                    }
                    else if (node.FirstChild != null && node.Name == "PeriodDuration_Days")
                    {
                        if (node.InnerText != null && node.InnerText.Length > 0)
                        {
                            PropertyInfo pi = this.GetType().GetProperty("PeriodDuration_Days");
                            //convert from string to nullable
                            Int32 temp2 = Convert.ToInt32(node.InnerText);
                            Int32? temp3 = new Int32?(temp2);
                            pi.SetValue(this, temp3, null);
                        }
                    }
                    else if (node.FirstChild != null) // && node.Name != "PeriodDuration_Days") 
                    {
                        //if null it has not data so we don't care about populating it
                        //PeriodDuration_Days is a computed column, can't add it to db.
                        switch (node.FirstChild.NodeType)
                        {
                            case XmlNodeType.Text://if the child is text then it is a property, we put the value into this

                                PropertyInfo propertyInfo = this.GetType().GetProperty(node.Name);
                                #if DEBUG
                                    Console.WriteLine(node.Name);
                                #endif
                                
                                try
                                {                                    
                                    propertyInfo.SetValue(this, Convert.ChangeType(node.InnerText, propertyInfo.PropertyType), null);
                                }
                                catch (System.NullReferenceException)
                                {
                                    if (node.Name == "Date")//converting a Date to a PeriodBeg, this is very not sustainable, should we change the field in the database?
                                    {
                                        PropertyInfo pi = this.GetType().GetProperty("PeriodBeg");
                                        pi.SetValue(this, Convert.ChangeType(node.InnerText, pi.PropertyType), null);
                                    }
                                    else
                                    {
                                        errors.Add("Error: Unrecognized XML file tag: " + node.Name);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //TODO: not sure what to do with any errors we get here other than write them to the error log
                                    errors.Add(ex.Message.ToString());
                                }

                                break;

                            case XmlNodeType.Element://this should be an object, we create an object and attach to this

                                Type type = Type.GetType("Sa.Pearl." + node.Name);
                                object o = Activator.CreateInstance(type);
                                foreach (XmlNode cn in node.ChildNodes)
                                {
                                    PropertyInfo pI = o.GetType().GetProperty(cn.Name);
                                    #if DEBUG
                                        Console.WriteLine(cn.Name);
                                    #endif
                                    try
                                    {
                                        pI.SetValue(o, Convert.ChangeType(cn.InnerText, pI.PropertyType), null);
                                    }
                                    catch (System.InvalidCastException)//if it is Nullable it gives an error so we have to go a step further to get the type conversion
                                    {
                                        if (cn.InnerText.Length > 0)
                                            if (cn.InnerText.Trim().ToUpper() != "NULL")
                                            {
                                                pI.SetValue(o, Convert.ChangeType(cn.InnerText, Nullable.GetUnderlyingType(pI.PropertyType)), null);
                                            }
                                            else
                                            {
                                                pI.SetValue(o,null, null);
                                            }                                            
                                    }
                                }
                                AddObject(o);
                                break;

                            default:
                                //should we do something here?
                                break;
                        }
                    }

                    
                }

                //and check if they filled in the SubmissionID but not ClientSubmissionID (which is probable)
                if (this.ClientSubmissionID == 0 && this.SubmissionID > 0)
                {
                    this.ClientSubmissionID = this.SubmissionID;
                }
            }

            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }
            //if (this.OpExes.Count < 1)
            //{
            //    //per GLC, this will be fatal error in calcs
            //    errors.Add("No OpEx information was found. Please include OpEx data and try again");
            //}
            //if (this.ProcessDatas.Count < 1)
            //{
            //    //per GLC, this will be fatal error in calcs
            //    errors.Add("No ProcessData information was found. Please include ProcessData data and try again");
            //}
            return errors;

        }

        private void AddObject<T>(T objectToAdd)
        {
            //the generic T objectToAdd cannot be cast to e.g an Absence, so we first have to cast the T to an object, then the object to the Absence. Clunky but true. Do it better if you can.
            object newobj = (T)(object)objectToAdd;

            Type type = typeof(T);
            string objectType = objectToAdd.GetType().ToString();
            //string typeString = type.ToString();
            switch (objectType.ToUpper().Trim())
            {
                case ("SA.PEARL.ABSENCE"):
                    staging.Absence a = (staging.Absence)newobj;
                    this.Absences.Add(a);
                    break;

                case ("SA.PEARL.CONFIG"):
                    staging.Config c = (staging.Config)newobj;
                    this.Configs.Add(c);
                    break;

                case ("SA.PEARL.CONFIGBUOY"):
                    staging.ConfigBuoy cb = (staging.ConfigBuoy)newobj;
                    this.ConfigBuoys.Add(cb);
                    break;

                case ("SA.PEARL.CONFIGRS"):
                    staging.ConfigR cr = (staging.ConfigR)newobj;
                    this.ConfigRS.Add(cr);
                    break;

                case ("SA.PEARL.CRUDE"):
                    staging.Crude cru = (staging.Crude)newobj;
                    this.Crudes.Add(cru);
                    break;

                case ("SA.PEARL.DIESEL"):
                    staging.Diesel d = (staging.Diesel)newobj;
                    this.Diesels.Add(d);
                    break;

                case ("SA.PEARL.EMISSION"):
                    staging.Emission e = (staging.Emission)newobj;
                    this.Emissions.Add(e);
                    break;

                case ("SA.PEARL.ENERGY"):
                    staging.Energy en = (staging.Energy)newobj;
                    this.Energies.Add(en);
                    break;

                case ("SA.PEARL.FIREDHEATER"):
                    staging.FiredHeater fh = (staging.FiredHeater)newobj;
                    this.FiredHeaters.Add(fh);
                    break;

                case ("SA.PEARL.GASOLINE"):
                    staging.Gasoline g = (staging.Gasoline)newobj;
                    this.Gasolines.Add(g);
                    break;

                case ("SA.PEARL.GENERALMISC"):
                    staging.GeneralMisc gm = (staging.GeneralMisc)newobj;
                    this.GeneralMiscs.Add(gm);
                    break;

                case ("SA.PEARL.INVENTORY"):
                    staging.Inventory i = (staging.Inventory)newobj;
                    this.Inventories.Add(i);
                    break;

                case ("SA.PEARL.KEROSENE"):
                    staging.Kerosene k = (staging.Kerosene)newobj;
                    this.Kerosenes.Add(k);
                    break;

                case ("SA.PEARL.LPG"):
                    staging.LPG l = (staging.LPG)newobj;
                    this.LPGs.Add(l);
                    break;

                case ("SA.PEARL.MAINTROUT"):
                    staging.MaintRout mr = (staging.MaintRout)newobj;
                    this.MaintRouts.Add(mr);
                    break;

                case ("SA.PEARL.MAINTTA"):
                    staging.MaintTA mt = (staging.MaintTA)newobj;
                    this.MaintTAs.Add(mt);
                    break;

                case ("SA.PEARL.MARINEBUNKER"):
                    staging.MarineBunker mb = (staging.MarineBunker)newobj;
                    this.MarineBunkers.Add(mb);
                    break;

                case ("SA.PEARL.MEXP"):
                    staging.MExp me = (staging.MExp)newobj;
                    this.MExps.Add(me);
                    break;

                case ("SA.PEARL.MISCINPUT"):
                    staging.MiscInput mi = (staging.MiscInput)newobj;
                    this.MiscInputs.Add(mi);
                    break;

                case ("SA.PEARL.OPEX"):
                    staging.OpEx o = (staging.OpEx)newobj;
                    this.OpExes.Add(o);
                    break;

                case ("SA.PEARL.PERSONNEL"):
                    staging.Personnel p = (staging.Personnel)newobj;
                    this.Personnels.Add(p);
                    break;

                case ("SA.PEARL.PROCESSDATA"):
                    staging.ProcessData pd = (staging.ProcessData)newobj;
                    this.ProcessDatas.Add(pd);
                    break;

                case ("SA.PEARL.RESID"):
                    staging.Resid r = (staging.Resid)newobj;
                    this.Resids.Add(r);
                    break;

                case ("SA.PEARL.RPFRESID"):
                    staging.RPFResid rp = (staging.RPFResid)newobj;
                    this.RPFResids.Add(rp);
                    break;

                case ("SA.PEARL.STEAM"):
                    staging.Steam s = (staging.Steam)newobj;
                    this.Steams.Add(s);
                    break;

                case ("SA.PEARL.YIELD"):
                    staging.Yield y = (staging.Yield)newobj;
                    this.Yields.Add(y);
                    break;

                default:
                    throw new Exception("Object not matched: " + objectType);
                    break;
            }




        }
        /*
        private List<String> MakeXMLFile(String filepath)
        {

            List<String> errors = new List<String>();

            try
            {
                XmlWriterSettings setting = new XmlWriterSettings();
                setting.ConformanceLevel = ConformanceLevel.Auto;

                using (XmlWriter writer = XmlWriter.Create(filepath, setting))
                {
                    writer.WriteStartElement("Submission");//start of the file

                    //write the elements that belong just to the Submission (have to write these separately)
                    foreach (PropertyInfo propertyInfo in this.GetType().GetProperties().Where(propertyInfo => propertyInfo.GetGetMethod().GetParameters().Count() == 0))
                    {

                        if (!propertyInfo.PropertyType.ToString().Contains("ICollection"))//holy cow this was hard to figure out how to do. there has to be an easier way to know if it is a collection or not
                        {

                            switch (propertyInfo.Name.ToString())
                            {
                                case "SubmissionID":
                                case "RefineryID":
                                case "UOM":
                                case "RptCurrency":
                                    writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(this, null) ?? string.Empty).ToString());
                                    break;
                                case "PeriodBeg"://because it needs to be written as Date
                                    writer.WriteElementString("Date", (propertyInfo.GetValue(this, null) ?? string.Empty).ToString());
                                    break;
                                default://any other field doesn't get written to the XML
                                    break;
                            }
                        }
                    }

                    //then go through each table and add them one at a time (got to be a better way to do this, right?)
                    foreach (staging.Absence a in this.Absences) { XMLStuff.WriteXML(a, "Absence", writer); }
                    foreach (staging.Comment c in this.Comments) { XMLStuff.WriteXML(c, "Comment", writer); }
                    foreach (staging.Config c in this.Configs) { XMLStuff.WriteXML(c, "Config", writer); }
                    foreach (staging.ConfigBuoy a in this.ConfigBuoys) { XMLStuff.WriteXML(a, "ConfigBuoy", writer); }
                    foreach (staging.ConfigR a in this.ConfigRS) { XMLStuff.WriteXML(a, "ConfigRS", writer); }
                    foreach (staging.Crude a in this.Crudes) { XMLStuff.WriteXML(a, "Crude", writer); }
                    foreach (staging.Diesel a in this.Diesels) { XMLStuff.WriteXML(a, "Diesel", writer); }
                    foreach (staging.Emission a in this.Emissions) { XMLStuff.WriteXML(a, "Emission", writer); }
                    foreach (staging.Energy a in this.Energies) { XMLStuff.WriteXML(a, "Energy", writer); }
                    foreach (staging.FinProdProp a in this.FinProdProps) { XMLStuff.WriteXML(a, "FinProdProp", writer); }
                    foreach (staging.FiredHeater a in this.FiredHeaters) { XMLStuff.WriteXML(a, "FiredHeater", writer); }
                    foreach (staging.Gasoline g in this.Gasolines) { XMLStuff.WriteXML(g, "Gasoline", writer); }
                    foreach (staging.GeneralMisc a in this.GeneralMiscs) { XMLStuff.WriteXML(a, "GeneralMisc", writer); }
                    foreach (staging.Inventory a in this.Inventories) { XMLStuff.WriteXML(a, "Inventory", writer); }
                    foreach (staging.Kerosene a in this.Kerosenes) { XMLStuff.WriteXML(a, "Kerosene", writer); }
                    foreach (staging.LPG a in this.LPGs) { XMLStuff.WriteXML(a, "LPG", writer); }
                    foreach (staging.MaintRout a in this.MaintRouts) { XMLStuff.WriteXML(a, "MaintRout", writer); }
                    foreach (staging.MaintTA a in this.MaintTAs) { XMLStuff.WriteXML(a, "MaintTA", writer); }
                    foreach (staging.MarineBunker a in this.MarineBunkers) { XMLStuff.WriteXML(a, "MarineBunker", writer); }
                    foreach (staging.MExp a in this.MExps) { XMLStuff.WriteXML(a, "MExp", writer); }
                    foreach (staging.MiscInput a in this.MiscInputs) { XMLStuff.WriteXML(a, "MiscInput", writer); }
                    foreach (staging.OpEx a in this.OpExes) { XMLStuff.WriteXML(a, "OpEx", writer); }
                    foreach (staging.Personnel a in this.Personnels) { XMLStuff.WriteXML(a, "Personnel", writer); }
                    foreach (staging.ProcessData a in this.ProcessDatas) { XMLStuff.WriteXML(a, "ProcessData", writer); }
                    foreach (staging.Resid r in this.Resids) { XMLStuff.WriteXML(r, "Resid", writer); }
                    foreach (staging.RPFResid a in this.RPFResids) { XMLStuff.WriteXML(a, "RPFResid", writer); }
                    foreach (staging.Steam a in this.Steams) { XMLStuff.WriteXML(a, "Steam", writer); }
                    foreach (staging.Yield a in this.Yields) { XMLStuff.WriteXML(a, "Yield", writer); }

                    //and write the end of the file
                    writer.WriteEndElement();
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;
        }
        */
        private List<String> SendDataToService(String ClientKey)
        {
            List<String> errors = new List<String>();
            string ret = string.Empty;

            try
            {                
                staging.Submission submissionToSend = PrepSubmissionToSend(ref errors);

                byte[] reader = Sa.Serialization.ToByteArray(submissionToSend);
                byte[] encrypted = Sa.Crypto.Encrypt(reader);//, eKey, sKey, Vector);

                //encrypt the ClientKey
                byte[] clientKeyReader = Sa.Serialization.ToByteArray(ClientKey);
                byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);

                List<Array> errorarray = new List<Array>();

                switch (appMode)
                {
                    case "Local":
                        WcfServiceLocal.Service1Client scLocal = new WcfServiceLocal.Service1Client();
                        ret = scLocal.SubmitData(encrypted, encryptedClientKey);
                        break;
                    case "Dev":
                        break;
                    case "QA":
                        break;
                    case "Prod":
                        WcfServiceProd.Service1Client scProd = new WcfServiceProd.Service1Client();
                        ret = scProd.SubmitData(encrypted, encryptedClientKey);
                        break;
                    default:
                        WcfServiceProd.Service1Client scProdDefault = new WcfServiceProd.Service1Client();
                        ret = scProdDefault.SubmitData(encrypted, encryptedClientKey);
                        break;
                }

                if (ret.Length > 0)
                {
                    errors.Add(ret);
                }
            }

            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;

        }

        private  Sa.dbs.Stage.Submission PrepSubmissionToSend(ref List<string> errors)
        {
            staging.Submission submissionToSend = new staging.Submission();
            try
            {                
                //this is terrible code but it works, needs to be updated to variable or to a generic
                foreach (staging.Absence a in this.Absences)
                {
                    staging.Absence c = new staging.Absence();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Absences.Add(c);
                }

                foreach (staging.Config a in this.Configs)
                {
                    staging.Config c = new staging.Config();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Configs.Add(c);
                }

                foreach (staging.ConfigBuoy a in this.ConfigBuoys)
                {
                    staging.ConfigBuoy c = new staging.ConfigBuoy();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.ConfigBuoys.Add(c);
                }

                foreach (staging.ConfigR a in this.ConfigRS)
                {
                    staging.ConfigR c = new staging.ConfigR();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.ConfigRS.Add(c);
                }

                foreach (staging.Crude a in this.Crudes)
                {
                    staging.Crude c = new staging.Crude();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Crudes.Add(c);
                }

                foreach (staging.Diesel a in this.Diesels)
                {
                    staging.Diesel c = new staging.Diesel();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Diesels.Add(c);
                }

                foreach (staging.Emission a in this.Emissions)
                {
                    staging.Emission c = new staging.Emission();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    c.EmissionType = c.EmissionType.Trim();
                    submissionToSend.Emissions.Add(c);
                }

                foreach (staging.Energy a in this.Energies)
                {
                    staging.Energy c = new staging.Energy();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Energies.Add(c);
                }

                foreach (staging.FiredHeater a in this.FiredHeaters)
                {
                    staging.FiredHeater c = new staging.FiredHeater();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.FiredHeaters.Add(c);
                }

                foreach (staging.Gasoline a in this.Gasolines)
                {
                    staging.Gasoline c = new staging.Gasoline();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Gasolines.Add(c);
                }

                foreach (staging.GeneralMisc a in this.GeneralMiscs)
                {
                    staging.GeneralMisc c = new staging.GeneralMisc();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.GeneralMiscs.Add(c);
                }

                foreach (staging.Inventory a in this.Inventories)
                {
                    staging.Inventory c = new staging.Inventory();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Inventories.Add(c);
                }

                foreach (staging.Kerosene a in this.Kerosenes)
                {
                    staging.Kerosene c = new staging.Kerosene();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Kerosenes.Add(c);
                }

                foreach (staging.LPG a in this.LPGs)
                {
                    staging.LPG c = new staging.LPG();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.LPGs.Add(c);
                }

                foreach (staging.MaintRout a in this.MaintRouts)
                {
                    staging.MaintRout c = new staging.MaintRout();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    c.ProcessID = c.ProcessID.Trim();
                    submissionToSend.MaintRouts.Add(c);
                }

                foreach (staging.MaintTA a in this.MaintTAs)
                {
                    staging.MaintTA c = new staging.MaintTA();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    c.ProcessID = c.ProcessID.Trim();
                    submissionToSend.MaintTAs.Add(c);
                }

                foreach (staging.MarineBunker a in this.MarineBunkers)
                {
                    staging.MarineBunker c = new staging.MarineBunker();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.MarineBunkers.Add(c);
                }

                foreach (staging.MExp a in this.MExps)
                {
                    staging.MExp c = new staging.MExp();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.MExps.Add(c);
                }

                foreach (staging.MiscInput a in this.MiscInputs)
                {
                    staging.MiscInput c = new staging.MiscInput();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.MiscInputs.Add(c);
                }

                foreach (staging.OpEx a in this.OpExes)
                {
                    staging.OpEx c = new staging.OpEx();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.OpExes.Add(c);
                }

                foreach (staging.Personnel a in this.Personnels)
                {
                    staging.Personnel c = new staging.Personnel();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Personnels.Add(c);
                }

                foreach (staging.ProcessData a in this.ProcessDatas)
                {
                    staging.ProcessData c = new staging.ProcessData();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.ProcessDatas.Add(c);
                }

                foreach (staging.Resid a in this.Resids)
                {
                    staging.Resid c = new staging.Resid();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Resids.Add(c);
                }

                foreach (staging.RPFResid a in this.RPFResids)
                {
                    staging.RPFResid c = new staging.RPFResid();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    c.EnergyType = c.EnergyType.Trim();
                    submissionToSend.RPFResids.Add(c);
                }

                foreach (staging.Steam a in this.Steams)
                {
                    staging.Steam c = new staging.Steam();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Steams.Add(c);
                }

                foreach (staging.Yield a in this.Yields)
                {
                    staging.Yield c = new staging.Yield();

                    foreach (PropertyInfo pi in c.GetType().GetProperties())
                    {
                        pi.SetValue(c, pi.GetValue(a, null));
                    }
                    c.SubmissionID = submissionToSend.SubmissionID;
                    c.Submission = submissionToSend;
                    submissionToSend.Yields.Add(c);
                }

                submissionToSend.SubmissionID = this.SubmissionID;
                submissionToSend.ClientSubmissionID = this.ClientSubmissionID;
                submissionToSend.PeriodBeg = this.PeriodBeg;
                submissionToSend.PeriodEnd = this.PeriodEnd;
                submissionToSend.PeriodDuration_Days = this.PeriodDuration_Days;
                submissionToSend.RptCurrency = this.RptCurrency;
                submissionToSend.UOM = this.UOM;
                submissionToSend.RefineryID = this.RefineryID;
                submissionToSend.Notes = this.Notes;
                            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }
            return submissionToSend;
        }


        private List<String> GetSubmissionFromService(String submissionID, String ClientKey)
        {

            List<String> errors = new List<String>();
            byte[] output = null;

            try
            {

                //convert the supplied submission ID string to int then encrypt it
                int intSubmissionID = Convert.ToInt32(submissionID);
                byte[] submissionIDReader = Sa.Serialization.ToByteArray(intSubmissionID);
                byte[] encryptedSubmissionID = Sa.Crypto.Encrypt(submissionIDReader);//, eKey, Vector);

                byte[] clientKeyReader = Sa.Serialization.ToByteArray(ClientKey);
                byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);

                //submit to the web service
                String returnerror = "";

                switch (appMode)
                {
                    case "Local":
                        WcfServiceLocal.Service1Client scLocal = new WcfServiceLocal.Service1Client();
                        output = scLocal.GetSubmission(encryptedSubmissionID, encryptedClientKey, out returnerror);
                        break;
                    case "Dev":
                        break;
                    case "QA":
                        break;
                    case "Prod":
                        WcfServiceProd.Service1Client scProd = new WcfServiceProd.Service1Client();
                        output = scProd.GetSubmission(encryptedSubmissionID, encryptedClientKey, out returnerror);
                        break;
                    default:
                        WcfServiceProd.Service1Client scProdDefault = new WcfServiceProd.Service1Client();
                        output = scProdDefault.GetSubmission(encryptedSubmissionID, encryptedClientKey, out returnerror);
                        break;
                }

                if (returnerror.Length > 0)
                {
                    errors.Add(returnerror);
                    return errors;
                }

                //decrypt the result and put it into a new submission object
                byte[] decryptedSubmission = Sa.Crypto.Decrypt(output);//, eKey, Vector);
                staging.Submission returnedSubmission = new staging.Submission();
                returnedSubmission = Sa.Serialization.ToObject<staging.Submission>(decryptedSubmission);

                //move all the items into the existing object
                this.Absences = returnedSubmission.Absences;
                this.Configs = returnedSubmission.Configs;
                this.ConfigBuoys = returnedSubmission.ConfigBuoys;
                this.ConfigRS = returnedSubmission.ConfigRS;
                this.Crudes = returnedSubmission.Crudes;
                this.Diesels = returnedSubmission.Diesels;
                this.Emissions = returnedSubmission.Emissions;
                this.Energies = returnedSubmission.Energies;
                this.FiredHeaters = returnedSubmission.FiredHeaters;
                this.Gasolines = returnedSubmission.Gasolines;
                this.GeneralMiscs = returnedSubmission.GeneralMiscs;
                this.Inventories = returnedSubmission.Inventories;
                this.Kerosenes = returnedSubmission.Kerosenes;
                this.LPGs = returnedSubmission.LPGs;
                this.MaintRouts = returnedSubmission.MaintRouts;
                this.MaintTAs = returnedSubmission.MaintTAs;
                this.MarineBunkers = returnedSubmission.MarineBunkers;
                this.MExps = returnedSubmission.MExps;
                this.MiscInputs = returnedSubmission.MiscInputs;
                this.OpExes = returnedSubmission.OpExes;
                this.Personnels = returnedSubmission.Personnels;
                this.ProcessDatas = returnedSubmission.ProcessDatas;
                this.Resids = returnedSubmission.Resids;
                this.RPFResids = returnedSubmission.RPFResids;
                this.Steams = returnedSubmission.Steams;
                this.Yields = returnedSubmission.Yields;

                //reset SubmissionIDs to the ClientSubmissionID
                foreach (staging.Absence a in this.Absences) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Config a in this.Configs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.ConfigBuoy a in this.ConfigBuoys) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.ConfigR a in this.ConfigRS) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Crude a in this.Crudes) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Diesel a in this.Diesels) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Emission a in this.Emissions) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Energy a in this.Energies) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.FiredHeater a in this.FiredHeaters) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Gasoline a in this.Gasolines) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.GeneralMisc a in this.GeneralMiscs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Inventory a in this.Inventories) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Kerosene a in this.Kerosenes) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.LPG a in this.LPGs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.MaintRout a in this.MaintRouts) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.MaintTA a in this.MaintTAs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.MarineBunker a in this.MarineBunkers) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.MExp a in this.MExps) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.MiscInput a in this.MiscInputs) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.OpEx a in this.OpExes) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Personnel a in this.Personnels) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.ProcessData a in this.ProcessDatas) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Resid a in this.Resids) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.RPFResid a in this.RPFResids) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Steam a in this.Steams) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }
                foreach (staging.Yield a in this.Yields) { a.SubmissionID = returnedSubmission.ClientSubmissionID; }



                this.SubmissionID = returnedSubmission.SubmissionID;
                this.ClientSubmissionID = returnedSubmission.ClientSubmissionID;
                this.PeriodBeg = returnedSubmission.PeriodBeg;
                this.PeriodEnd = returnedSubmission.PeriodEnd;
                this.PeriodDuration_Days = returnedSubmission.PeriodDuration_Days;
                this.RptCurrency = returnedSubmission.RptCurrency;
                this.UOM = returnedSubmission.UOM;
                this.RefineryID = returnedSubmission.RefineryID;
                this.Notes = returnedSubmission.Notes;

            }

            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;

        }

        private List<String> GetStatusFromService(String ClientKey)
        {
            List<String> errors = new List<String>();
            errors.Add("error test, not yet implemented");
            return errors;
        }

        private List<String> ClearCurrentSubmission()
        {
            List<String> errors = new List<String>();

            try
            {
                //first clear all the tables
                this.Absences.Clear();
                this.Configs.Clear();
                this.ConfigBuoys.Clear();
                this.ConfigRS.Clear();
                this.Crudes.Clear();
                this.Diesels.Clear();
                this.Emissions.Clear();
                this.Energies.Clear();
                this.FiredHeaters.Clear();
                this.Gasolines.Clear();
                this.GeneralMiscs.Clear();
                this.Inventories.Clear();
                this.Kerosenes.Clear();
                this.LPGs.Clear();
                this.MaintRouts.Clear();
                this.MaintTAs.Clear();
                this.MarineBunkers.Clear();
                this.MExps.Clear();
                this.MiscInputs.Clear();
                this.OpExes.Clear();
                this.Personnels.Clear();
                this.ProcessDatas.Clear();
                this.Resids.Clear();
                this.RPFResids.Clear();
                this.Steams.Clear();
                this.Yields.Clear();

                //then the individual variables in a submission
                //some of these are nullable, others are not so I'm setting them to 0 or blank
                this.SubmissionID = 0;
                this.ClientSubmissionID = 0;
                this.PeriodBeg = System.DateTime.Now; //not sure what this should be
                this.PeriodEnd = null;
                this.PeriodDuration_Days = null;
                this.RptCurrency = "";
                this.UOM = "";
                this.RefineryID = "";
                this.Notes = "";
                //this.tsInserted = System.DateTimeOffset.Now; //not sure what this should be
                //this.tsInsertedHost = "";
                //this.tsInsertedUser = "";
                //this.tsInsertedApp = "";

            }

            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;
        }


        private void test()
        {

            var props = this.GetType().GetProperties();

            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (pi.PropertyType.Name.Contains("ICollection"))//.GetInterface(typeof(System.Collections.Generic.ICollection<>).FullName) != null)
                {
                    //pi.SetValue(c, pi.GetValue(a, null));
                    //Type type = pi.GetType();
                    //if (type.IsClass == true)
                    //	{
                    System.Diagnostics.Debug.Write(pi.Name);
                    //}
                }
            }
        }

        #endregion

    }

    public partial class ClientKPIs : output.ShortOutput
    {
        #region private members

        string appMode = string.Empty;
        
        #endregion

        #region constructors

        public ClientKPIs()
        {
            
        }

       
        //for Embedded Assembly
        static byte[] StreamToBytes(System.IO.Stream input)
        {
            var capacity = input.CanSeek ? (int)input.Length : 0;
            using (var output = new System.IO.MemoryStream(capacity))
            {
                int readLength;
                var buffer = new byte[4096];

                do
                {
                    readLength = input.Read(buffer, 0, buffer.Length);
                    output.Write(buffer, 0, readLength);
                }
                while (readLength != 0);

                return output.ToArray();
            }
        }


        public ClientKPIs(string applicationMode="")
        {
            if (applicationMode.Length > 0)
            appMode = applicationMode;
        }
        #endregion

        #region public

        /// <summary>Gets the KPI data with the chosen Submission ID and Client Key and puts it into this KPI object. Returns a list of errors, or an empty list if no errors occurred.</summary>
        public List<String> RetrieveKPIs(String SubmissionID, String ClientKey)
        {
            //return a set of KPIs from the database, loads it into a class object for the client to read

            List<String> errors = new List<String>();

            //first error check: ClientKey and SubmissionID must not be blank
            if (ClientKey.Trim() == "")
            {
                errors.Add("ClientKey must not be blank");
            }

            if (SubmissionID.Trim() == "")
            {
                errors.Add("SubmissionID must not be blank");
            }

            if (errors.Count != 0)
            {
                return errors;
            }
            else
            {
                errors = FlushKPIs();
                if (errors.Count != 0)
                {
                    return errors;
                }
            }
                       
            

            //try and submit the data
            try
            {
                errors = GetKPIsFromService(SubmissionID, ClientKey);
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;

        }

        /// <summary>Clear out everything in this KPI object. Returns a list of errors, or an empty list if no errors occurred.</summary>
        public List<String> FlushKPIs()
        {
            List<String> errors = ClearCurrentKPIs();

            return errors;
        }

        /// <summary>Given a path to an XML file it will save the KPI object to that location (and will automatically overwrite any file there).</summary>
        public List<String> SaveKPIsToXML(String filepath)
        {
            List<String> errors = new List<String>();
            //will write the public partial class ClientKPIs  class to file
            errors = SaveKPIDataToXML(filepath);

            return errors;
        }

        #endregion

        #region private

        private List<String> GetKPIsFromService(String submissionID, String ClientKey)
        {
            List<String> errors = new List<String>();

            try
            {

                ////convert the supplied submission ID string to int then encrypt it
                int intSubmissionID = Convert.ToInt32(submissionID);
                byte[] submissionIDReader = Sa.Serialization.ToByteArray(intSubmissionID);
                byte[] encryptedSubmissionID = Sa.Crypto.Encrypt(submissionIDReader);

                //encrypt the ClientKey
                byte[] clientKeyReader = Sa.Serialization.ToByteArray(ClientKey);
                byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);

                //submit to the web service
                String returnerror = string.Empty; ;

                byte[] output = null;

                switch (appMode)
                {
                    case "Local":
                        WcfServiceLocal.Service1Client scLocal = new WcfServiceLocal.Service1Client();
                        output = scLocal.GetKPI(encryptedSubmissionID, encryptedClientKey, out returnerror);
                        break;
                    case "Dev":
                        break;
                    case "QA":
                        break;
                    case "Prod":
                        WcfServiceProd.Service1Client scProd = new WcfServiceProd.Service1Client();
                        output = scProd.GetKPI(encryptedSubmissionID, encryptedClientKey, out returnerror);
                        break;
                    default:
                        WcfServiceProd.Service1Client scProdDefault = new WcfServiceProd.Service1Client();
                        output = scProdDefault.GetKPI(encryptedSubmissionID, encryptedClientKey, out returnerror);
                        break;
                }

                if (returnerror.Length > 0)
                {
                    errors.Add(returnerror);
                    return errors;
                }

                ////decrypt the result and put it into a new submission object
                byte[] decryptedKPIs = Sa.Crypto.Decrypt(output);
                output.ShortOutput returnedKPIs = new output.ShortOutput();
                returnedKPIs = Sa.Serialization.ToObject<output.ShortOutput>(decryptedKPIs);

                //move all the items into the existing object
                this.DataKeys = returnedKPIs.DataKeys;
                this.GroupKeys = returnedKPIs.GroupKeys;
                //this.Data = returnedKPIs.Data;
                this.DataSubmissions = returnedKPIs.DataSubmissions;
                this.ProcessDatas = returnedKPIs.ProcessDatas;
                this.Messages = returnedKPIs.Messages;
            }

            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;
        }

        private List<String> SaveKPIDataToXML(String filepath)
        {
            //TODO: write SaveKPItoXML function
            List<String> errors = new List<String>();
            //write this to file:  public partial class ClientKPIs
            //this.Data
            //this.DataKeys
            //this.DataSubmission
            //this.GroupKeys
            /*
            List<String> xmlWriterError = test(filepath);
            if (xmlWriterError.Count > 0)
            {
                return xmlWriterError;
            }
			*/
            try
            {
                XmlWriterSettings setting = new XmlWriterSettings();
                setting.ConformanceLevel = ConformanceLevel.Auto;

                using (XmlWriter writer = XmlWriter.Create(filepath, setting))
                {
                    writer.WriteStartElement("KPIs");//start of the file

                    //write the elements that belong just to the Submission (have to write these separately)
                    foreach (PropertyInfo propertyInfo in this.GetType().GetProperties().Where(propertyInfo => propertyInfo.GetGetMethod().GetParameters().Count() == 0))
                    {

                        if (!propertyInfo.PropertyType.ToString().Contains("ICollection"))// to know if it is a collection or not
                        {
                            writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(this, null) ?? string.Empty).ToString());
                        }
                    }

                    ////then go through each table and add them one at a time (got to be a better way to do this, right?)
                    //foreach (output.Datum a in this.Data) { XMLStuff.WriteXML(a, "Data", writer); }
                    foreach (output.DataKey a in this.DataKeys) { XMLStuff.WriteXML(a, "DataKeys", writer); }
                    foreach ( output.SubmissionData a in this.DataSubmissions) { XMLStuff.WriteXML(a, "DataSubmissions", writer); }
                    foreach (output.GroupKey a in this.GroupKeys) { XMLStuff.WriteXML(a, "GroupKeys", writer); }
                    //foreach (dim.Energy_LU a in this.Energy_LU) { XMLStuff.WriteXML(a, "Energy_LU", writer); }
                    foreach (output.ProcessData a in this.ProcessDatas) { XMLStuff.WriteXML(a, "ProcessDatas", writer); }
                    foreach (output.Message a in this.Messages) { XMLStuff.WriteXML(a, "Messages", writer); }
                    //and write the end of the file
                    writer.WriteEndElement();
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;
        }

        private List<String> ClearCurrentKPIs()
        {
            List<String> errors = new List<String>();

            try
            {
                //first clear all the tables
                this.DataKeys.Clear();
                //this.Data.Clear();
                this.GroupKeys.Clear();
                this.DataSubmissions.Clear();
                this.CalcTimes.Clear();
                this.DataTables.Clear();
                this.Messages.Clear();
                this.ProcessDatas.Clear();
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;
        }

        #endregion

    }

    public partial class ClientReferences : dim.ShortDim
    {

        #region private members

        string appMode = string.Empty;

        #endregion

        #region constructors

        public ClientReferences()
        {
        }

        public ClientReferences(string applicationMode="")
        {
            if (applicationMode.Length > 0)
            appMode = applicationMode;
        }

        #endregion

        #region public
        /// <summary>Retrieve a set of References from the web service and load them into the Reference object. Requires a valid ClientKey.  Returns a list of errors, or an empty list if no errors occurred.</summary>
        public List<String> RetrieveReferences(String ClientKey)
        {
            List<String> errors = new List<String>();

            //error check: ClientKey must not be blank
            if (ClientKey.Trim() == "")
            {
                errors.Add("ClientKey must not be blank");
                return errors;
            }

            //submit the data
            try
            {
                errors = GetReferencesFromService(ClientKey);
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;
        }

        /// <summary>Clear out everything in this Reference object. Returns a list of errors, or an empty list if no errors occurred.</summary>
        public List<String> FlushReferences()
        {
            List<String> errors = ClearReferences();
            return errors;
        }

        /// <summary>Given a path to an XML file it will save the Reference object to that location (and will automatically overwrite any file there).</summary>
        public List<String> SaveReferenceDataToXML(String filepath)
        {
            List<String> errors = new List<String>();

            errors = SaveReferencesToXML(filepath);

            return errors;
        }

        #endregion

        #region private

        private List<String> GetReferencesFromService(String ClientKey)
        {
            List<String> errors = new List<String>();
            byte[] output = null;

            try
            {


                //convert the supplied ClientKey string to byte array then encrypt it
                byte[] clientKeyReader = Sa.Serialization.ToByteArray(ClientKey);
                byte[] encryptedClientKey = Sa.Crypto.Encrypt(clientKeyReader);

                //submit to the web service
                String returnerror = "";

                switch (appMode)
                {
                    case "Local":
                        WcfServiceLocal.Service1Client scLocal = new WcfServiceLocal.Service1Client();
                        output = scLocal.GetReference(encryptedClientKey, out returnerror);
                        break;
                    case "Dev":
                        break;
                    case "QA":
                        break;
                    case "Prod":
                        WcfServiceProd.Service1Client scProd = new WcfServiceProd.Service1Client();
                        output = scProd.GetReference(encryptedClientKey, out returnerror);
                        break;
                    default:
                        WcfServiceProd.Service1Client scProdDefault = new WcfServiceProd.Service1Client();
                        output = scProdDefault.GetReference(encryptedClientKey, out returnerror);
                        break;
                }

                if (returnerror.Length > 0)
                {
                    errors.Add(returnerror);
                    return errors;
                }

                //decrypt the result and put it into a new submission object
                byte[] decryptedReferences = Sa.Crypto.Decrypt(output);
                dim.ShortDim returnedReferences = new dim.ShortDim();
                returnedReferences = Sa.Serialization.ToObject<dim.ShortDim>(decryptedReferences);

                //move all the items into the existing object
                //we do it this way because we already have an object that the user is referencing, and we want to fill it with the data
                this.Absence_LU = returnedReferences.Absence_LU;
                //this.CptlCode_LU = returnedReferences.CptlCode_LU;
                this.Crude_LU = returnedReferences.Crude_LU;
                this.EmissionType_LU = returnedReferences.EmissionType_LU;
                this.Energy_LU = returnedReferences.Energy_LU;
                this.Material_LU = returnedReferences.Material_LU;
                this.OpEx_LU = returnedReferences.OpEx_LU;
                this.Pers_LU = returnedReferences.Pers_LU;
                this.PressureRange_LU = returnedReferences.PressureRange_LU;
                this.Process_LU = returnedReferences.Process_LU;
                this.ProductGrade_LU = returnedReferences.ProductGrade_LU;
                this.TankType_LU = returnedReferences.TankType_LU;
                this.UOM = returnedReferences.UOM;
                this.YieldCategory_LU = returnedReferences.YieldCategory_LU;

                this.FHFuelType_LU = returnedReferences.FHFuelType_LU;
                this.FHProcessFluid_LU = returnedReferences.FHProcessFluid_LU;
                this.FHService_LU = returnedReferences.FHService_LU;
                this.MaterialProp_LU = returnedReferences.MaterialProp_LU;
                this.ProductProperties_LU = returnedReferences.ProductProperties_LU;
                this.ValidValuesList_LU = returnedReferences.ValidValuesList_LU;
            }

            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;
        }

        private List<String> SaveReferencesToXML(String filepath)
        {
            List<String> errors = new List<String>();

            List<String> xmlWriterError = test(filepath);
            if (xmlWriterError.Count > 0)
            {
                return xmlWriterError;
            }

            try
            {
                XmlWriterSettings setting = new XmlWriterSettings();
                setting.ConformanceLevel = ConformanceLevel.Auto;

                using (XmlWriter writer = XmlWriter.Create(filepath, setting))
                {
                    writer.WriteStartElement("References");//start of the file

                    //write the elements that belong just to the Submission (have to write these separately)
                    foreach (PropertyInfo propertyInfo in this.GetType().GetProperties().Where(propertyInfo => propertyInfo.GetGetMethod().GetParameters().Count() == 0))
                    {

                        if (!propertyInfo.PropertyType.ToString().Contains("ICollection"))//holy cow this was hard to figure out how to do. there has to be an easier way to know if it is a collection or not
                        {
                            writer.WriteElementString(propertyInfo.Name.ToString(), (propertyInfo.GetValue(this, null) ?? string.Empty).ToString());
                        }
                    }

                    ////then go through each table and add them one at a time (got to be a better way to do this, right?)
                    foreach (Sa.dbs.Dbo.Absence_LU a in this.Absence_LU) { XMLStuff.WriteXML(a, "Absence_LU", writer); }
                    //foreach (dim.CptlCode_LU a in this.CptlCode_LU) { XMLStuff.WriteXML(a, "CptlCode_LU", writer); }
                    foreach (Sa.dbs.Dbo.Crude_LU a in this.Crude_LU) { XMLStuff.WriteXML(a, "Crude_LU", writer); }
                    foreach (dim.EmissionType_LU a in this.EmissionType_LU) { XMLStuff.WriteXML(a, "EmissionType_LU", writer); }
                    foreach (Sa.dbs.Dbo.Energy_LU a in this.Energy_LU) { XMLStuff.WriteXML(a, "Energy_LU", writer); }
                    foreach (dim.FHFuelType_LU a in this.FHFuelType_LU) { XMLStuff.WriteXML(a, "FHFuelType_LU", writer); }
                    foreach (dim.FHProcessFluid_LU a in this.FHProcessFluid_LU) { XMLStuff.WriteXML(a, "FHProcessFluid_LU", writer); }
                    foreach (dim.FHService_LU a in this.FHService_LU) { XMLStuff.WriteXML(a, "FHService_LU", writer); }
                    foreach (Sa.dbs.Dbo.Material_LU a in this.Material_LU) { XMLStuff.WriteXML(a, "Material_LU", writer); }
                    foreach (dim.MaterialProp_LU a in this.MaterialProp_LU) { XMLStuff.WriteXML(a, "MaterialProp_LU", writer); }
                    foreach (dim.OpEx_LU a in this.OpEx_LU) { XMLStuff.WriteXML(a, "OpEx_LU", writer); }
                    foreach (Sa.dbs.Dbo.Pers_LU a in this.Pers_LU) { XMLStuff.WriteXML(a, "Pers_LU", writer); }
                    foreach (dim.PressureRange_LU a in this.PressureRange_LU) { XMLStuff.WriteXML(a, "PressureRange_LU", writer); }
                    foreach (dim.Process_LU a in this.Process_LU) { XMLStuff.WriteXML(a, "Process_LU", writer); }
                    foreach (dim.ProductGrade_LU a in this.ProductGrade_LU) { XMLStuff.WriteXML(a, "ProductGrade_LU", writer); }
                    foreach (dim.ProductProperties_LU a in this.ProductProperties_LU) { XMLStuff.WriteXML(a, "ProductProperties_LU", writer); }
                    foreach (Sa.dbs.Dbo.TankType_LU a in this.TankType_LU) { XMLStuff.WriteXML(a, "TankType_LU", writer); }
                    foreach (Sa.dbs.Dbo.UOM a in this.UOM) { XMLStuff.WriteXML(a, "UOM", writer); }
                    foreach (dim.ValidValuesList_LU a in this.ValidValuesList_LU) { XMLStuff.WriteXML(a, "ValidValuesList_LU", writer); }
                    foreach (dim.YieldCategory_LU a in this.YieldCategory_LU) { XMLStuff.WriteXML(a, "YieldCategory_LU", writer); }

                    //and write the end of the file
                    writer.WriteEndElement();
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }

            return errors;
        }

        private List<String> ClearReferences()
        {
            List<String> result = new List<String>();

            try
            {
                //first clear all the tables
                this.Absence_LU.Clear();
                //this.CptlCode_LU.Clear();
                this.Crude_LU.Clear();
                this.EmissionType_LU.Clear();
                this.Energy_LU.Clear();
                this.Material_LU.Clear();
                this.OpEx_LU.Clear();
                this.Pers_LU.Clear();
                this.PressureRange_LU.Clear();
                this.Process_LU.Clear();
                this.ProductGrade_LU.Clear();
                this.TankType_LU.Clear();
                this.UOM.Clear();
                this.YieldCategory_LU.Clear();

            }

            catch (Exception ex)
            {
                result.Add(ex.Message.ToString());
            }

            return result;
        }

        private List<String> test(String filepath)
        {


            //var props = this.GetType().GetProperties();

            //foreach (PropertyInfo pi in this.GetType().GetProperties())
            //	{
            //	if (pi.PropertyType.Name.Contains("ICollection"))//.GetInterface(typeof(System.Collections.Generic.ICollection<>).FullName) != null)
            //		{
            //		//pi.SetValue(c, pi.GetValue(a, null));
            //		//Type type = pi.GetType();
            //		//if (type.IsClass == true)
            //		//	{
            //		System.Diagnostics.Debug.Write(pi.Name);
            //		object o = this.GetType().GetProperty(pi.Name);
            //		object p = this.
            //		//}
            //		}
            //	}


            //		public void LogObject(object obj, int indent)
            //{
            //if (obj == null) return;
            //string indentString = new string(' ', indent);

            List<String> errors = new List<String>();
            try
            {
                XmlWriterSettings setting = new XmlWriterSettings();
                setting.ConformanceLevel = ConformanceLevel.Auto;

                using (XmlWriter writer = XmlWriter.Create(filepath, setting))
                {

                    XMLStuff.WriteClassToXML(this, writer);

                }
            }
            catch (Exception ex)
            {
                errors.Add(ex.Message.ToString());
            }
            return errors;
            //Type objType = this.GetType();
            //PropertyInfo[] properties = objType.GetProperties();

            //foreach (PropertyInfo property in properties)
            //{
            //	Type tColl = typeof(ICollection<>);
            //	Type t = property.PropertyType;
            //	string name = property.Name;


            //	object propValue = property.GetValue(this, null);
            //	//check for nested classes as properties
            //	if (property.PropertyType.Assembly == objType.Assembly)
            //		{
            //		//string _result = string.Format("{0}{1}:", indentString, property.Name);
            //		//log.Info(_result);
            //		//LogObject(propValue, indent + 2);
            //		}
            //	else
            //		{
            //		//string _result = string.Format("{0}{1}: {2}", indentString, property.Name, propValue);
            //		//log.Info(_result);
            //		}

            //	//check for collection
            //	if (t.IsGenericType && tColl.IsAssignableFrom(t.GetGenericTypeDefinition()) ||
            //		t.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == tColl))
            //		{
            //		////var get = property.GetGetMethod();
            //		IEnumerable listObject = (IEnumerable)property.GetValue(this, null);
            //		if (listObject != null)
            //			{
            //			foreach (object o in listObject)
            //				{
            //				//LogObject(o, indent + 2);
            //				}
            //			}
            //		}
            //}
            //}












        }

        #endregion

    }

    internal class XMLStuff
    {

        public static void WriteXML<T>(T a, string name, XmlWriter writer)
        {
            //generic method to write each table to XML
            writer.WriteStartElement(name);
            foreach (PropertyInfo p in a.GetType().GetProperties())
            {
                if (p.Name.ToString() != "Submission")
                {
                    writer.WriteElementString(p.Name.ToString(), (p.GetValue(a, null) ?? string.Empty).ToString());
                }
            }

            writer.WriteEndElement();

        }

        public static void WriteClassToXML(object theClass, XmlWriter writer)
        {

            Type objType = theClass.GetType();

            writer.WriteStartElement(objType.Name);

            PropertyInfo[] properties = objType.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                Type tColl = typeof(ICollection<>);
                Type t = property.PropertyType;
                string name = property.Name;


                object propValue = property.GetValue(theClass, null);
                //check for nested classes as properties
                if (property.PropertyType.Assembly == objType.Assembly)
                {
                    //string _result = string.Format("{0}{1}:", indentString, property.Name);
                    //log.Info(_result);
                    //LogObject(propValue, indent + 2);
                }
                else
                {
                    //string _result = string.Format("{0}{1}: {2}", indentString, property.Name, propValue);
                    //log.Info(_result);
                }

                //check for collection
                if (t.IsGenericType && tColl.IsAssignableFrom(t.GetGenericTypeDefinition()) ||
                    t.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == tColl))
                {





                    ////var get = property.GetGetMethod();
                    IEnumerable listObject = (IEnumerable)property.GetValue(theClass, null);
                    if (listObject != null)
                    {
                        foreach (object o in listObject)
                        {

                            writer.WriteStartElement(name);

                            foreach (PropertyInfo p in o.GetType().GetProperties())
                            {
                                writer.WriteElementString(p.Name.ToString(), (p.GetValue(o, null) ?? string.Empty).ToString());

                            }
                            //writer.WriteElementString(o.Name.ToString(), (o.GetValue(theClass, null) ?? string.Empty).ToString());
                            //LogObject(o, indent + 2);

                            writer.WriteEndElement();

                        }
                    }





                }
            }

            writer.WriteEndElement();

        }

    }

    #region partialclasses

    //these are here so the client can see them, they don't do anything else except make them visible to the client

    //Staging
    [Serializable]
    public class Absence : staging.Absence { }
    [Serializable]
    public class Config : staging.Config { }
    [Serializable]
    public partial class ConfigBuoy : staging.ConfigBuoy { }
    [Serializable]
    public partial class ConfigRS : staging.ConfigR { }
    [Serializable]
    public partial class Crude : staging.Crude { }
    [Serializable]
    public partial class Diesel : staging.Diesel { }
    [Serializable]
    public partial class Emission : staging.Emission { }
    [Serializable]
    public partial class Energy : staging.Energy { }
    [Serializable]
    public partial class FiredHeater : staging.FiredHeater { }
    [Serializable]
    public partial class Gasoline : staging.Gasoline { }
    [Serializable]
    public partial class GeneralMisc : staging.GeneralMisc { }
    [Serializable]
    public partial class Inventory : staging.Inventory { }
    [Serializable]
    public partial class Kerosene : staging.Kerosene { }
    [Serializable]
    public partial class LPG : staging.LPG { }
    [Serializable]
    public partial class MaintRout : staging.MaintRout { }
    [Serializable]
    public partial class MaintTA : staging.MaintTA { }
    [Serializable]
    public partial class MarineBunker : staging.MarineBunker { }
    [Serializable]
    public partial class MExp : staging.MExp { }
    [Serializable]
    public partial class MiscInput : staging.MiscInput { }
    [Serializable]
    public partial class OpEx : staging.OpEx { }
    [Serializable]
    public partial class Personnel : staging.Personnel { }
    [Serializable]
    public partial class ProcessData : staging.ProcessData { }
    [Serializable]
    public partial class Resid : staging.Resid { }
    [Serializable]
    public partial class RPFResid : staging.RPFResid { }
    [Serializable]
    public partial class Steam : staging.Steam { }
    [Serializable]
    public partial class Yield : staging.Yield { }

    //References
    //[Serializable]
    //public partial class Absence_LU : dim.Absence_LU { }
    //[Serializable]
    //public partial class CptlCode_LU : dim.CptlCode_LU { }
    //[Serializable]
    //public partial class Crude_LU : dim.Crude_LU { }
    [Serializable]
    public partial class EmissionType_LU : dim.EmissionType_LU { }
    //[Serializable]
    //public partial class Energy_LU : dim.Energy_LU { }
    //[Serializable]
    //public partial class Material_LU : dim.Material_LU { }
    [Serializable]
    public partial class OpEx_LU : dim.OpEx_LU { }
    //[Serializable]
    //public partial class Pers_LU : dim.Pers_LU { }
    [Serializable]
    public partial class PressureRange_LU : dim.PressureRange_LU { }
    [Serializable]
    public partial class Process_LU : dim.Process_LU { }
    [Serializable]
    public partial class ProductGrade_LU : dim.ProductGrade_LU { }
    //[Serializable]
    //public partial class TankType_LU : dim.TankType_LU { }
    //[Serializable]
    //public partial class UOM : dim.UOM { }
    [Serializable]
    public partial class YieldCategory_LU : dim.YieldCategory_LU { }

    //KPIs
    //public partial class Datum : output.Datum { }
    [Serializable]
    public partial class DataKey : output.DataKey { }
    [Serializable]
    public partial class GroupKey : output.GroupKey { }
    [Serializable]
    public partial class DataSubmission : output.DataSubmission { }


    #endregion



}
