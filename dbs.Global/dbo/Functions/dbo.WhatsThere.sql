﻿CREATE AGGREGATE [dbo].[WhatsThere](@valueA NVARCHAR (200))
    RETURNS NVARCHAR (MAX)
    EXTERNAL NAME [CustomAggregates].[CustomAggregates.WhatsThere];

